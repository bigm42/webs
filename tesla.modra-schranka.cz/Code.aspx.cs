﻿using System;
using System.Web.UI.WebControls;

public partial class live_CODE : System.Web.UI.Page
{
    protected void F0_OnLoad(object sender, EventArgs e)
    {
	string[] lines = System.IO.File.ReadAllLines(Server.MapPath("~/Default.aspx.cs"));
	Label lb=new Label();
	for (int i=0;i<lines.Length;i++)
		{
		lb.Text+=lines[i]+"<br>";
		}
	P1.Controls.Add(lb);
    }

    protected void HL(object sender, EventArgs e)
    {
       if (Request.Url.OriginalString.ToUpper().Contains("HTTPS"))
	      (sender as HyperLink).NavigateUrl="https://tesla.modra-schranka.cz/Default.aspx";
       else
	      (sender as HyperLink).NavigateUrl="http://tesla.modra-schranka.cz/Default.aspx";
    }
}
﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing;

public partial class TESLAedit_Default : System.Web.UI.Page
{
    protected void HL(object sender, EventArgs e)
    {
       if (Request.Url.OriginalString.ToUpper().Contains("HTTPS"))
	      (sender as HyperLink).NavigateUrl="https://tesla.modra-schranka.cz/Code.aspx";
       else
	      (sender as HyperLink).NavigateUrl="http://tesla.modra-schranka.cz/Code.aspx";
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

	W.Visible=false;P.Visible=false;

            if (!Request.Url.ToString().Contains("https"))
        {
            string sr = Request.Url.ToString();
            int i=0;
            while (!sr.Substring(i,24).Equals("tesla.modra-schranka.cz/"))
                i++;
            Response.Redirect("https://"+sr.Substring(i));
        }
    }

	protected int FindFirstSlash(int from, string str)
	{
		int i=from;
		while (str[i]!='/') i++;
		return i;
	}

    protected void TESLAedit(object sender, EventArgs e)
    {
        if ((sender as TextBox).Text.Substring(0,6).Equals("login:"))
	{
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
	        SqlCommand comm = new SqlCommand("select id,pass from tesla_users where nick='"+(sender as TextBox).Text.Substring(6,FindFirstSlash(0,(sender as TextBox).Text)-6)+"'", conn);
        string id="";string command="";
	try
        {
            conn.Open();
            SqlDataReader dr=comm.ExecuteReader();
		if (!dr.Read()) 
		{ 
			command = "insert into tesla_users(nick,pass) values('" + (sender as TextBox).Text.Substring(6, FindFirstSlash(0, (sender as TextBox).Text) - 6) + "','" + (sender as TextBox).Text.Substring(FindFirstSlash(0, (sender as TextBox).Text) + 1, FindFirstSlash(FindFirstSlash(0, (sender as TextBox).Text) + 1, (sender as TextBox).Text) - FindFirstSlash(0, (sender as TextBox).Text) - 1) + "') "; 
			id = "@@identity";
		}
		else
		{
			if (!((sender as TextBox).Text.Substring(FindFirstSlash(0, (sender as TextBox).Text) + 1, FindFirstSlash(FindFirstSlash(0, (sender as TextBox).Text) + 1, (sender as TextBox).Text) - FindFirstSlash(0, (sender as TextBox).Text) - 1)).Equals(dr["pass"]))
				{W.Visible=true;P.Visible=true;return;}
		        id=dr["id"].ToString();
		}
	     dr.Close();
		comm = new SqlCommand(command+"insert into tesla_texts(userid,t,descr) values("+id+",@t,'"+(sender as TextBox).Text.Substring(FindFirstSlash(FindFirstSlash(0,(sender as TextBox).Text)+1,(sender as TextBox).Text)+1)+"')", conn);
        comm.Parameters.Add("@t",DateTime.Now);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
	}
        (sender as TextBox).Text = "login:";
    }

    protected void TESLAeditScreen(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select tesla_users.nick,tesla_texts.t,tesla_texts.descr from tesla_texts left join tesla_users on tesla_texts.userid=tesla_users.id", conn);
        bool b=false;
	try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
		{
			        Color color = new Color();
			        if (b) 
					color = ColorTranslator.FromHtml("#ffef56");
				else
					color = ColorTranslator.FromHtml("#1d7c24");
				b=!b;
				Label lb=new Label();
				lb.BackColor=color;
				lb.Text=Convert.ToDateTime(dr[1]).ToString("yyyy-MM-dd HH:mm")+" <b>["+dr[0].ToString()+"]</b> "+dr[2].ToString()+"<br/>";
				(sender as Panel).Controls.Add(lb);
		}
		dr.Close();
        }
        finally { conn.Close(); }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Statistic.aspx.cs" Inherits="Statistic" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>(game4upper statistic)</title>
<link rel="icon" href="~/tarum.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/tarum.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/21karty/zada.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
</head>
<body style="background-color:#e9e9e9">

    <form id="form1" runat="server">
    <div>
        <br />
        <asp:Panel runat="server" HorizontalAlign="Center">
            <h1>(game4upper statistic)</h1>
            <h3>sledováno od 17.10.2018 - watched from 2018/17/10</h3>
            <asp:Label ID="LabelCompWins" runat="server" OnPreRender="LabelCompWins_PreRender"></asp:Label>
            <asp:Table runat="server" HorizontalAlign="Center" GridLines="Both">
                <asp:TableRow><asp:TableCell BackColor="DarkGray">
                    <b>Filter:</b><br />
                    rows: <asp:TextBox ID="TextBoxFilterNum" runat="server" Width="50" Text="50"></asp:TextBox>
                </asp:TableCell><asp:TableCell>
                    IP: <asp:TextBox ID="TextBoxFilterIP" runat="server"></asp:TextBox><br />
                    name: <asp:TextBox ID="TextBoxFilterName" runat="server"></asp:TextBox>
                </asp:TableCell><asp:TableCell>
                    Harvested cards
                </asp:TableCell><asp:TableCell>
                    computer: <asp:TextBox ID="TextBoxFilterPaklComp" runat="server"></asp:TextBox><asp:RadioButton ID="RadioButtonComp" runat="server" GroupName="radio" Text="X" BackColor="Red" /><br />
                    player: <asp:TextBox ID="TextBoxFilterPaklPlayer" runat="server"></asp:TextBox><asp:RadioButton ID="RadioButtonPlayer" runat="server" GroupName="radio" Text="X" BackColor="Green" />
                </asp:TableCell><asp:TableCell>
                    <asp:Button ID="ButtonVyhledat" runat="server" Text="Vyhledat" />
                    <br />
                    <asp:RadioButton ID="RadioButton1" runat="server" GroupName="radio" Text="X"/>
                </asp:TableCell></asp:TableRow>
            </asp:Table>
            <asp:Label ID="LabelNalezeno" runat="server" Text=""></asp:Label>
            <br /><br />
		<asp:HyperLink ID="HyperLinkBack1" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx" >Zpátky. (Back.)</asp:HyperLink>
		<br />
            <asp:LinkButton ID="LinkButtonDown1" runat="server" OnClick="LinkButtonDown_Click"><<<</asp:LinkButton>
            --------- 
            <asp:LinkButton ID="LinkButtonUp1" runat="server" OnClick="LinkButtonUp_Click">>>></asp:LinkButton>
            <br />
            <asp:Table ID="TableVypis" runat="server" HorizontalAlign="Center" GridLines="Both" OnPreRender="TableVypis_PreRender">
                <asp:TableHeaderRow BackColor="DarkGray" Font-Bold="True"><asp:TableCell>
                    IP
                </asp:TableCell><asp:TableCell>
                    Name of player
                </asp:TableCell><asp:TableCell>
                    X
                </asp:TableCell><asp:TableCell>
                    Harvested cards of computer
                </asp:TableCell><asp:TableCell>
                    Harvested cards of player
                </asp:TableCell></asp:TableHeaderRow>
            </asp:Table>
            <asp:LinkButton ID="LinkButtonDown2" runat="server" OnClick="LinkButtonDown_Click"><<<</asp:LinkButton>
            --------- 
            <asp:LinkButton ID="LinkButtonUp2" runat="server" OnClick="LinkButtonUp_Click">>>></asp:LinkButton>
		<br />
		<asp:HyperLink ID="HyperLinkBack2" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx" >Zpátky. (Back.)</asp:HyperLink>
		<br />
        </asp:Panel>
       </div>
    </form>
</body>
</html>

﻿<%@ WebHandler Language="C#" Class="CardBack" %>

using System;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

public class CardBack : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        Random rnd = new Random(DateTime.Now.Millisecond);

        Bitmap bm = new Bitmap(200,200);
         Bitmap card = new Bitmap(115,164);
        Graphics gr = Graphics.FromImage(bm);
	int x;int y;int col;
        for (int i=0;i<21;i++){x=rnd.Next(200);y=rnd.Next(200);col=rnd.Next(256);
            gr.FillRectangle(new SolidBrush(Color.FromArgb(col,col,col)), x, y, rnd.Next(200-x)+x, rnd.Next(200-y)+y);
	}

           for (int xx=43;xx<115+43;xx++)
                for (int yy=18;yy<164+18;yy++)
                    card.SetPixel(xx-43, yy-18, bm.GetPixel(xx, yy));        
         
        MemoryStream st = new MemoryStream();
        card.Save(st, ImageFormat.Png);
        context.Response.Clear();
        context.Response.ContentType = "image/png";
        //context.Response.Buffer = true;
        //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BinaryWrite(st.GetBuffer());
        context.Response.Flush();
        //context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
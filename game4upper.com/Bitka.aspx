﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bitka.aspx.cs" Inherits="Bitka" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Tarum přebika! (game4upper)</title>
<link rel="icon" href="~/tarum.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/tarum.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/21karty/zada.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
</head>
<body style="background-color:#e9e9e9">

    <form id="form1" runat="server">
    <div>
        <asp:Label ID="LabelUnder" runat="server" Text="" OnLoad="LabelUnder_Load" Visible="false"></asp:Label>
        <br />
        <asp:Panel runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="souhlas_Load" OnClick="souhlas_Click"/>
        <asp:Label ID="LabelBR" runat="server" Text="<br>"></asp:Label>
            <asp:CheckBox ID="CheckBoxReport" runat="server" AutoPostBack="True" Text="Report."  OnCheckedChanged="CheckBoxReport_CheckedChanged"/>
        <br />
            Jméno hráče (name of player): <asp:TextBox ID="TextBoxName" runat="server" Width="80" AutoPostBack="True" OnInit="TextBoxName_Init" OnTextChanged="TextBoxName_TextChanged"></asp:TextBox>
        <h1>Tarum přebika na 4! (Tarum game4upper!)</h1>
           
        <asp:Panel ID="PanelStul" runat="server" HorizontalAlign="Center">
        <asp:Table ID="Stul" runat="server" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell BorderWidth="2"  BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center">
                    <asp:Image ID="KartaStulComp" runat="server" ImageUrl="~/21karty/hrej_comp.png" BorderWidth="2"  BorderColor="White" BorderStyle="Solid" />
                    <asp:Image ID="KartaStulTY" runat="server" ImageUrl="~/21karty/hrej_ty.png" BorderWidth="2"  BorderColor="White" BorderStyle="Solid" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="Pakly" runat="server" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" BorderWidth="4"  BorderColor="White" BorderStyle="Solid">
                    <asp:Image ID="Zada1" runat="server" ImageUrl="~/21karty/zada.png" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center" >
                    <asp:Label ID="LabelPaklComp" runat="server" OnPreRender="LabelPaklComp_PreRender" />
                </asp:TableCell>
                <asp:TableCell Width="400">
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelPaklRuka" runat="server" OnPreRender="LabelPaklRuka_PreRender" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center" BorderWidth="4"  BorderColor="White" BorderStyle="Solid">
                    <asp:Image ID="Zada2" runat="server" ImageUrl="~/21karty/zada.png" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="Ruka" runat="server" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell BorderWidth="4"  BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center">
                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/21karty/zada.png"  OnClick="Ruka_Click"/>
                    <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/21karty/zada.png" OnClick="Ruka_Click"/>
                    <asp:ImageButton ID="Image3" runat="server" ImageUrl="~/21karty/zada.png" OnClick="Ruka_Click"/>
                    <asp:ImageButton ID="Image4" runat="server" ImageUrl="~/21karty/zada.png" OnClick="Ruka_Click"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table></asp:Panel>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center"></asp:Panel>
        <br /><br />
        --- <asp:Button ID="ButtonRozdej" runat="server" Text="...Rozdej... (...Deal again...)"  OnClick="ButtonRozdej_Click" ToolTip="...Deal..." Height="50" Width="400" Font-Size="20"/> ---
        <asp:Button ID="ButtonDale" runat="server" Text="...Pokračovat... (...Continue...)"  OnClick="ButtonDale_Click" Enabled="False" ToolTip="...Continue..." Height="50" Width="400" Font-Size="20"/> ---
	<br /><br />
         --- <asp:HyperLink ID="HyperLink" runat="server" ForeColor="Black" NavigateUrl="~/Tarum Cards.png" >Všechny karty. (All Cards.)</asp:HyperLink>
       --- <asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx" >Zpátky. (Back.)</asp:HyperLink> ---
	<br /><br />
        <asp:Label runat="server" ForeColor="DarkGray" Text="--- AD2014 - AD2018 Manufactured by MICKEYsoftware. (v1.1.2.5) ---"/>
	<br />       
    </asp:Panel>
    </div>
    </form>
</body>
</html>

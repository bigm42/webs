﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web;

public partial class Bitka : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Session["login"] == null)
        {
            for (int i = 0; i < Request.Cookies.Count; i++)
            {
                if (Request.Cookies[i].Name.Equals("name") || Request.Cookies[i].Name.Equals("cookies"))
                {
                    HttpCookie cook = new HttpCookie(Request.Cookies[i].Name, Request.Cookies[i].Value);
                    cook.Expires = DateTime.Now.AddYears(1);
                    Response.Cookies.Set(cook);
                }
            }
            Session["login"] = 1;
        }

        if (!Request.Url.ToString().Contains("https"))
        {
            int j = 0;
            for (int i = 0; i < Request.Url.ToString().Length - 14; i++)
                if (Request.Url.ToString().Substring(i, 14) == "game4upper.com") j = i;
            Response.Redirect("https://" + Request.Url.ToString().Substring(j));
        }

        if ((Request["pz"]!=null)&&(!HyperLinkBack.NavigateUrl.Contains("pz=1")))
            HyperLinkBack.NavigateUrl+="?pz=1";

        if (Session["pakli"] == null)
            ButtonRozdej_Click(ButtonRozdej, e);
        if (Session["paklrukai"] == null)
            Session["paklrukai"] = 0;
        if (Session["paklcompi"] == null)
            Session["paklcompi"] = 0;
        if (Session["rukai"] == null)
            Session["rukai"] = 0;
        if (Session["comprukai"] == null)
            Session["comprukai"] = 0;

    }

    protected bool VyssiVPaklu(int karta)
    {
        object[] obj = { Image1, Image2, Image3, Image4 };
        for (int i = 0; i < Convert.ToInt32(Session["pakli"]); i++)
        {
            if (Convert.ToInt32(Session["pakl" + i.ToString()]) > karta)
                return true;
        }
        for (int i = 0; i < Convert.ToInt32(Session["rukai"]); i++)
        {
            if (Convert.ToInt32((obj[i] as ImageButton).ImageUrl.Substring(10, (obj[i] as ImageButton).ImageUrl.Length - 14)) > karta)
                return true;
        }
        return false;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        Random rnd = new Random(DateTime.Now.Millisecond);
        int loops = 0;

    loop:
        if ((Session["comp"] != null) && (Convert.ToInt32(Session["comprukai"]) > 0))
        {
            int r = -1; bool hrano = false;
            if (KartaStulTY.ImageUrl.Contains("hrej") && KartaStulComp.ImageUrl.Contains("hrej"))
            {
                int max = 0;
                for (int i = 0; i < Convert.ToInt32(Session["comprukai"]); i++)
                    if (Convert.ToInt32(Session["compruka" + i.ToString()]) > Convert.ToInt32(Session["compruka" + max.ToString()]))
                        max = i;
                if (VyssiVPaklu(Convert.ToInt32(Session["compruka" + max.ToString()])))
                    r = rnd.Next(Convert.ToInt32(Session["comprukai"]));
                else r = max;
                hrano = true;
            }
            else
            {
                if (KartaStulComp.ImageUrl.Contains("hrej"))
                {
                    int[] rozd = new int[4];
                    for (int i = 0; i < Convert.ToInt32(Session["comprukai"]); i++)
                        rozd[i] = Convert.ToInt32(Session["compruka" + i.ToString()]) - Convert.ToInt32(KartaStulTY.ImageUrl.Substring(10, KartaStulTY.ImageUrl.Length - 14));
                    int idea1 = 0;
                    while ((rozd[idea1] < 0) && (idea1 < Convert.ToInt32(Session["comprukai"]) - 1))
                        idea1++;
                    for (int i = idea1 + 1; i < Convert.ToInt32(Session["comprukai"]); i++)
                    {
                        if ((rozd[i] < rozd[idea1]) && (rozd[i] > 0))
                            idea1 = i;
                    }
                    if (rozd[idea1] > 0)
                        r = idea1;
                    else
                    {
                        int min = 0;
                        for (int i = 1; i < Convert.ToInt32(Session["comprukai"]); i++)
                        {
                            if (Convert.ToInt32(Session["compruka" + i.ToString()]) < Convert.ToInt32(Session["compruka" + min.ToString()]))
                                min = i;
                        }
                        r = min;
                    }
                    ButtonDale.Enabled = true;
                    hrano = true;
                }
            }
            if (hrano)
            {
                KartaStulComp.ImageUrl = "~/21karty/" + Session["compruka" + r.ToString()].ToString() + ".png";
                for (int i = r; i < Convert.ToInt32(Session["comprukai"]) - 1; i++)
                    Session["compruka" + i.ToString()] = Session["compruka" + (i + 1).ToString()];
                Session["comprukai"] = Convert.ToInt32(Session["comprukai"]) - 1;
                Session["comp"] = null;
                Vypis();
                return;
            }
        }

        if ((!KartaStulTY.ImageUrl.Contains("hrej") && !KartaStulComp.ImageUrl.Contains("hrej")))
        {
            object[] obj = { Image1, Image2, Image3, Image4 };
            if (Convert.ToInt32(KartaStulTY.ImageUrl.Substring(10, KartaStulTY.ImageUrl.Length - 14)) > Convert.ToInt32(KartaStulComp.ImageUrl.Substring(10, KartaStulComp.ImageUrl.Length - 14)))
            {
                Session["paklruka" + Session["paklrukai"].ToString()] = Convert.ToInt32(KartaStulTY.ImageUrl.Substring(10, KartaStulTY.ImageUrl.Length - 14));
                Session["paklruka" + (Convert.ToInt32(Session["paklrukai"]) + 1).ToString()] = Convert.ToInt32(KartaStulComp.ImageUrl.Substring(10, KartaStulComp.ImageUrl.Length - 14));
                Session["paklrukai"] = Convert.ToInt32(Session["paklrukai"]) + 2;
                if (Convert.ToInt32(Session["pakli"]) > 0)
                {
                    (obj[Convert.ToInt32(Session["rukai"])] as ImageButton).Visible = true;
                    (obj[Convert.ToInt32(Session["rukai"])] as ImageButton).ImageUrl = "~/21karty/" + Session["pakl" + (Convert.ToInt32(Session["pakli"]) - 1).ToString()].ToString() + ".png";
                    Session["rukai"] = Convert.ToInt32(Session["rukai"]) + 1;
                    Session["compruka" + Session["comprukai"].ToString()] = Session["pakl" + (Convert.ToInt32(Session["pakli"]) - 2).ToString()];
                    Session["comprukai"] = Convert.ToInt32(Session["comprukai"]) + 1;
                    Session["pakli"] = Convert.ToInt32(Session["pakli"]) - 2;
                }
                Session["comp"] = null;
            }
            else
            {
                Session["paklcomp" + Session["paklcompi"].ToString()] = Convert.ToInt32(KartaStulTY.ImageUrl.Substring(10, KartaStulTY.ImageUrl.Length - 14));
                Session["paklcomp" + (Convert.ToInt32(Session["paklcompi"]) + 1).ToString()] = Convert.ToInt32(KartaStulComp.ImageUrl.Substring(10, KartaStulComp.ImageUrl.Length - 14));
                Session["paklcompi"] = Convert.ToInt32(Session["paklcompi"]) + 2;
                if (Convert.ToInt32(Session["pakli"]) > 0)
                {
                    Session["compruka" + Session["comprukai"].ToString()] = Session["pakl" + (Convert.ToInt32(Session["pakli"]) - 1).ToString()];
                    Session["comprukai"] = Convert.ToInt32(Session["comprukai"]) + 1;
                    (obj[Convert.ToInt32(Session["rukai"])] as ImageButton).Visible = true;
                    (obj[Convert.ToInt32(Session["rukai"])] as ImageButton).ImageUrl = "~/21karty/" + Session["pakl" + (Convert.ToInt32(Session["pakli"]) - 2).ToString()].ToString() + ".png";
                    Session["rukai"] = Convert.ToInt32(Session["rukai"]) + 1;
                    Session["pakli"] = Convert.ToInt32(Session["pakli"]) - 2;
                }
                Session["comp"] = 1;
            }
            KartaStulTY.ImageUrl = "~/21karty/hrej_ty.png";
            KartaStulComp.ImageUrl = "~/21karty/hrej_comp.png";
            loops++;
            if (loops > 10) { LabelUnder.Text += "loops>10!"; return; }
            if (Session["comp"] != null) goto loop;
        }

        if ((Convert.ToInt32(Session["comprukai"]) == 0) && (Convert.ToInt32(Session["rukai"]) == 0))
        {
            PanelStul.Visible = false;
            ButtonDale.Enabled = false;

            Label lb = new Label();int win = 100;
            if (Convert.ToInt32(Session["paklrukai"]) > Convert.ToInt32(Session["paklcompi"]))
            { lb.Text = "<br /><br /><h2>Vyhrál jste! (You win!)</h2>"; win = 0; Session["comp"] = null; }
            else
                {lb.Text = "<br /><br /><h2>Prohrál jste! (You lose!)</h2>";Session["comp"] = 1;}
            lb.Text += "Computer's harvested cards:<br>";
            Panel1.Controls.Add(lb);
            for (int i = 0; i < Convert.ToInt32(Session["paklcompi"]); i++)
            {
                Image img = new Image();
                img.ImageUrl = "~/21karty/" + Session["paklcomp" + i.ToString()].ToString() + ".png";
                Panel1.Controls.Add(img);
            }
            lb = new Label();
            lb.Text = "<br /><br /><br /><br />Tvé karty (Your cards):<br />";
            Panel1.Controls.Add(lb);
            for (int i = 0; i < Convert.ToInt32(Session["paklrukai"]); i++)
            {
                Image img = new Image();
                img.ImageUrl = "~/21karty/" + Session["paklruka" + i.ToString()].ToString() + ".png";
                Panel1.Controls.Add(img);
            }
            lb = new Label();
            lb.Text = "<br /><br />";
            Panel1.Controls.Add(lb);

            string str1 = "";string str3 = "";
            for (int i = 0; i < Convert.ToInt32(Session["paklcompi"]); i++)
            {
                str3 = Session["paklcomp" + i.ToString()].ToString();
		if (str3.Length==1) str3="0"+str3;
		str1+=str3;
                if (i != Convert.ToInt32(Session["paklcompi"]) - 1) str1 += ",";
            }
            string str2 = "";
            for (int i = 0; i < Convert.ToInt32(Session["paklrukai"]); i++)
            {
                str3 = Session["paklruka" + i.ToString()].ToString();
		if (str3.Length==1) str3="0"+str3;
		str2+=str3;
                if (i != Convert.ToInt32(Session["paklrukai"]) - 1) str2 += ",";
            }
            SqlConnection conn = new SqlConnection(Constants.ConnStr);
            SqlCommand comm = new SqlCommand("select max(id) from game4upper_wins", conn);
            try
            {
                conn.Open();
                object obj = comm.ExecuteScalar();
                int id = -1;
                if (obj != DBNull.Value) id = Convert.ToInt32(obj); 
                comm = new SqlCommand("insert into game4upper_wins (id,ip,name,win,computer,player) values(@id1,@ip1,@name1,@win1,@comp,@play)", conn);
                comm.Parameters.AddWithValue("id1", id + 1);
                comm.Parameters.AddWithValue("ip1", Request.UserHostAddress);
                comm.Parameters.AddWithValue("name1", TextBoxName.Text);
                comm.Parameters.AddWithValue("win1", win);
                comm.Parameters.AddWithValue("comp", str1);
                comm.Parameters.AddWithValue("play", str2);
                comm.ExecuteNonQuery();
            }
            finally { conn.Close(); }
        }

        Vypis();
    }

    protected void Vypis()
    {
        LabelUnder.Text += "# of cards in your hand=" + Session["rukai"].ToString() + "; # of cards in computer's hand=" + Session["comprukai"].ToString() + "; # of your harvested cards=" + Session["paklrukai"].ToString() + "; # of computer's harvested cards=" + Session["paklcompi"].ToString() + "; # of cards to distribute=" + Session["pakli"].ToString()+"<br>coputer's hand:";
        for (int i = 0; i < Convert.ToInt32(Session["comprukai"]); i++)
            {LabelUnder.Text += Session["compruka" + i.ToString()].ToString();
		if (i!=Convert.ToInt32(Session["comprukai"])-1) LabelUnder.Text +=  ",";}
        LabelUnder.Text += "<br>coputer's harvested cards:";
        for (int i = 0; i < Convert.ToInt32(Session["paklcompi"]); i++)
            {LabelUnder.Text += Session["paklcomp" + i.ToString()].ToString();
		if (i!=Convert.ToInt32(Session["paklcompi"])-1) LabelUnder.Text +=  ",";}
        LabelUnder.Text += "<br>your harvested cards:";
        for (int i = 0; i < Convert.ToInt32(Session["paklrukai"]); i++)
            {LabelUnder.Text += Session["paklruka" + i.ToString()].ToString();
		if (i!=Convert.ToInt32(Session["paklrukai"])-1) LabelUnder.Text +=  ",";}
        LabelUnder.Text += "<br>cards to distribute:";
        for (int i = 0; i < Convert.ToInt32(Session["pakli"]); i++)
            {LabelUnder.Text += Session["pakl" + i.ToString()].ToString();
		if (i!=Convert.ToInt32(Session["pakli"])-1) LabelUnder.Text +=  ",";}
    }

    protected void Ruka_Click(object sender, ImageClickEventArgs e)
    {
        if (KartaStulTY.ImageUrl.Contains("hrej"))
        {
            object[] obj = { Image1, Image2, Image3, Image4 };
            KartaStulTY.ImageUrl = (sender as ImageButton).ImageUrl;
            for (int i = Convert.ToInt32((sender as ImageButton).ID.Substring(5)) - 1; i < Convert.ToInt32(Session["rukai"]) - 1; i++)
                (obj[i] as ImageButton).ImageUrl = (obj[i + 1] as ImageButton).ImageUrl;
            Session["rukai"] = Convert.ToInt32(Session["rukai"]) - 1;
            Session["comp"] = 1;
            if (Convert.ToInt32(Session["rukai"]) >= 0)
                (obj[Convert.ToInt32(Session["rukai"])] as ImageButton).Visible = false;
            else
                LabelUnder.Text += "Under!";
        } else ButtonDale.Enabled = false;
    }

    protected void ButtonDale_Click(object sender, EventArgs e)
    {
		ButtonDale.Enabled = false;
    }

    protected void ButtonRozdej_Click(object sender, EventArgs e)
    {
        PanelStul.Visible = true;
        ButtonDale.Enabled = false;

        object[] obj = { Image1, Image2, Image3, Image4 };
        Random rnd = new Random(DateTime.Now.Millisecond);
        int[] pakl = new int[22];
        for (int i = 0; i < 22; i++)
            pakl[i] = i;
        for (int i = 0; i < 43; i++)
        {
            int i1 = rnd.Next(22);
            int i2 = rnd.Next(22);
            int zal = pakl[i1];
            pakl[i1] = pakl[i2];
            pakl[i2] = zal;
        }
        for (int i = 0; i < 14; i++)
            Session["pakl" + i.ToString()] = pakl[i];
        Session["pakli"] = 14;
        for (int i = 0; i < 8; i += 2)
        {
            Session["compruka" + (i / 2).ToString()] = pakl[i + 14];
            (obj[i / 2] as ImageButton).Visible = true;
            (obj[i / 2] as ImageButton).ImageUrl = "~/21karty/" + pakl[i + 15].ToString() + ".png";
        }
        KartaStulComp.ImageUrl = "~/21karty/hrej_comp.png";
        KartaStulTY.ImageUrl = "~/21karty/hrej_ty.png";
        Session["comprukai"] = 4;
        Session["rukai"] = 4;
        Session["paklrukai"] = 0;
        Session["paklcompi"] = 0;
    }

    protected void LabelUnder_Load(object sender, EventArgs e)
    {
        (sender as Label).Text = "";
    }

    protected void CheckBoxReport_CheckedChanged(object sender, EventArgs e)
    {
        if ((sender as CheckBox).Checked)
        {
            LabelUnder.Visible = true;
            LabelUnder.Text = "";
        } else LabelUnder.Visible = false;
    }

    protected void LabelPaklComp_PreRender(object sender, EventArgs e)
    {
	(sender as Label).Text="<h2>Computer:<br>"+Session["paklcompi"].ToString()+" cards.</h2>";
    }

    protected void LabelPaklRuka_PreRender(object sender, EventArgs e)
    {
	(sender as Label).Text="<h2>Tvé (your):<br>"+Session["paklrukai"].ToString()+" cards.</h2>";
    }

    protected void TextBoxName_Init(object sender, EventArgs e)
    {
        if (Request["name"] != null) (sender as TextBox).Text = Request["name"];
    }

    protected void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        if ((sender as TextBox).Text!="")
        {
            if (!(sender as TextBox).Text.Contains("--")) (sender as TextBox).Text = "-- " + (sender as TextBox).Text + " --";
            HttpCookie cook = new HttpCookie("name", (sender as TextBox).Text);
            cook.Expires = DateTime.Now.AddYears(1);
            Response.AppendCookie(cook);
        }
    }

    protected void souhlas_Load(object sender, EventArgs e)
    {
       if ((Request["cookies"]!=null)&&Request["cookies"].ToString().Contains("ano")) {souhlas.Visible=false;LabelBR.Visible=false;}
    }

    protected void souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
	if (Request["cookies"]!=null) cook = new HttpCookie("cookies", Request["cookies"].ToString()+"ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
	souhlas.Visible=false;
	LabelBR.Visible=false;
    }
}
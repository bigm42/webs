﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing;

public partial class Statistic : System.Web.UI.Page
{
    protected string Filter()
    {
        string str = "";
        if (TextBoxFilterIP.Text != "") str += " and ip like '%" + TextBoxFilterIP.Text + "%'";
        if (TextBoxFilterName.Text != "") str += " and name like '%" + TextBoxFilterName.Text + "%'";
        if (TextBoxFilterPaklComp.Text != "") str += " and computer like '%" + TextBoxFilterPaklComp.Text + "%'";
        if (TextBoxFilterPaklPlayer.Text != "") str += " and player like '%" + TextBoxFilterPaklPlayer.Text + "%'";
        if (RadioButtonComp.Checked) str += " and win=100";
        if (RadioButtonPlayer.Checked) str += " and win=0";
        if (str.Length > 0) str = "where" + str.Substring(4);
        return str;
    }

    protected void LabelCompWins_PreRender(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select avg(win) from game4upper_wins "+Filter(), conn);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            dr.Read();
            LabelCompWins.Text = "<h4>Computer má úspěšnost (fruitfulness is) " + dr[0].ToString() + "% z nalezených (from find).</h4>";
        }
        finally { conn.Close(); }
    }

    protected void TableVypis_PreRender(object sender, EventArgs e)
    {
        if (Session["starti"] == null) Session["starti"] = 0;
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select count(id) from game4upper_wins " + Filter(), conn);
        try
        {
            conn.Open();
            int radku= Convert.ToInt32(comm.ExecuteScalar());
		Session["radku"] =radku;
		int max=Convert.ToInt32(Session["starti"]) + Convert.ToInt32(TextBoxFilterNum.Text);
		if (max>radku) max=radku;
            LabelNalezeno.Text = Session["radku"].ToString() + " files find. (" + (Convert.ToInt32(Session["starti"])+1).ToString() + " - " + max.ToString() + ")";
            comm = new SqlCommand("select ip,name,win,computer,player from game4upper_wins " + Filter()+" ORDER BY id DESC OFFSET "+Session["starti"].ToString()+" ROWS FETCH NEXT "+Convert.ToInt32(TextBoxFilterNum.Text).ToString()+" ROWS ONLY", conn);
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                    TableRow tr = new TableRow();
                    TableCell tc = new TableCell();
                    Label lb = new Label();
                    lb.Text = dr[0].ToString();
                    tc.Controls.Add(lb);
                    tr.Cells.Add(tc);

                    tc = new TableCell();
                    lb = new Label();
                    lb.Text = dr[1].ToString();
                    tc.Controls.Add(lb);
                    tr.Cells.Add(tc);

                    tc = new TableCell();
                    lb = new Label();
                    if (dr[2].ToString() == "0") { lb.Text = "P"; tc.BackColor = Color.Green; } else { lb.Text = "C"; tc.BackColor = Color.Red; }
                    tc.Controls.Add(lb);
                    tr.Cells.Add(tc);

                    tc = new TableCell();
                    lb = new Label();
                    lb.Text = dr[3].ToString();
                    tc.Controls.Add(lb);
                    tr.Cells.Add(tc);

                    tc = new TableCell();
                    lb = new Label();
                    lb.Text = dr[4].ToString();
                    tc.Controls.Add(lb);
                    tr.Cells.Add(tc);

                    TableVypis.Rows.Add(tr);
            }
        }
        finally { conn.Close(); }

    }

    protected void LinkButtonDown_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["starti"]) - Convert.ToInt32(TextBoxFilterNum.Text) >= 0)
            Session["starti"] = Convert.ToInt32(Session["starti"]) - Convert.ToInt32(TextBoxFilterNum.Text);
	else
	    Session["starti"] = 0;
    }

    protected void LinkButtonUp_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["starti"]) + Convert.ToInt32(TextBoxFilterNum.Text) < Convert.ToInt32(Session["radku"]))
            Session["starti"] = Convert.ToInt32(Session["starti"]) + Convert.ToInt32(TextBoxFilterNum.Text);
    }
}
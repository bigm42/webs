﻿using System;

public partial class _Default : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

	Session["pakli"] = null;

        if (!Request.Url.ToString().Contains("https"))
        {
            int j = 0;
            for (int i=0;i< Request.Url.ToString().Length-14;i++)
                if (Request.Url.ToString().Substring(i, 14) == "game4upper.com") j = i;
            Response.Redirect("https://" + Request.Url.ToString().Substring(j));
        }

	if (Request["help"]!=null) PanelHelp.Visible=true;
	if ((Request["pz"]!=null)&&!Start.NavigateUrl.Contains("pz=1")) {Start.NavigateUrl+="?pz=1";Help.NavigateUrl+="&pz=1";HyperLinkCloseHelp.NavigateUrl+="?pz=1";Back.Visible=true;}
	}

    protected void Torch(object sender, EventArgs e)
    {
	Response.Redirect("https://torch.jaleco.com/");
    }
}
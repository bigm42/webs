﻿using System;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;

public partial class _dEFAULT : System.Web.UI.Page
{
    protected void ChB_OChCh(object sender, EventArgs e)
    {
       ID.Visible=false;
    }

    protected void ISWB_OC(object sender, EventArgs e)
    {
       if (pan.Visible) {ID.Visible=pan.Visible=false;}else{ID.Visible=pan.Visible=true;}
    }

    protected void OI(object sender, EventArgs e)
    {
    Label lb=new Label();
        lb.Text = "==================================<br>";
	pan.Controls.Add(lb);
        DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/pic/"));
        FileInfo[] fi = di.GetFiles();
        if (CheckBox1.Checked)
        {
            string[] str=new string[fi.Length]; 
            str[0]=fi[0].LastWriteTime.ToString("yyyy-MM-dd-HH:mm") +" -- "+fi[0].Name + "<br>";
            for (int i = 1; i < fi.Length; i++)
            {
                int i1 = 0;
                while ((i1 < i) && (str[i1].CompareTo(fi[i].LastWriteTime.ToString("yyyy-MM-dd-HH:mm")) > 0))
                    i1++;
                for (int i2 = i; i2 > i1; i2--)
                    str[i2] = str[i2 - 1];
                str[i1] = fi[i].LastWriteTime.ToString("yyyy-MM-dd-HH:mm") + " -- " + fi[i].Name + "<br>";
            }
            foreach (string st in str)
            {
                HyperLink hl = new HyperLink();
                hl.Text = st;
                hl.NavigateUrl = "~/pic/" + st.Substring(20,st.Length-24);
		hl.ForeColor=ColorTranslator.FromHtml("#B3B3B3");
                pan.Controls.Add(hl);
            }
        }
        else
            foreach (FileInfo fimem in fi)
            {
                HyperLink hl = new HyperLink();
                hl.Text = fimem.Name + " (" + fimem.LastWriteTime.ToString("d.M.yyyy-HH:mm") + ")<br>";
                hl.NavigateUrl = "~/pic/" + fimem.Name;
		hl.ForeColor=ColorTranslator.FromHtml("#B3B3B3");
                pan.Controls.Add(hl);
            }
    lb=new Label();
        lb.Text = "==================================<br>";
	pan.Controls.Add(lb);
    }
}
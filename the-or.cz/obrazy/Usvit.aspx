﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="download_Default" %>

<head runat="server">
    <title>OBRAZY/Usvit</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
	<h2>I. Usvit</h2>
<asp:Label runat="server" Text="www" ForeColor="White"/> Vesel do usti male chodby, jako by nevedel, ktera bije. Rana jako z dela se ozvala o nekolik ulic dal. Lide se zacali zbihat k onomu mistu.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> Nebylo mista vne, jen malo z toho si nyni vybavoval - jedine: ze nebylo mista, ani prostoru. Stale se ptal sam sebe: "To bylo kvuli me?"<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> Pomalu se soural k onomu mistu, shluk lidi ho desil. Bal se zeptat co se stalo, tak tam jen tak stal a dival se na zmatene klokotajici dav.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> "Mel jsem rad jeho pisnicky," rekl hlas nejblize k nemu, "byl to dobry clovek, tohle mu nikdo nepral!" koncilo zhodnoceni skonu slavneho cloveka.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> "Jak se jmenovala obet'?" zmohl se Petr na otazku.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> "Michal Michal," odvetil nejblizsi clovek, "Michal Michal."<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> "Ani jsem ho neznal," prohodil nevedomky Petr.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> "Nebyl z nejslavnejsich," odtusil nejblizsi.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> "... jako bych ho znal odjakziva." zhodnotil Petr vinu plynouci odnikud. Zacal se bat, ze je psychicky chory.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> Pockal slusne do prijezdu sanitky a dal se na odchod. Bombove atentaty nema rad.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> Usel par bloku a zjistil, ze se musi vratit, proste vratit. Kdyz dosel na misto, kde zkonal umelec Michal Michal, nikdo tam nebyl.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> "To uz neni slavny?" ptal se Petr sam sebe.<br/>
<asp:Label runat="server" Text="www" ForeColor="White"/> Pak si ale vsiml, ze ohorele je misto, kde se nachazel, kdyz slysel vybuch. "A sakra!" pomyslel si a zmizel.
<br/>
<br/>(c) LP2015 Martin Kreag<br/>
    </div>
    </form>
</body>
</html>

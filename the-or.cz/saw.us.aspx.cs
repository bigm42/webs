﻿using System;

public partial class saw_us : System.Web.UI.Page
{
    protected void LabelOdpocet_OnInit(object sender, EventArgs e)
    {
        TimeSpan kmkz = (new DateTime(2018, 4, 6, 23, 59, 0)) - DateTime.Now;
        if (kmkz.Days>0)
            LabelOdpocet.Text = "End of Shredinger's law is incoming in " + kmkz.Days.ToString() + " days!";
        if (kmkz.Days == 0 && kmkz.Minutes > -1)
            LabelOdpocet.Text = "Have nice day!";
    }
}
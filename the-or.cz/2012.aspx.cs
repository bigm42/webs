﻿using System;

public partial class _2012 : System.Web.UI.Page
{
    protected void LabelOdpocet_OnInit(object sender, EventArgs e)
    {
        TimeSpan kmkz = (new DateTime(2012, 12, 12, 23, 59, 0)) - DateTime.Now;
        if (kmkz.Days>0)
            LabelOdpocet.Text = "Konec máyského kalendáře v(e) " + kmkz.Days.ToString() + " dnech!";
        if (kmkz.Days == 0 && kmkz.Minutes > -1)
            LabelOdpocet.Text = "Konec máyského kalendáře je dnes!";
    }
}
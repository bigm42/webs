﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="zk.aspx.cs" Inherits="zk" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Žákovská knížka tohoto světa</title>
<link rel="icon" href="http://www.the-or.cz/hammer.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="http://www.the-or.cz/hammer.ico" type="image/x-icon" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
<asp:Panel runat="server" HorizontalAlign="Center">
<h1>Žákovská knížka tohoto světa</h1>
<asp:Table runat="server" HorizontalAlign="Center" BorderStyle="Solid" BorderWidth="1px" GridLines="Both" BorderColor="Black">

<asp:TableRow><asp:TableCell HorizontalAlign="Left" Width="100">
22.5.2018
</asp:TableCell><asp:TableCell HorizontalAlign="Left" Width="400">
Dnes si nasrali do bot.
</asp:TableCell><asp:TableCell HorizontalAlign="Center" Width="100">
<asp:Image runat="server" ImageUrl="~/parafa.png" Width="55"/>
</asp:TableCell></asp:TableRow>

<asp:TableRow><asp:TableCell HorizontalAlign="Left" Width="100">
24.5.2018
</asp:TableCell><asp:TableCell HorizontalAlign="Left" Width="400">
Notně se polepšili. Dokonce si občas i boty vyčistili.
</asp:TableCell><asp:TableCell HorizontalAlign="Center" Width="100">
<asp:Image runat="server" ImageUrl="~/parafa.png" Width="44"/>
</asp:TableCell></asp:TableRow>

<asp:TableRow><asp:TableCell HorizontalAlign="Left" Width="100">
17.9.2018
</asp:TableCell><asp:TableCell HorizontalAlign="Left" Width="400">
Zákon v praxi.
</asp:TableCell><asp:TableCell HorizontalAlign="Center" Width="100">
<asp:Table runat="server"><asp:TableRow><asp:TableCell>1</asp:TableCell><asp:TableCell><asp:Image runat="server" ImageUrl="~/parafa.png" Width="40"/></asp:TableCell></asp:TableRow></asp:Table>
</asp:TableCell></asp:TableRow>

</asp:Table>
Další poznámky s datem posílejte na mitti@white-magic.net .
</asp:Panel>
    </div>
    </form>
</body>
</html>

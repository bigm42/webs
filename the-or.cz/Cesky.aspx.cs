﻿using System;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;


public partial class Cesky : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

	zvuk.Visible=false;
}

    protected void Link_PreRender(object sender, EventArgs e)
    {
	if (Request.Url.OriginalString.Contains("?"))
	{
		int i=0;
		while (Request.Url.OriginalString[i]!='?') i++;
		(sender as HyperLink).NavigateUrl="~/Default.aspx?set=1&"+Request.Url.OriginalString.Substring(i+1);
	}
	else
		(sender as HyperLink).NavigateUrl="~/Default.aspx?set=1";

	if (CheckBox1.Checked&&(!(sender as HyperLink).NavigateUrl.Contains("date")))
		(sender as HyperLink).NavigateUrl+="&date=1";
}

    protected void Zvuk_Click(object sender, EventArgs e)
    {
	zvuk.Visible=true;
}

    protected string UpDir(string dir)
    {
        if (dir.Equals("~/pic/"))
            return "~/pic/";
        else
        {
            int i = dir.Length - 2;
            while (dir[i] != '/')
                i--;
            return dir.Substring(0, i + 1);
        }
    }
    
    protected void pan_PR(object sender, EventArgs e)
    {
        DirectoryInfo di2 = new DirectoryInfo(Server.MapPath(LabelDir.Text));
        FileInfo[] fi = di2.GetFiles();
        DirectoryInfo[] di = di2.GetDirectories();
            if (!LabelDir.Text.Equals("~/pic/"))
            {
                HyperLink hl = new HyperLink();
                hl.Text = "[..]<br>";
                hl.NavigateUrl = "~/Cesky.aspx?dir=" + UpDir(LabelDir.Text);
                if (CheckBox1.Checked) hl.NavigateUrl+="&date=1";
                pan.Controls.Add(hl);
            }
        if (CheckBox1.Checked)
        {
            string[] str = new string[fi.Length + di.Length];
            for (int i = 0; i < fi.Length; i++)
                str[i] = fi[i].LastWriteTime.ToString("yyyy-MM-dd-HH:mm") + " -- " + fi[i].Name + "<br>";
            for (int i = 0; i < di.Length; i++)
                str[i + fi.Length] = di[i].CreationTime.ToString("yyyy-MM-dd-HH:mm") + " -- !složka! " + di[i].Name + "<br>";
            for (int i = 1; i < fi.Length + di.Length; i++)
            {
                int i1 = 0; string strp = str[i];
                while ((i1 < i) && (str[i1].CompareTo(strp) > 0))
                    i1++;
                for (int i2 = i; i2 > i1; i2--)
                    str[i2] = str[i2 - 1];
                str[i1] = strp;
            }
            foreach (string st in str)
            {
                HyperLink hl = new HyperLink();
                hl.Text = st;
                if (st.Contains("!složka!"))
                {
                    hl.NavigateUrl = "~/Cesky.aspx?dir=" + LabelDir.Text + st.Substring(29, st.Length - 33) + "/&date=1";
                    hl.ForeColor = ColorTranslator.FromHtml("#c8ab37");
                }
                else
                    hl.NavigateUrl = LabelDir.Text + st.Substring(20, st.Length - 24);
                pan.Controls.Add(hl);
            }
        }
        else
        {
            foreach (DirectoryInfo dimem in di)
            {
                HyperLink hl = new HyperLink();
                hl.Text = "!složka! " + dimem.Name + " (" + dimem.CreationTime.ToString("d.M.yyyy-HH:mm") + ")<br>";
                hl.NavigateUrl = "~/Cesky.aspx?dir=" + LabelDir.Text + dimem.Name + "/";
                hl.ForeColor = ColorTranslator.FromHtml("#c8ab37");
                pan.Controls.Add(hl);
            }
            foreach (FileInfo fimem in fi)
            {
                HyperLink hl = new HyperLink();
                hl.Text = fimem.Name + " (" + fimem.LastWriteTime.ToString("d.M.yyyy-HH:mm") + ")<br>";
                hl.NavigateUrl = LabelDir.Text + fimem.Name;
                pan.Controls.Add(hl);
            }
        }
    }

    protected void LabelDir_Load(object sender, EventArgs e)
    {
        if (Request["dir"] != null) LabelDir.Text = Request["dir"];
    }

    protected void CheckBox1_Load(object sender, EventArgs e)
    {
        if (Request["date"] != null) 
		CheckBox1.Checked=true;
    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (Request.Url.ToString().Contains("&date=1")) Response.Redirect("~/Cesky.aspx?dir="+LabelDir.Text);
    }
}
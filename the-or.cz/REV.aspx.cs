﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing;

public partial class REV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string BezMezer(string str)
    {
        int i=0;
        while (i < str.Length)
        {
            if (str[i] == ' ')
                str = str.Substring(0, i) + str.Substring(i + 1);
            else
                i++;
        }
        return str;
    }
    
    protected void ButtonPost_OnClick(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("insert into the_or_forum(usr,text,time) values(@usr,@text,@time)", conn);
        if (BezMezer(TextBoxUser.Text) == "")
            TextBoxUser.Text = "*nobody*";
        comm.Parameters.Add("@usr",TextBoxUser.Text);
        comm.Parameters.Add("@text",TextBoxComm.Text);
        comm.Parameters.Add("@time",DateTime.Now);
        try
        {
            conn.Open();
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
        TextBoxComm.Text = "";TextBoxUser.Text = "";
    }

    protected void PanelComm_OnPreRender(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select usr,text,time from the_or_forum", conn);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Label lb = new Label();
                lb.Text = dr["time"].ToString() + "(Prague time) ";
                PanelComm.Controls.Add(lb);
                lb = new Label();
                lb.ForeColor=Color.FromArgb(int.Parse("000066", System.Globalization.NumberStyles.HexNumber));
		lb.Font.Bold=true;
                lb.Text = dr["usr"].ToString() + ": ";
                PanelComm.Controls.Add(lb);
                lb = new Label();
                lb.Text = dr["text"].ToString() + "<br><br>";
                PanelComm.Controls.Add(lb);
            }
            dr.Close();
        }
        finally { conn.Close(); }
    }
    
    protected void ipButton_Click(object sender, EventArgs e)
    {
        bool count=false;
	SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select c from the_or_rev_ips where ip=@ip", conn);
        comm.Parameters.Add("@ip",Request.UserHostAddress);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            if (dr.Read()) count=true;
	    dr.Close();	
        }
        finally { conn.Close(); }
        if (!count)
		comm = new SqlCommand("insert into the_or_rev_ips(ip,c) values(@ip,1)", conn);
	else
		comm = new SqlCommand("update the_or_rev_ips set c=c+1 where ip=@ip", conn);
        comm.Parameters.Add("@ip",Request.UserHostAddress);
        try
        {
            conn.Open();
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }

	ipLabel.Visible=false;ipButton.Enabled=false;secret.Visible=true;
    }
    
    protected void ipLabel_Load(object sender, EventArgs e)
    {
	SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select ip,c from the_or_rev_ips", conn);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
		ipLabel.Text+=dr[1].ToString()+"x("+dr[0].ToString()+") ";
	    dr.Close();	
        }
        finally { conn.Close(); }
    }
}
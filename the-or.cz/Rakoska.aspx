﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Rakoska.aspx.cs" Inherits="Rakoska" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Spolek Za Navrácení Rákosky Do Škol</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Font-Size="Large" Font-Bold="True" Font-Underline="True">Spolek Za Navrácení Rákosky Do Škol</asp:Label>
        <br /> <br /> <br /> <br />
        <asp:Label ID="Label2" runat="server" > Myslím, že pod vlivem nového svobodného pocitu, který z nás všech čiší ;o), nám magoří mládež. Situace učitelů na Základních školách je čím dál horší. Mají 
        jenom jedny nervy a proto navrhuji návrat rákosky do škol. Stačí to jen prosadit. Myslím, že to vyjde (možná za sto let, ale někdy se začít musí). Děkuji za pozornost, přihlášky a diskuzi směrujte na
        <a href="mailto:mitti@white-magic.net">mitti@white-magic.net</a> .</asp:Label>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class kino_Provoz : System.Web.UI.Page
{
    protected string BR(string str)
    {
        for (int i = 0; i < str.Length-3; i++)
            if (str.Substring(i, 4) == "<br>")
                str = str.Substring(0, i) + "\r\n" + str.Substring(i + 4);
        return str;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Session[Constants_kino.LoginIdStr] == null)
            Response.Redirect(Constants_kino.UrlStarts + "/Registrace.aspx", true);
        else
        {
            SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
            try
            {
                conn.Open();
                string commStr = "SELECT     kino_walk_register.things_done AS things_done, kino_rooms.id AS room_id, kino_rooms.view_str AS room_str, kino_persons.view_str AS person_str, kino_things.view_str AS thing_str, kino_rooms.s, kino_rooms.j, kino_rooms.v, kino_rooms.z";
                commStr += " FROM         kino_walk_register LEFT JOIN";
                commStr += "           kino_rooms ON kino_walk_register.room = kino_rooms.id LEFT JOIN";
                commStr += " kino_persons ON kino_rooms.person = kino_persons.id LEFT JOIN";
                commStr += " kino_things ON kino_rooms.thing = kino_things.id";
                commStr += " WHERE     (kino_walk_register.user_id = @id)";
                SqlCommand comm = null;
                SqlDataReader r = null;
loop:           comm = new SqlCommand(commStr, conn);
                comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
                r=comm.ExecuteReader();
                bool createUser=false;
                bool level = false;
                if (r.Read())
                {
                    string str = BR(r["room_str"].ToString());
                    switch (str.Substring(0, 2))
                    {
                        case "L&":
                            level = true;
                            Session[Constants_kino.LevelStr] = 1;
                            str = str.Substring(2, str.Length - 2);
                            break;
                        case "B&":
                            Session[Constants_kino.PointUpStr] = 1;
                            str = str.Substring(2, str.Length - 2);
                            break;
                    }
                    if (Convert.ToInt32(Session[Constants_kino.RoomIdStr]) != Convert.ToInt32(r["room_id"]))
                        TextBoxLog.Text=TextBoxLog.Text+ str + "\r\n \r\n";
                    Session[Constants_kino.RoomIdStr] = r["room_id"];

                    LabelThingsDone.Text = "Důležitých věcí uděláno: " + r["things_done"].ToString();
                    if (r["person_str"] == DBNull.Value)
                    {
                        ButtonTalk.Enabled = false;
                        ButtonTalk.Text = "Mluvit s";
                    }
                    else
                    {
                        ButtonTalk.Enabled = true;
                        ButtonTalk.Text = "Mluvit s "+r["person_str"].ToString();
                    }
                    if (r["thing_str"] == DBNull.Value)
                    {
                        ButtonUse.Enabled = false;
                        ButtonUse.Text = "Použít";
                    }
                    else
                    {
                        ButtonUse.Enabled = true;
                        ButtonUse.Text = "Použít " + r["thing_str"].ToString();
                    }
                    if (r["s"] == DBNull.Value)
                    {
                        ButtonS.Enabled = false;
                        Session[Constants_kino.SStr] = 0;
                    }
                    else
                    {
                        ButtonS.Enabled = true;
                        Session[Constants_kino.SStr] = r["s"];
                    }
                    if (r["j"] == DBNull.Value)
                    {
                        ButtonJ.Enabled = false;
                        Session[Constants_kino.JStr] = 0;
                    }
                    else
                    {
                        ButtonJ.Enabled = true;
                        Session[Constants_kino.JStr] = r["j"];
                    }
                    if (r["v"] == DBNull.Value)
                    {
                        ButtonV.Enabled = false;
                        Session[Constants_kino.VStr] = 0;
                    }
                    else
                    {
                        ButtonV.Enabled = true;
                        Session[Constants_kino.VStr] = r["v"];
                    }
                    if (r["z"] == DBNull.Value)
                    {
                        ButtonZ.Enabled = false;
                        Session[Constants_kino.ZStr] = 0;
                    }
                    else
                    {
                        ButtonZ.Enabled = true;
                        Session[Constants_kino.ZStr] = r["z"];
                    }
                 }
                else
                    createUser = true;
                r.Close();
                
                if (createUser)
                {
                    comm = new SqlCommand("insert into kino_walk_register(user_id,room,level) values(@id,@sroom,@sroom)", conn);
                    comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
                    comm.Parameters.Add("@sroom", Constants_kino.StartRoom);
                    comm.ExecuteNonQuery();
                    goto loop;
                }
                if (level)
                {
                    comm = new SqlCommand("update kino_walk_register set level=room, things_done=0 where user_id=@id  delete from kino_things_done where user_id=@id", conn);
                    comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
                    comm.ExecuteNonQuery();
                }
            }
            finally { conn.Close(); }
        }
    }

    protected void ButtonGameStart_OnClick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session[Constants_kino.RoomIdStr]) == Constants_kino.StartRoom)
            Session[Constants_kino.RoomIdStr] = 0;
        TextBoxLog.Text = "";
        RadioButtonListTalk.Items.Clear();
        SqlConnection conn=new SqlConnection(Constants_kino.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand("update kino_walk_register set room=@sroom,level=@sroom,things_done=0 where user_id=@id  delete from kino_things_done where user_id=@id", conn);
            comm.Parameters.Add("@sroom", Constants_kino.StartRoom);
            comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
    }

    protected void ButtonLevelStart_OnClick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session[Constants_kino.LevelStr]) == 1)
            Session[Constants_kino.RoomIdStr] = 0;
        TextBoxLog.Text = "";
        RadioButtonListTalk.Items.Clear();
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand("update kino_walk_register set room=level,things_done=0 where user_id=@id  delete from kino_things_done where user_id=@id", conn);
            comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
    }

    protected void ZmenRoom(int room)
    {
        TextBoxLog.Text = "";
        if (Convert.ToInt32(Session[Constants_kino.LevelStr]) == 1)
            Session[Constants_kino.LevelStr] = 0;
        string str1 = "";
        if (Convert.ToInt32(Session[Constants_kino.PointUpStr]) == 1)
        {
            str1 = "score=score+1,";
            Session[Constants_kino.PointUpStr] = 0;
        }
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand("update kino_walk_register set " + str1 + "room=@room where user_id=@id", conn);
            comm.Parameters.Add("@room", room);
            comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
    }

    protected void ButtonUse_OnClick(object sender, EventArgs e)
    {
        List<int> l = new List<int>();
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm1 = new SqlCommand("select thing_id from kino_things_done where user_id=@id", conn);
            comm1.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
            SqlDataReader r1 = comm1.ExecuteReader();
            while (r1.Read())
                l.Add(Convert.ToInt32(r1[0]));
            r1.Close();
            int[] things_done = l.ToArray();

            RadioButtonListTalk.Items.Clear();
            string commStr = "SELECT     kino_things.after_str, kino_things.block_room, kino_things.view_str, kino_things.things_needed, kino_walk_register.things_done, kino_things.id as thing_id";
            commStr += " FROM         kino_walk_register LEFT JOIN";
            commStr += "                       kino_rooms ON kino_walk_register.room = kino_rooms.id LEFT JOIN";
            commStr += "            kino_things ON kino_rooms.thing = kino_things.id";
            commStr += " WHERE     (kino_walk_register.user_id = @id)";
            int thing_id = 0;
            SqlCommand comm = new SqlCommand(commStr, conn);
            comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
            SqlDataReader r = comm.ExecuteReader();
            r.Read();
            bool ThingsDoneInc = false;
            bool ImpToZero = false;
            if (r["block_room"] != DBNull.Value)
                ZmenRoom(Convert.ToInt32(r["block_room"]));
            if (r["things_needed"] == DBNull.Value)
            {
                TextBoxLog.Text = TextBoxLog.Text + "Použils " + r["view_str"].ToString() + " s výsledkem: " + BR(r["after_str"].ToString()) + "\r\n \r\n";
            }
            else
                if (Convert.ToInt32(r["things_needed"]) == 0)
                {
                    thing_id = Convert.ToInt32(r["thing_id"]);
                    ThingsDoneInc = true;
                    for (int i = 0; i < things_done.Length; i++)
                        if (thing_id == things_done[i])
                            ThingsDoneInc = false;
                    TextBoxLog.Text = TextBoxLog.Text + "Použils " + r["view_str"].ToString() + " s výsledkem: " + BR(r["after_str"].ToString()) + "\r\n \r\n";
                }
                else
                    if (Convert.ToInt32(r["things_needed"]) == Convert.ToInt32(r["things_done"]))
                    {
                        if (r["block_room"] != DBNull.Value)
                            ZmenRoom(Convert.ToInt32(r["block_room"]));
                        TextBoxLog.Text = TextBoxLog.Text + "Použils " + r["view_str"].ToString() + " s výsledkem: " + BR(r["after_str"].ToString()) + "\r\n \r\n";
                        ImpToZero = true;
                    }
                    else
                        TextBoxLog.Text += "Málo důležitých věcí uděláno, ještě musíš vykonat " + (Convert.ToInt32(r["things_needed"]) - Convert.ToInt32(r["things_done"])).ToString() + " věcí.\r\n\r\n";
            r.Close();
            if (ThingsDoneInc)
            {
                comm = new SqlCommand("update kino_walk_register set things_done=things_done+1 where user_id=@id  insert into kino_things_done(thing_id,user_id) values(@tid,@id)", conn);
                comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
                comm.Parameters.Add("@tid", thing_id);
                comm.ExecuteNonQuery();
            }
            if (ImpToZero)
            {
                comm = new SqlCommand("update kino_walk_register set things_done=0 where user_id=@id  delete from kino_things_done where user_id=@id", conn);
                comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
                comm.ExecuteNonQuery();
            }
        }
        finally { conn.Close(); }
    }

    protected void ButtonTalk_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        string commStr = "SELECT     kino_computer_talk.str AS c_talk, kino_person_talk.str AS p_talk1, kino_person_talk_2.str AS p_talk3, kino_person_talk_1.str AS p_talk2,";
        commStr += "               kino_person_talk.c1 AS cid1, kino_person_talk_2.c1 AS cid3, kino_person_talk_1.c1 AS cid2, kino_computer_talk.room AS room, kino_persons.view_str AS pstr";
        commStr += " FROM         kino_walk_register LEFT JOIN";
        commStr += "               kino_rooms ON kino_walk_register.room = kino_rooms.id LEFT JOIN";
        commStr += "               kino_persons ON kino_rooms.person = kino_persons.id LEFT JOIN";
        commStr += "               kino_computer_talk ON kino_persons.ctalk_id = kino_computer_talk.id LEFT JOIN";
        commStr += "              kino_person_talk ON kino_computer_talk.p1 = kino_person_talk.id LEFT JOIN";
        commStr += "              kino_person_talk AS kino_person_talk_1 ON kino_computer_talk.p2 = kino_person_talk_1.id LEFT JOIN";
        commStr += "              kino_person_talk AS kino_person_talk_2 ON kino_computer_talk.p3 = kino_person_talk_2.id";
        commStr += " WHERE     (kino_walk_register.user_id = @id)";
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand(commStr, conn);
            comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
            SqlDataReader r = comm.ExecuteReader();
            r.Read();
            TextBoxLog.Text = TextBoxLog.Text + "Mluvíš s " + r["pstr"].ToString() + ":\r\nON: " + BR(r["c_talk"].ToString()) + "\r\n \r\n";
            Session[Constants_kino.MluvciStr] = r["pstr"].ToString();
            if (r["room"] == DBNull.Value)
            {
                if (r["p_talk1"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk1"].ToString(), r["cid1"].ToString()));
                if (r["p_talk2"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk2"].ToString(), r["cid2"].ToString()));
                if (r["p_talk3"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk3"].ToString(), r["cid3"].ToString()));
            }
            else
            {
                ZmenRoom(Convert.ToInt32(r["room"]));
                TextBoxLog.Text = "Mluvil si s " + r["pstr"].ToString() + ":\r\nON: " + r["c_talk"].ToString() + "\r\n \r\n";
            }
            r.Close();
        }
        finally { conn.Close(); }
    }

    protected void RadioButtonListTalk_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonListTalk.SelectedValue == "")
        {
            TextBoxLog.Text = TextBoxLog.Text + "TY: " + RadioButtonListTalk.SelectedItem.Text + "\r\n \r\n";
            RadioButtonListTalk.Items.Clear();
        }
        else
        {
            string commStr = "SELECT     kino_computer_talk.str AS c_talk, kino_computer_talk.room, kino_person_talk.str AS p_talk1, kino_person_talk_1.str AS p_talk2,";
            commStr += " kino_person_talk_2.str AS p_talk3, kino_person_talk.c1 AS cid1, kino_person_talk_1.c1 AS cid2, kino_person_talk_2.c1 AS cid3";
            commStr += " FROM         kino_computer_talk LEFT JOIN";
            commStr += "                       kino_person_talk ON kino_computer_talk.p1 = kino_person_talk.id LEFT JOIN";
            commStr += "                       kino_person_talk AS kino_person_talk_1 ON kino_computer_talk.p2 = kino_person_talk_1.id LEFT JOIN";
            commStr += "                       kino_person_talk AS kino_person_talk_2 ON kino_computer_talk.p3 = kino_person_talk_2.id";
            commStr += " WHERE     (kino_computer_talk.id = @id)";
            SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
            try
            {
                conn.Open();
                SqlCommand comm = new SqlCommand(commStr, conn);
                comm.Parameters.Add("@id", Convert.ToInt32(RadioButtonListTalk.SelectedValue));
                SqlDataReader r = comm.ExecuteReader();
                r.Read();
                TextBoxLog.Text = TextBoxLog.Text.Substring(0, TextBoxLog.Text.Length - 4);
                string str = RadioButtonListTalk.SelectedItem.Text;
                RadioButtonListTalk.Items.Clear();
                if (r["room"] == DBNull.Value)
                {
                    if (r["p_talk1"] != DBNull.Value)
                        RadioButtonListTalk.Items.Add(new ListItem(r["p_talk1"].ToString(), r["cid1"].ToString()));
                    if (r["p_talk2"] != DBNull.Value)
                        RadioButtonListTalk.Items.Add(new ListItem(r["p_talk2"].ToString(), r["cid2"].ToString()));
                    if (r["p_talk3"] != DBNull.Value)
                        RadioButtonListTalk.Items.Add(new ListItem(r["p_talk3"].ToString(), r["cid3"].ToString()));
                }
                else
                {
                    ZmenRoom(Convert.ToInt32(r["room"]));
                    TextBoxLog.Text = "Mluvil si s " + Session[Constants_kino.MluvciStr].ToString() + ":\r\n";
                }
                TextBoxLog.Text = TextBoxLog.Text + "TY: " + str + "\r\nON: " + BR(r["c_talk"].ToString()) + "\r\n \r\n";
                r.Close();
            }
            finally { conn.Close(); }
        }
    }

    protected void ButtonS_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constants_kino.SStr]));
    }

    protected void ButtonV_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constants_kino.VStr]));
    }

    protected void ButtonZ_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constants_kino.ZStr]));
    }

    protected void ButtonJ_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constants_kino.JStr]));
    }

    protected void ButtonLogin_OnClick(object sender, EventArgs e)
    {
        Response.Redirect(Constants_kino.UrlStarts + "/Registrace.aspx");
    }
}
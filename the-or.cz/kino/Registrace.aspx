﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registrace.aspx.cs" Inherits="kino_Registrace" %>

<head runat="server">
    <title>Registrace / kino / Registrace</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="LabelVyvoj" runat="server" OnInit="LabelVyvoj_OnInit" Font-Size="Larger" ForeColor="#FF3300">
        Hra je tevprv ve vývoji, pokud Vás začátek zaujal a chcete vědět, kdy ji zprovozním, zašlete email na 
        <a href="mailto:kecal@textovka.net?subject=Chci zpravu o uvedeni do provozu Divnosti Tajemstvi">mm@textovka.net</a>, 
        budete informováni v den zprovoznění.</asp:Label>

        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonOK">
        <center>
            <asp:Label ID="Label2" runat="server" Text="Přihlášení a registrace (2v1)" Font-Underline="True" Font-Size="X-Large" Font-Bold="True"></asp:Label>
            <asp:Table ID="Table1" runat="server" >
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">Jméno uživatele:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="TextBoxUser" runat="server" MaxLength="50"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">Heslo:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="TextBoxPass" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:CheckBox ID="CheckBoxChangePass" runat="server" Text="Změnit heslo u stávajícího účtu" />  
            <br />          
            <asp:Button ID="ButtonOK" runat="server" Text="Potvrdit" OnClick="ButtonOK_OnClick"/>
            <br />
            <asp:Label ID="LabelError" runat="server" Text="" ForeColor="#FF3300"></asp:Label>
            <br />          
            <br />          
            <br />          
            <br /> 
		                 <asp:HyperLink ID="Back" runat="server" ForeColor="Black" NavigateUrl="http://the-or.cz/" >Zpátky na the-or.cz</asp:HyperLink><br />       
        </center></asp:Panel>
    </div>
    </form>
</body>
</html>

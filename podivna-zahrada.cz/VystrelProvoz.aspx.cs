﻿using System;
using System.Web;
using System.Web.UI.WebControls;

public partial class VystrelProvoz : System.Web.UI.Page
{
    protected void souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) souhlas.Visible = false;
    }

    protected void souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        souhlas.Visible = false;
    }

    protected void LabelPal_PreRender(object sender, EventArgs e)
    {
        Random rnd = new Random(DateTime.Now.Millisecond);
        (sender as Label).Text = "";
        if (rnd.Next(5) == 0)
            (sender as Label).Text = "PAL";
        else
        {
            byte[] b = new byte[3];
            for (int i = 0; i <= 2; i++) b[i] = Convert.ToByte(65 +rnd.Next(20));
            char[] ch = new char[3];
            ch = System.Text.Encoding.ASCII.GetChars(b);
            for (int i = 0; i <= 2; i++) (sender as Label).Text = (sender as Label).Text + ch[i];
        }
}

    protected void ButtonStop_Click(object sender, EventArgs e)
    {
        if (LabelPal.Text == "PAL")
            Session["hit"] = 1;
        else
            Session["hit"] = 0;
        Response.Redirect("~/Vystrel.aspx");
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Sloupek.aspx.cs" Inherits="Sloupek" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Podivná zahrada</title>
<link rel="icon" href="~/kan-li_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/kan-li_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/kan-li_logo.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Martin Míka"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Slopek pro vaše počteníčko."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9
	">
    <form id="form1" runat="server">
        <div>
            <asp:Table ID="Basic" runat="server" HorizontalAlign="Center"><asp:TableRow><asp:TableCell HorizontalAlign="Center">
       <br /><br />
        <h1>Sloupek.</h1>
            <br />
            <asp:Panel ID="Sloupek1" runat="server" HorizontalAlign="Justify" Width="400" Visible="false">
31.3.2018<br /><br />
Drak vyšel. Hymna se ozvala. V Čechách je zase dobře. Jde to sice od 
počátku, ale ouroda je doma, pod střechou.<br />
Drak vytáhl kartičku a povídá: „Podpisy za vstup středověku 
na naše uzemí se množí.‟ Ale to nevadí - vlak jede dál.<br />
Jsou to ale blázni, co? Středověk! Výmysl vědce, aby mohl lépe vládnout. Musíme toho 
spoustu vypnout, ale vlak a elekroauto si necháme - káže drak. Letadlo je blbost,
signál taky, obé nám akorát blbne hlavu, ale vlak - to je něco - pravý základ.
No nic - říká náš původce drak - to vyřeší budoucnost. Snažme se jim nezavdávat příčinu 
k převratu a žijme !slušně!. V úctě našemu Jedimu, který !slušně! vymyslel.<br />
Útlak je něco, co se rozpadne přesně ve chvíli, kdy se tomu vysmějete. Ti, kdož chtějí
utlačovat pak přistupují k násilí. To je něco, čemu už se nějde vysmát, ale lze to 
ingnorovat a v dobách minulích šlo využít disidentství, díky tak šťastnému kapitálnímu 
rozkolu Evropy, nebo dnes undergroundu (dětem internacionály). Dnešní život v Evropě spěje
k udání, tak to udejme pomalu a popořadě, ať na nic nezapomenem, nebo se někde nespletem.
Tak vám přeji ať to !slušně! zařídíte.<br />
            </asp:Panel>

            <asp:Panel ID="Sloupek2" runat="server" HorizontalAlign="Justify" Width="400">
10.6.2018<br /><br />
<h2>O malém dráčkovi</h2>
Jednou z lesa, domů se nesa, moudrý Ezop. A potká draka: „Potřeboval bych, aby si se postaral o mé nemluvně,‟ objeví se Ezopovi
v hlavě slova. „Tak jo, draka jsem ještě nevychovával, co se mi může stát,‟ odvětí Ezop a odnese si dračí vejce domů.<br />
Po dvou dnech se drak vylíhl. Ezop ještě spal. Drak vyšel za humna - ozvala se hymna Bohémie - a vedle něj se objevila žena.
„Nový?‟ zeptala se, „tak to si Ezopa dáme, taková odvaha - přijmout draka do útulku, Valasi čiň!‟ Valas přilít, inicioval draka, že
zazářil a Ezopovi zmizel barák i s Ezopem. Místo něj tady nejednou stálo horské ubočí se slují a v ní drak velký jako jeho rodič.
„Byl dobrý, ten Ezop, - moc jsme si to užili - mé mládí a tak. Ten po tom toužil - zrodit mě,‟ poznamenal drak Ezopovým přátelům...
<br />
<br />
Osudy lidí a draků od pradávna váže divnost - divnost Tajemství - možná, že se rodí takhle, možná jinak. To je věc fantasie a ne
naše. Věřme víc svým zvířatům než-li sobě. O co více chceme bojovat o jejich práva, o to mín je chápem - co doopravdy chtějí.
Myslím, že testy na krysách krysy přímo vyžadují, a moc se jim nechce bydlet s námi. Prostě, co se týká přírody, nikdo neví kdo
je otec a kdo syn, a koho co zblajzne, či použije...<br />
Tak sláva Ezopovi - zrodil draka.<br />
            </asp:Panel>

        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz .</asp:HyperLink><br />
            <br /><br />
            <br /><br />
           <asp:HyperLink runat="server" ForeColor="DarkGray" NavigateUrl="~/Sloupek.aspx?sl=1">31.3.2018</asp:HyperLink>
           <asp:Label runat="server" ForeColor="DarkGray" Text=" --- "/>
           <asp:HyperLink runat="server" ForeColor="DarkGray" NavigateUrl="~/Sloupek.aspx?sl=2">10.6.2018</asp:HyperLink>
            </asp:TableCell></asp:TableRow></asp:Table>
        </div>
    </form>
</body>
</html>

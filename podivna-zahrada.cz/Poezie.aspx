﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Poezie.aspx.cs" Inherits="Poezie" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Zprávníčkova poezie.</title>
<link rel="icon" href="~/kan-li_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/kan-li_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/kan-li_logo.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
<meta name="author" content="Mistr Kan-Li.">
<meta name="ROBOTS" content="index, follow">
<meta name="description" content="Mistrova poezie.">
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost">
</head>

<body style="background-color:#e9e9e9
	">
<embed height="0px" width="0px" src="život se vrací odkuď se vzal.wav" />  
  <form id="form1" runat="server">
    <div>
         <br /><br /><br /><br />
         
         <asp:Table runat="server" HorizontalAlign="Center"><asp:TableRow><asp:TableCell Width="700" VerticalAlign="Top" HorizontalAlign="Center">
         
	<h2>Poezie!</h2>
	<asp:Image ID="ImageMain"  ImageUrl="~/image2992.png" runat="server" Width="700"></asp:Image>
        <br />
        <br />
        <br />
        (c) 2014 Krev <br />
		<asp:HyperLink ID="HyperLinkOriginal" runat="server" ForeColor="Black" NavigateUrl="~/Original.aspx?file=IMG_2063.jpg&content=image/jpeg" >Original.</asp:HyperLink>
<br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky.</asp:HyperLink>
         
   </asp:TableCell>
   <asp:TableCell Width="400" ></asp:TableCell>
   <asp:TableCell HorizontalAlign="Center">
   
       
	<asp:Panel runat="server" Width="300" HorizontalAlign="Left">
	
	<h3>Mužská láska!</h3>
	Mám rád věci tvé,<br />
	z věcí tvých se často zvrací,<br />
	mám rád bolest naší,<br />
	tím vše tvé se ke mě vrací.<br /><br /><br />

		<h3>Lítost!</h3>
	Mám rád vůni tvé kůže,	<br />
	mám rád když zebe mě růže,<br />
	mám rád okrasu tvých okras - tedy sebe.<br />
	Když nemiluji - čím dál víc zebe!<br /><br /><br />	

		<h3>Horší!</h3>
	Okrasou mé okrasy,<br />
	okrasou jseš ty,<br />
	občas dávám vál okrasy,<br />
	návdavkem i nýty!<br /><br /><br />

		<h3>KREV!</h3>
        Vůně line se z touhy,<br />
	kde hledat mám život svůj,<br />
	vývoj je vždy sen pouhý,<br />
	já a ty jsme vždy jen tvůj.<br /><br /><br />
</asp:Panel>


   </asp:TableCell></asp:TableRow></asp:Table>
   
    </div>
    </form>
</body>

</html>

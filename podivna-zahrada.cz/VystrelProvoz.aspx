﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VystrelProvoz.aspx.cs" Inherits="VystrelProvoz" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Výstřel. (A shot.)</title>
<link rel="icon" href="~/vystrel.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/vystrel.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/vystrel.jpg"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="refresh" content="1;url=VystrelProvoz.aspx"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Tato hra spočívá v uposlechnutí rozkazu PAL. Po stisknutí tlačítka vám budou nabízeny trojice písmen a vy musíte zmáčknout tlačítko až na příkaz PAL. Hodně zábavy..."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9">
    <form id="form1" runat="server" DefaultButton="ButtonStop">
        <div>
            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="souhlas_Load" OnClick="souhlas_Click"/>
       <br /><br />
        <h1>Výstřel. (A shot.)</h1>
	<b>PAL! = SHOOT!</b>
        <br /><br />
            <asp:Label ID="LabelPal" runat="server" Font-Size="60" OnPreRender="LabelPal_PreRender"></asp:Label><br />
                <asp:Button ID="ButtonStop" runat="server" Text="STOP" OnClick="ButtonStop_Click" Height="70" Width="550" Font-Size="30"></asp:Button>
                </asp:Panel>
        </div>
    </form>
</body>
</html>

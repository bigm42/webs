﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class SazkaNaCislo : System.Web.UI.Page
{
    protected override void OnPreRender(EventArgs e)
    {
        if (Request["napoveda"] != null)
        {
            LiteralNapoveda.Visible = true;
            HyperLinkZavritNapovedu.Visible = true;
        }

        if (Session["cislo"] == null)
        {
            if (Session["count"] == null) ButtonZnovu_Click(new Object(), e);
            Random rnd = new Random(DateTime.Now.Millisecond);
            Session["cislo"] = rnd.Next(3) + 1;
        }
        base.OnPreRender(e);

    }

    protected int PocetBodu()
    {
        int d = 0;
        for (int i = 1; i <= 3; i++)
            if (Convert.ToInt32(Session[i.ToString()]) == Convert.ToInt32(Session["cislo"]))
                d = i;
        return (4 - d);
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Session["count"] = 0;
        Session["1"] = 0;
        Session["2"] = 0;
        Session["3"] = 0;
        Session["b1"] = 0;
        Session["b2"] = 0;
        Session["b3"] = 0;
        Session["cislo"] = null;
        b1.Visible = true;
        b2.Visible = true;
        b3.Visible = true;
        ButtonZnovu.Visible = false;
    }

    protected void Button_PreRender(object sender, EventArgs e)
    {
        (sender as Button).Visible = (Convert.ToInt32(Session[(sender as Button).ID]) == 0);
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        Session["count"] = Convert.ToInt32(Session["count"]) + 1;
        Session[Convert.ToString(Session["count"])] = (sender as Button).ID[1].ToString();
        Session[(sender as Button).ID] = 1;
        if (Convert.ToInt32(Session["count"]) == 3)
        {
            ButtonZnovu.Visible = true;
            HttpCookie cookCount, cookPoints;
            if (Request["count"] == null)
                cookCount = new HttpCookie("count", "1");
            else
                cookCount = new HttpCookie("count", (Convert.ToInt32(Request["count"]) + 1).ToString());
            cookCount.Expires = DateTime.Today.AddYears(1);
            Response.AppendCookie(cookCount);
            if (Request["points"] == null)
                cookPoints = new HttpCookie("points", PocetBodu().ToString());
            else
                cookPoints = new HttpCookie("points", (Convert.ToInt32(Request["points"]) + PocetBodu()).ToString());
            cookPoints.Expires = DateTime.Today.AddYears(1);
            Response.AppendCookie(cookPoints);
            if (PocetBodu() == 3) PanelZvuk.Visible = true;
            SqlConnection conn = new SqlConnection(Constants.ConnStr);
            SqlCommand comm = new SqlCommand("select max(id) from [podivna-zahrada_wins]", conn);
            try
            {
                conn.Open();
                object obj = comm.ExecuteScalar();
                int id = -1;
                if (obj != DBNull.Value) id = Convert.ToInt32(obj);
                comm = new SqlCommand("insert into [podivna-zahrada_wins] (id,time,[file],ip,name,p_score,add_str) values(@id,@time,@file,@ip,@name,@p_score,@add_str)", conn);
                comm.Parameters.AddWithValue("id", id + 1);
                comm.Parameters.AddWithValue("time", DateTime.Now);
                comm.Parameters.AddWithValue("file", "SazkaNaCislo.aspx");
                comm.Parameters.AddWithValue("ip", Request.UserHostAddress);
                comm.Parameters.AddWithValue("name", TextBoxName.Text);
                comm.Parameters.AddWithValue("p_score", PocetBodu());
                comm.Parameters.AddWithValue("add_str", "0"+Session["1"].ToString()+ ",0" + Session["2"].ToString() + ",0" + Session["3"].ToString());
                comm.ExecuteNonQuery();
            }
            finally { conn.Close(); }
        }
    }


    protected void LiteralCisla_PreRender(object sender, EventArgs e)
    {
        string str = "<b>Tipy:";
        for (int i = 1; i <= Convert.ToInt32(Session["count"]); i++)
            str += " " + Session[Convert.ToString(i)].ToString();
        str += "</b>";
        (sender as Literal).Text = str;
    }

    protected void LiteralScore_PreRender(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["count"]) == 3)
            (sender as Literal).Text = "Počet bodů: " + PocetBodu().ToString() +". (points.)";
        else
            if (Request["count"] != null)
                (sender as Literal).Text = "Celkový počet bodů je " + Request["points"].ToString() + "(points) na " + Request["count"].ToString() + "(count of tries) pokusů.";
    }

    protected void ImageCislo_PreRender(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["count"]) == 3)
            (sender as Image).ImageUrl = "~/text" + Session["cislo"].ToString() + ".png";
        else
            (sender as Image).ImageUrl = "";
    }

    protected void Souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) Souhlas.Visible = false;
    }

    protected void Souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        Souhlas.Visible = false;
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void LiteralNapoveda_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = false;
    }

    protected void HyperLinkZavritNapovedu_Load(object sender, EventArgs e)
    {
        (sender as HyperLink).Visible = false;
    }

    protected void TextBoxName_Init(object sender, EventArgs e)
    {
        if ((Request["login"] != null) && Request["login"].Contains("--")) (sender as TextBox).Text = Request["login"];
    }

    protected void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        if ((sender as TextBox).Text != "")
        {
            if (!(sender as TextBox).Text.Contains("--")) (sender as TextBox).Text = "-- " + (sender as TextBox).Text + " --";
            HttpCookie cook = new HttpCookie("login", (sender as TextBox).Text);
            cook.Expires = DateTime.Now.AddYears(1);
            Response.AppendCookie(cook);
        }
    }
}
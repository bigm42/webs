﻿using System;

public partial class Rozcestnik : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
	Session["pz"] = null;
    }

    protected void Zpravnicek_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zpravnicek.aspx");
    }

    protected void Upper_Click(object sender, EventArgs e)
    {
        Response.Redirect("http://game4upper.com/?pz=1");
    }

    protected void Ostatni_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx?dale=pravdive");
    }
}
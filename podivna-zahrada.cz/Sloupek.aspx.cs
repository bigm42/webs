﻿using System;
using System.Web.UI.WebControls;

public partial class Sloupek : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["sl"]!=null)
        {
            for (int i = 0; i < Basic.Rows[0].Cells[0].Controls.Count; i++)
                if (Basic.Rows[0].Cells[0].Controls[i] is Panel) (Basic.Rows[0].Cells[0].Controls[i] as Panel).Visible = false;
            (Basic.Rows[0].Cells[0].FindControl("Sloupek" + Request["sl"].ToString()) as Panel).Visible = true;
        }
    }
}
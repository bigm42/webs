﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SazkaNaCislo.aspx.cs" Inherits="SazkaNaCislo" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sázka na Číslo. (Punt On Number.)</title>
<link rel="icon" href="~/sazka.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/sazka.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/text2.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Tato hra spočívá v hádání již vylosovaného čísla od 1 do 3. Nejdřív hádáte, a pak se dozvíte vylosované číslo a počet obdržených bodů. Hodně zábavy..."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9
	 ">
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="PanelZvuk" runat="server" OnLoad="PanelZvuk_Load" Visible="false">
            <embed height="0px" width="0px" src="hejhola.wav" />
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="Souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="Souhlas_Load" OnClick="Souhlas_Click"/>
       <br />
                Jméno hráče (name of player): <asp:TextBox ID="TextBoxName" runat="server" Width="80" AutoPostBack="True" OnInit="TextBoxName_Init" OnTextChanged="TextBoxName_TextChanged"></asp:TextBox>
        <br />
        <h1>Sázka na Číslo. (Punt On Number.)</h1>
            <asp:HyperLink ID="HyperLinkNapoveda" runat="server" NavigateUrl="~/SazkaNaCislo.aspx?napoveda=1" Font-Size="20" ForeColor="Black">? Nápověda ?</asp:HyperLink>
            <br />
            <asp:Literal ID="LiteralNapoveda" runat="server" Visible="false">
                Tato hra spočívá v hádání již vylosovaného čísla od 1 do 3. Nejdřív hádáte, a pak se dozvíte vylosované číslo a počet obdržených bodů. Hodně zábavy...<br />
            </asp:Literal>
                <asp:HyperLink ID="HyperLinkZavritNapovedu" runat="server" NavigateUrl="~/SazkaNaCislo.aspx" Visible="false" OnLoad="HyperLinkZavritNapovedu_Load" 
                    ForeColor="Black">Zavřít nápovědu.</asp:HyperLink>
<br />
            <asp:Literal ID="LiteralScore" runat="server" OnPreRender="LiteralScore_PreRender"></asp:Literal>
        <br /><br />
<asp:Table runat="server" HorizontalAlign="Center">
    <asp:TableRow Height="400" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell Font-Size="42"><asp:Literal ID="LiteralCisla" runat="server" OnPreRender="LiteralCisla_PreRender"></asp:Literal></asp:TableCell>
        <asp:TableCell><asp:Image ID="ImageCislo" runat="server" OnPreRender="ImageCislo_PreRender"></asp:Image></asp:TableCell>
    </asp:TableRow>
</asp:Table>
        <br /><br />
            <asp:Button ID="b1" runat="server" Text="1" OnPreRender="Button_PreRender" OnClick="Button_Click" Height="100" Width="100" Font-Size="50" ToolTip="Losuj na vybrané nahodné číslo 1-3."></asp:Button>
            <asp:Button ID="b2" runat="server" Text="2" OnPreRender="Button_PreRender" OnClick="Button_Click" Height="100" Width="100" Font-Size="50" ToolTip="Losuj na vybrané nahodné číslo 1-3."></asp:Button>
            <asp:Button ID="b3" runat="server" Text="3" OnPreRender="Button_PreRender" OnClick="Button_Click" Height="100" Width="100" Font-Size="50" ToolTip="Losuj na vybrané nahodné číslo 1-3."></asp:Button>
            <asp:Button ID="ButtonZnovu" runat="server" Text="Znovu... (Again...)" OnClick="ButtonZnovu_Click" Visible="False" Height="100" Width="400" Font-Size="30" ToolTip="Nové nahodné číslo 1-3."></asp:Button>
        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz . (Back.)</asp:HyperLink><br />
</asp:Panel>    
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Pexeso.aspx.cs" Inherits="Pexeso" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pexeso.</title>
<link rel="icon" href="~/pexeso.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/pexeso.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/pexeso/1.jpg"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Tato hra spočívá v hraní hry Pexeso s počítačem. Začíná hráč a pak se pořadí střídá. Když někdo otočí v jednom kole dvojici má bod a dvojice zůstává otočená. Vyhrává ten kdo nasbírá nejvíc dvojic na svém kontě. Hodně zábavy..."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9">
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="Souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="Souhlas_Load" OnClick="Souhlas_Click"/>
       <br />
                Jméno hráče (name of player): <asp:TextBox ID="TextBoxName" runat="server" Width="80" AutoPostBack="True" OnInit="TextBoxName_Init" OnTextChanged="TextBoxName_TextChanged"></asp:TextBox>
        <br />
        <h1>Pexeso.</h1>
                    <asp:Button ID="ButtonNapoveda" runat="server" Font-Size="20" Text="? Nápověda ?" OnClick="ButtonNapoveda_Click"></asp:Button>
            <br />
            <asp:Literal ID="LiteralNapoveda" runat="server" Visible="false">
                Tato hra spočívá v hraní hry Pexeso s počítačem. Začíná hráč a pak se pořadí střídá. Když někdo otočí v jednom kole dvojici má bod a dvojice zůstává otočená.
                Vyhrává ten kdo nasbírá nejvíc dvojic na svém kontě. Hodně zábavy...<br />
            </asp:Literal>
                <asp:Button ID="ButtonZavritNapovedu" runat="server" OnClick="ButtonZavritNapovedu_Click" Visible="false" Text="Zavřít nápovědu."></asp:Button>
<br />
<asp:Literal ID="LiteralScore" runat="server" OnLoad="LiteralScore_Load"></asp:Literal>
        <br /><br />
<asp:Table ID="TableDvorek" runat="server" OnInit="TableDvorek_Init" HorizontalAlign="Center"></asp:Table>
        <br /><br />
            <asp:Button ID="ButtonPokracovat" runat="server" Text="Pokračovat. (Continue.)" OnLoad="ButtonPokracovat_Load" Height="70" Width="350" Font-Size="15" Enabled="False"></asp:Button>
            <asp:Button ID="ButtonZnovu" runat="server" Text="Rozdej. (Deal.)" OnClick="ButtonZnovu_Click" Height="70" Width="450" Font-Size="30"></asp:Button>
        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz . (Back.)</asp:HyperLink><br />
</asp:Panel>
        </div>
    </form>
</body>
</html>

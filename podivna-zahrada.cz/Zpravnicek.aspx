﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Zpravnicek.aspx.cs" Inherits="Zpravnicek" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Zprávníček.</title>
<link rel="icon" href="~/kan-li_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/kan-li_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/kan-li_logo.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Krev"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Zprávníček, sociální normy."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>

<body style="background-color:#e9e9e9
	">
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Přijměte níže uvedené losy jako dar. Děkuji za pochopení." OnLoad="souhlas_Load" OnClick="souhlas_Click"/>
         <br /><br /><br /><br />
         <asp:Table runat="server" HorizontalAlign="Center"><asp:TableRow><asp:TableCell Width="700" VerticalAlign="Top" HorizontalAlign="Center">
         
	<h2>Zprávníček vás vítá!</h2>
	<asp:ImageButton ID="ImageMainButton"  ImageUrl="~/kan-li_logo.png" runat="server" OnClick="ISWB_OC" ToolTip="Bonusové číslo."></asp:ImageButton>
        <br />
        <br />
        <br />
        (c) 2014 Krev <br />
	Pro e-mail zde si (a další nápisy) napište sem<b>: mitti@white-magic.net .</b>
	<br /><br />
             <asp:Label ID="LabelLogin" runat="server" ></asp:Label>
	        <br /><br />
        <asp:HyperLink ID="HyperLinkRoot" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" Font-Size="20"><b>Dále na základní menu...</b><br /></asp:HyperLink>     
        <asp:HyperLink ID="HyperLinkDesktop" runat="server" ForeColor="Black" NavigateUrl="~/kan-li_desktop.png" >Zprávníčkův obrázek na plochu.<br /></asp:HyperLink>   
          

	<br />
        <br />
        <br />
	Zadejte zde své jméno pro kompletní cookie ochranu: <asp:TextBox ID="jmeno" BackColor="#e9e9e9" runat="server" OnTextChanged="jmeno_TextCh" OnInit="jmeno_Init" AutoPostBack="True"></asp:TextBox>
        <br />
             <asp:Label ID="LabelCislo" runat="server" OnPreRender="LabelCislo_PreRender"></asp:Label>
	        <br />
        <br />
	Kontrola, který nápis patří k losu určitého čísla: podivna-zahrada.cz/Zpravnicek.aspx?cislo=určité_číslo .<br />
         
   </asp:TableCell>
   <asp:TableCell Width="400" ></asp:TableCell>
   <asp:TableCell HorizontalAlign="Center">

       <asp:Label ID="LabelZprava" runat="server" Text="" Font-Size="Large" OnPreRender="LabelZprava_PreRender"></asp:Label><br /><br />
    <asp:ImageButton ID="ImageButtonDownload" runat="server" ImageUrl="~/download.png" OnClick="ImageButtonDownload_Click"/>

<asp:Panel ID="pan" runat="server" Visible="False" OnLoad="OL" Width="300">
                   <asp:HyperLink ID="HyperLinkBSP" runat="server" ForeColor="Black" NavigateUrl="~/download/bsplayer262.1068.exe" >Video přehrávač (program).</asp:HyperLink> <br/>      
    <asp:CheckBox ID="CheckBox1" runat="server" Text="Řadit dle data." Checked="true" AutoPostBack="True" OnCheckedChanged="ChB_OChCh"></asp:CheckBox><br/>
	<asp:ImageButton ID="Znovu" runat="server" onClick="ChB_OChCh" ImageUrl="~/kan-li_znovu.png" ToolTip="Znovu načíst."/>
</asp:Panel>
   
<br />
   
	<asp:Panel runat="server" Width="300">
        <asp:Label ID="LabelSpolZakony" runat="server" >
            <h3>Společenské zákony</h3>
		
		--- <u>Zákon blázna:</u> jednotka musí být svobodná.<br /><br />
		--- <u>Zákon obrany:</u> skupiny jednotek jsou segregovány.<br /><br />
		--- <u>Zákon peněz:</u> nevynucené zlo se přeplácí.<br /><br />
		--- <u>Zákon kulky:</u> jednotka se musí bát odplaty.<br /><br />
		--- <u>Zákon sociality:</u> vždy je všechno jednotky (vyjma vlastnictví).<br /><br />
		--- <u>Zákon vlastnictví:</u> jednotka může vlastnit jen to co dokáže unést.<br /><br />
		--- <u>Zákon vyšších:</u> vyšší jedná prvý.<br /><br />
		
        </asp:Label>

        <asp:Label ID="LabelZpravnicek" runat="server" Font-Size="Large">
            <h1>Zprávníček</h1>
        
             --- Máslo, nůž a chleba - krve je vždy třeba. <br /><br />
            --- Kafíčko, kafíčko, kdopak ti dal prd - to byl asi krt.<br /><br />
            --- <strike>Naděje umřela - dobře jí tak.</strike><br /><br />
		--- <br /><br />
		--- "Žijte draze," Kan-Li stroze. <br /><br />
            --- Býti nejvyšším - vítej nejsladším.<br /><br />
		--- Víru dávej pomalu - nebo se ti vrátí.<br /><br />
		--- Hostíme se - vyberme si.<br /><br />
		--- Naše vyšlo ukáplo!<br /><br />
		--- Není, je a bude.<br /><br />
		--- Závist předchází pýchu.<br /><br />
		--- Důležitý je se umět překvapovat.<br /><br />
		--- Nejhorší je, že ty jako normální člověk, musíš žít s tím, že realita není o vědci. Voják, doktor, filmař.<br /><br />
		--- Špatný má jí rád!<br /><br />
		--- Prohra je smrt, smrt není prohra (porážka ještě není prohra). <br /><br />
		--- Já jsem zase chytrej!<br /><br />
		--- Musí ti to přijít (pochopitelně), jinak by svět nebyl!<br /><br />
		--- Chcete hodně? Proto jsem tu! Kan-Li fighter.<br /><br />
		--- Třeba je třeba!<br /><br />
		--- <strike>Hlídám, hlídám člověčinu. ČíCa</strike><br /><br />
		--- <br /><br />
		--- Ze všech svých robotů si vybral právě sebe.<br /><br />
		--- Melem pomalu!<br /><br />
		--- sTrach je nelepší košile! Černá košile!<br /><br />
		--- Nesnili jste někdy konečně o naprosto soběstačné sluneční soustavě, dokonce občas něco darující? Vítejte.<br /><br />
		--- Než zaútočíš, dobře se ujisti, že jenom nehraješ hokej. (Už jsi někdy zkoušel bojovat mozkem?)<br /><br />
		--- Ke všemu potřebuješ teorii a odvahu.<br /><br />
		--- <br /><br />
		--- Nebýt někdy v právu je lepší než ho nedodržovat!<br /><br />
		--- Motto doby 21.: Věda není sud, sud je věda.<br /><br />
		--- "Vítězství je naše," cizopasníci.<br /><br />
		--- Víte k čemu se modlí bůh? K naší duně. Kan-Li.<br /><br />
		--- Zrada je zrada. Nevíte o něčem jiném?<br /><br />
		--- <br /><br />
		--- <u>Jako</u> leccos jde položit, ale kvadrant? INUNAN<br /><br />
		--- <u>Jako</u> leccos jde umučit, ale člověk je debil. Kan-Li friend.<br /><br />
		--- Vítězství musíš milovat! Čech.<br /><br />
		--- Jsem, tedy jsi. Kan-Li.<br /><br />
		--- 19.2.2018 Víra přistoupila v nás.<br /><br />
		--- <strike>Kung-Fu je (zatím) to jediné, co je.</strike> Kan-Li.<br /><br />
		--- ... rezervu sem! (22.4.2018 18:53)<br /><br />
		--- Škola segregace je bojem.<br /><br />
		--- Nezáleží na tom jak je malý, záleží na tom z čeho je!<br /><br />
        </asp:Label>
</asp:Panel>


   </asp:TableCell></asp:TableRow></asp:Table>
   </asp:Panel>
    </div>
    </form>
</body>

</html>

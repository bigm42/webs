﻿using System;
using System.Web.UI.WebControls;
using System.Web;
using System.Data.SqlClient;

public partial class NamorniBitva : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Session["zasah"]!=null)
        {
            if (Session[Session["zasah"].ToString()].ToString()[0] == '0')
                (TableMore.FindControl("B" + Session["zasah"].ToString()) as ImageButton).ImageUrl = "~/kam_more.png";
            else
            {
                if (Session[Session["zasah"].ToString()].ToString()[0] == '1')
                    (TableMore.FindControl("B" + Session["zasah"].ToString()) as ImageButton).ImageUrl = "~/kam_computer.png";
                else
                    (TableMore.FindControl("B" + Session["zasah"].ToString()) as ImageButton).ImageUrl = "~/kam_player.png";
            }
            Session["zasah"] = null;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        Random rnd = new Random(DateTime.Now.Millisecond);

        if (Session["znovu"] == null)
        {
            for (int i = 1; i <= 9; i++)
                for (int j = 1; j <= 9; j++)
                {
                    (TableMore.FindControl("B" + i.ToString() + j.ToString()) as ImageButton).ImageUrl = "~/kam.png";
                    Session[i.ToString() + j.ToString()] = 0;
                }
            for (int j = 1; j <= 2; j++)
                for (int w = 3; w >= 1; w--)
                    for (int i = 1; i <= 4 - w; i++)
                    {
                        bool b = true;
                        int x = 0; int y = 0;int smer = 0;int log = 0;
                        while (b)
                        {
                            x = rnd.Next(9) + 1;
                            y = rnd.Next(9) + 1;
                            if (rnd.Next(2) == 0) smer = -1; else smer = 1;
                            log = rnd.Next(2);
                            if ((log == 0) && ((x + (w - 1)*smer > 9) || (x + (w - 1)*smer < 1)))
                                smer = smer * (-1);
                            if ((log == 1) && ((y + (w - 1) * smer > 9) || (y + (w - 1) * smer < 1)))
                                smer = smer * (-1);
                            bool b1 = false;
                            for (int w2 = 0; w2 < w; w2++)
                            {
                                if (log == 0)
                                { if ((Session[(x + smer * w2).ToString() + y.ToString()]!=null)&&(Convert.ToInt32(Session[(x + smer * w2).ToString() + y.ToString()]) > 0)) b1 = true; }
                                else
                                { if ((Session[x.ToString() + (y + smer * w2).ToString()]!=null)&&(Convert.ToInt32(Session[x.ToString() + (y + smer * w2).ToString()]) > 0)) b1 = true; }
                            }
                            b = b1;
                        }
                        for (int w1 = -1; w1 < w+1; w1++)
                        {
                            if (log == 0)
                            {
                                Session[(x + smer * w1).ToString() + (y-1).ToString()] = 3;
                                if ((w1<0)||(w1>=w))
                                    Session[(x + smer * w1).ToString() + y.ToString()] = 3;
                                else
                                    Session[(x + smer * w1).ToString() + y.ToString()] = j;
                                Session[(x + smer * w1).ToString() + (y+1).ToString()] = 3;
                            }
                            else
                            {
                                Session[(x-1).ToString() + (y + smer * w1).ToString()] = 3;
                                if ((w1 < 0) || (w1 >= w))
                                    Session[x.ToString() + (y + smer * w1).ToString()] = 3;
                                else
                                    Session[x.ToString() + (y + smer * w1).ToString()] = j;
                                Session[(x+1).ToString() + (y + smer * w1).ToString()] = 3;
                            }
                        }
                    }
            for (int ix = 1; ix <= 9; ix++)
                for (int iy=1; iy <= 9; iy++)
                    if (Session[ix.ToString() + iy.ToString()].ToString() == "3")
                        Session[ix.ToString() + iy.ToString()] = 0;
            Session["znovu"] = 1;Session["player"] = 0;Session["comp"] = 0;Session["konec"] = null;Session["krok"] = 1;Session["opakovani"] = null;
        }

        if ((Session["znovu"] != null)&&(Session["krok"]!=null))
        {
            bool b = true;
            int x = 0; int y = 0; int c = 0;
            while (b)
            {
                x = rnd.Next(9) + 1;
                y = rnd.Next(9) + 1;
                b = (Session[x.ToString() + y.ToString()].ToString().Length > 1);
                if (c > 42) goto loop;
                c++;
            }
        loop:
            if (Session[x.ToString() + y.ToString()].ToString()[0] == '0')
                (TableMore.FindControl("B" + x.ToString() + y.ToString()) as ImageButton).ImageUrl = "~/kam_zasah.png";
            else
            {
                if (Session[x.ToString() + y.ToString()].ToString()[0] == '1')
                {
                    (TableMore.FindControl("B" + x.ToString() + y.ToString()) as ImageButton).ImageUrl = "~/kam_computer_zasah.png";
                    if (Session["konec"] == null) Session["player"] = Convert.ToInt32(Session["player"].ToString()) + 1;
                }
                else
                {
                    (TableMore.FindControl("B" + x.ToString() + y.ToString()) as ImageButton).ImageUrl = "~/kam_player_zasah.png";
                    if (Session["konec"] == null) Session["comp"] = Convert.ToInt32(Session["comp"].ToString()) + 1;
                }
            }
            if ((Convert.ToInt32(Session["comp"].ToString()) >= 10) || (Convert.ToInt32(Session["player"].ToString()) >= 10))
                Session["konec"] = 1;
            Session[x.ToString() + y.ToString()] = Session[x.ToString() + y.ToString()].ToString() + "1";
            Session["zasah"] = x.ToString() + y.ToString();
            Session["krok"] = null;
        }

        base.OnPreRender(e);
    }

    protected void LiteralScore_PreRender(object sender, EventArgs e)
    {
        (sender as Literal).Text = "<b>Computer " + Session["comp"].ToString() + " : Player " + Session["player"].ToString() + "</b><br>";
        int win = 100;bool wr = false;
        if (Convert.ToInt32(Session["comp"].ToString()) >= 10)
        { (sender as Literal).Text += "<b>Počítač vyhrál. (Computer wins.)</b>"; wr = true; }
        if (Convert.ToInt32(Session["player"].ToString()) >= 10)
        {
            (sender as Literal).Text += "<b>Vyhrál jste. (You wins.)</b>";
            if (Session["opakovani"]==null)PanelZvuk.Visible = true;
            Session["opakovani"] = 1;
            win = 0;
            wr = true;
        }
        if (wr)
        {
            SqlConnection conn = new SqlConnection(Constants.ConnStr);
            SqlCommand comm = new SqlCommand("select max(id) from [podivna-zahrada_wins]", conn);
            try
            {
                conn.Open();
                object obj = comm.ExecuteScalar();
                int id = -1;
                if (obj != DBNull.Value) id = Convert.ToInt32(obj);
                comm = new SqlCommand("insert into [podivna-zahrada_wins] (id,time,[file],ip,name,win,c_score,p_score) values(@id,@time,@file,@ip,@name,@win,@c_score,@p_score)", conn);
                comm.Parameters.AddWithValue("id", id + 1);
                comm.Parameters.AddWithValue("time", DateTime.Now);
                comm.Parameters.AddWithValue("file", "NamorniBitva.aspx");
                comm.Parameters.AddWithValue("ip", Request.UserHostAddress);
                comm.Parameters.AddWithValue("name", TextBoxName.Text);
                comm.Parameters.AddWithValue("win", win);
                comm.Parameters.AddWithValue("c_score", Convert.ToInt32(Session["comp"]));
                comm.Parameters.AddWithValue("p_score", Convert.ToInt32(Session["player"]));
                comm.ExecuteNonQuery();
            }
            finally { conn.Close(); }
        }
    }

    protected void TableMore_Init(object sender, EventArgs e)
    {
        for (int i = 1; i <= 9; i++)
        {
            TableRow tr = new TableRow();
            for (int j = 1; j <= 9; j++)
            {
                TableCell tc = new TableCell();
                tc.HorizontalAlign = HorizontalAlign.Center;
                tc.VerticalAlign = VerticalAlign.Middle;
                ImageButton ib = new ImageButton();
                ib.Click += Button_Click;
                ib.Load += Button_Load;
                ib.Width = 50;
                ib.ImageUrl = "~/kam.png";
                ib.ID = "B" + i.ToString() + j.ToString();
                tc.Controls.Add(ib);
                tr.Controls.Add(tc);
            }
            (sender as Table).Controls.Add(tr);
        }
    }

    protected void Button_Load(object sender, EventArgs e)
    {
        if ((Session[(sender as ImageButton).ID.Substring(1, 2)]!=null) &&(Session[(sender as ImageButton).ID.Substring(1, 2)].ToString().Length > 1))
        {
            if (Session[(sender as ImageButton).ID.Substring(1, 2)].ToString()[0] == '0')
                (sender as ImageButton).ImageUrl = "~/kam_more.png";
            else
            {
                if (Session[(sender as ImageButton).ID.Substring(1, 2)].ToString()[0] == '1')
                    (sender as ImageButton).ImageUrl = "~/kam_computer.png";
                else
                    (sender as ImageButton).ImageUrl = "~/kam_player.png";
             }
        }
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        if (Session[(sender as ImageButton).ID.Substring(1, 2)].ToString()[0] == '0')
            (sender as ImageButton).ImageUrl = "~/kam_more.png";
        else
        {
            if (Session[(sender as ImageButton).ID.Substring(1, 2)].ToString()[0] == '1')
            {
                (sender as ImageButton).ImageUrl = "~/kam_computer.png";
                if (Session["konec"]==null) Session["player"] = Convert.ToInt32(Session["player"].ToString()) + 1;
            }
            else
            {
                (sender as ImageButton).ImageUrl = "~/kam_player.png";
                if (Session["konec"] == null) Session["comp"] = Convert.ToInt32(Session["comp"].ToString()) + 1;
            }
        }
        if ((Convert.ToInt32(Session["comp"].ToString()) >= 10) || (Convert.ToInt32(Session["player"].ToString()) >= 10))
            Session["konec"] = 1;
        Session[(sender as ImageButton).ID.Substring(1, 2)] = Session[(sender as ImageButton).ID.Substring(1, 2)].ToString() + "1";
        Session["krok"] = 1;
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Session["znovu"] = null;
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void LiteralNapoveda_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = (Request["napoveda"] != null);
    }

    protected void HyperLinkZavritNapovedu_Load(object sender, EventArgs e)
    {
        (sender as HyperLink).Visible = (Request["napoveda"] != null);
    }

    protected void Souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) Souhlas.Visible = false;
    }

    protected void Souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        Souhlas.Visible = false;
    }

    protected void TextBoxName_Init(object sender, EventArgs e)
    {
        if ((Request["login"] != null) && Request["login"].Contains("--")) (sender as TextBox).Text = Request["login"];
    }

    protected void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        if ((sender as TextBox).Text != "")
        {
            if (!(sender as TextBox).Text.Contains("--")) (sender as TextBox).Text = "-- " + (sender as TextBox).Text + " --";
            HttpCookie cook = new HttpCookie("login", (sender as TextBox).Text);
            cook.Expires = DateTime.Now.AddYears(1);
            Response.AppendCookie(cook);
        }
    }
}
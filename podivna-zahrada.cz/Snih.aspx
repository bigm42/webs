﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Snih.aspx.cs" Inherits="Snih" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Úklid sněhu. (Snow.)</title>
<link rel="icon" href="~/snich.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/snich.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/chodnik_snich.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Tato hra spočívá v zametání sněhu po řádkách či sloupcích. Tlačítko se šipkami uklidí řádek či sloupec. Sníh občas přibívá, jak často bude přibívat volíme před rozsypáním dole mezi dvorem a tlačítkem na škále 0 až 5. Kdy 0 je nepadá vůbec, 1 padá každé kolo a 5 padá každé páté kolo. Před rozsypáním můžeme volit ještě počet zasypaných políček. Hodně zábavy..."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9">
    <form id="form1" runat="server">
        <div>
        <asp:Panel ID="PanelZvuk" runat="server" OnLoad="PanelZvuk_Load" Visible="false">
            <embed height="0px" width="0px" src="hejhola.wav" />
        </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
       <br /><br />
        <h1>Úklid sněhu. (Snow.)</h1>
            <asp:HyperLink ID="HyperLinkNapoveda" runat="server" NavigateUrl="~/Snih.aspx?napoveda=1" Font-Size="20" ForeColor="Black">? Nápověda ?</asp:HyperLink>
            <br />
            <asp:Literal ID="LiteralNapoveda" runat="server" Visible="false" OnLoad="LiteralNapoveda_Load">
                Tato hra spočívá v zametání sněhu po řádkách či sloupcích. Tlačítko se šipkami uklidí řádek či sloupec. Sníh občas přibívá, jak často bude přibívat volíme před
                rozsypáním dole mezi dvorem a tlačítkem na škále 0 až 5. Kdy 0 je nepadá vůbec, 1 padá každé kolo a 5 padá každé páté kolo. Před rozsypáním můžeme volit ještě
                počet zasypaných políček. Hodně zábavy...<br />
            </asp:Literal>
                <asp:HyperLink ID="HyperLinkZavritNapovedu" runat="server" NavigateUrl="~/Snih.aspx" Visible="false" OnLoad="HyperLinkZavritNapovedu_Load" 
                    ForeColor="Black">Zavřít nápovědu.</asp:HyperLink>
            <br />
<asp:Literal ID="LiteralScore" runat="server" OnPreRender="LiteralScore_PreRender"></asp:Literal>
        <br />
                         <asp:Table ID="TableMore" runat="server" OnInit="TableMore_Init" HorizontalAlign="Center"></asp:Table>
                                Sněhu na začátku: <asp:DropDownList ID="DropDownListStart" runat="server" OnInit="DropDownListStart_Init"></asp:DropDownList><br />
                        za kolik kol přírůstek: <asp:DropDownList ID="DropDownListPridat" runat="server" OnInit="DropDownListPridat_Init"></asp:DropDownList>
<br />
            <asp:Button ID="ButtonZnovu" runat="server" Text="Rozsyp sníh. (Deal.)" OnClick="ButtonZnovu_Click" Height="70" Width="550" Font-Size="30"></asp:Button>
        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz . (Back.)</asp:HyperLink><br />
</asp:Panel>
        </div>
    </form>
</body>
</html>

﻿using System;
using System.IO;

public partial class Original : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        byte[] b=File.ReadAllBytes(Server.MapPath("~/"+Request["file"].ToString()));
        
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename="+Request["file"].ToString());
        Response.AddHeader("Content-Length", b.Length.ToString());
        Response.ContentType = Request["content"].ToString();
        Response.BinaryWrite(b);
        Response.End();
    }

}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_dEFAULT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Podivná zahrada.</title>
<link rel="icon" href="~/kan-li_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/kan-li_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/kan-li_logo.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Menu podivné-zahrady.cz ."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>

<body style="background-color:#e9e9e9
	">
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
        <br /><br /><br /><br />
	<asp:Image runat="server" ImageUrl="~/podivna-zahrada.cz_nadpis.png"/>
	<br /><br /><br />
        <asp:HyperLink ID="HyperLinkZpravnicek" runat="server" ForeColor="Black" NavigateUrl="~/Zpravnicek.aspx" >Zprávníček.</asp:HyperLink>
	<br /><br />
        <asp:HyperLink ID="HyperLinkSloupek" runat="server" ForeColor="Black" NavigateUrl="~/Sloupek.aspx" >Sloupek pro vaše počteníčko.</asp:HyperLink>
	<br /><br />
        <asp:HyperLink ID="HyperLinkTarum" runat="server" ForeColor="Black" NavigateUrl="http://1tarum2cards1.com/?pz=1" >Tarum pro vaše potěšení. (Tarum card mirror for your enjoy.)</asp:HyperLink>
        <asp:HyperLink ID="HyperLinkTarumNapoveda" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive&napoveda=2" >? Nápověda ?</asp:HyperLink>

            <asp:Panel ID="PanelNapoveda2" runat="server" Visible="false"><br />
                Toto je věštecké karetní zrcadlo. Co znamenají pozice karet zjistíte, když na nich na chvíli zastavíte myší. Klikáním otáčíte. Hodně zábavy...<br />
                <asp:HyperLink ID="HyperLinkZavritNapovedu2" runat="server" NavigateUrl="~/Default.aspx?dale=pravdive" 
                    ForeColor="Black">Zavřít nápovědu.</asp:HyperLink>
            </asp:Panel>
	<br /><br />
        <asp:HyperLink ID="HyperLinkTarumBitka" runat="server" ForeColor="Black" NavigateUrl="https://game4upper.com/?pz=1" >Tarum přebika pro vaše bojové potěšení. (Tarum game4upper for your fight enjoy.)</asp:HyperLink>
        <asp:HyperLink ID="HyperLinkTarumBitkaNapoveda" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive&napoveda=1" >? Nápověda ?</asp:HyperLink>

            <asp:Panel ID="PanelNapoveda1" runat="server" Visible="false"><br />
                Tato hra spočívá v přebíjení soupeřových karet svými vyššími. Taky se jí přezdívá Hra vyšších. Vyhrává ten kdo má více vítězných štychů. Hodně zábavy...<br />
                <asp:HyperLink ID="HyperLinkZavritNapovedu1" runat="server" NavigateUrl="~/Default.aspx?dale=pravdive" 
                    ForeColor="Black">Zavřít nápovědu.</asp:HyperLink>
            </asp:Panel>
	<br /><br />
        <asp:HyperLink ID="HyperLinkKino" runat="server" ForeColor="Black" NavigateUrl="http://kino.mickey-software.cz/?pz=1" >Textová hra KINO.</asp:HyperLink>
	<br /><br />
        <asp:HyperLink ID="HyperLinkTajemstvi" runat="server" ForeColor="Black" NavigateUrl="http://tajemstvi.mickey-software.cz/?pz=1" >Krátká Rychlokvašná Textovka Divnost Tajemství.</asp:HyperLink>
<br /><br />
	<asp:HyperLink ID="HyperLinkPoezie" runat="server" ForeColor="Black" NavigateUrl="~/Poezie.aspx">Mistrova Poezie.</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkLotto" runat="server" ForeColor="Black" NavigateUrl="~/Lotto.aspx">Lotto.</asp:HyperLink>       
<br /><br />
        <asp:HyperLink ID="HyperLinkSZpdf" runat="server" ForeColor="Black" NavigateUrl="~/společenské zákony.pdf" >Společenské zákony v PDF.</asp:HyperLink> <br />      
        <asp:HyperLink ID="HyperLinkSZpdfEng" runat="server" ForeColor="Black" NavigateUrl="~/Social Norms.pdf" >Social Norms in PDF.</asp:HyperLink> <br />      
	<br /><br />
	<asp:HyperLink ID="HyperLinkSazka" runat="server" ForeColor="Black" NavigateUrl="~/SazkaNaCislo.aspx">Sázka na Číslo. (Punt On Number.)</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkHejblatko" runat="server" ForeColor="Black" NavigateUrl="~/Hejblatko.aspx">Hejblátko na 9.</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkSlepice" runat="server" ForeColor="Black" NavigateUrl="~/NajdiSlepici.aspx">Najdi slepici. (Find the Cake.)</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkNamBitva" runat="server" ForeColor="Black" NavigateUrl="~/NamorniBitva.aspx">Námořní bitva. (Sea Fight.)</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkVystrel" runat="server" ForeColor="Black" NavigateUrl="~/Vystrel.aspx">Výstřel. (A shot.)</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkPexeso" runat="server" ForeColor="Black" NavigateUrl="~/Pexeso.aspx">Pexeso.</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkPolicko" runat="server" ForeColor="Black" NavigateUrl="~/Policko.aspx">Políčko.</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkSnih" runat="server" ForeColor="Black" NavigateUrl="~/Snih.aspx">Úklid sněhu. (Snow.)</asp:HyperLink>       
<br /><br />
	<asp:HyperLink ID="HyperLinkStatistics" runat="server" ForeColor="Black" NavigateUrl="~/Statistic.aspx">Statistika. (Statistics.)</asp:HyperLink>       
<br /><br />
<asp:Label ID="LabelC" runat="server" ForeColor="DarkGray">(c) 2014-2018 Mickey Software pro Krev.</asp:Label>
<br /><br />
<asp:Label ID="LabelWeby" runat="server" ForeColor="DarkGray"><b>Dceřiné weby:</b></asp:Label>
<asp:HyperLink ID="HyperLink1tarum2cards1" runat="server" ForeColor="DarkGray" NavigateUrl="http://1tarum2cards1.com">1tarum2cards1.com,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkbeltbox" runat="server" ForeColor="DarkGray" NavigateUrl="http://belt-box.net">belt-box.net,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkblueenvelope" runat="server" ForeColor="DarkGray" NavigateUrl="http://blue-envelope.net">blue-envelope.net,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkcobras" runat="server" ForeColor="DarkGray" NavigateUrl="http://c-o-b-r-a-s.org">c-o-b-r-a-s.org,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkcupofcoffee" runat="server" ForeColor="DarkGray" NavigateUrl="http://cup-of-coffee.org">cup-of-coffee.org,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkgame4upper" runat="server" ForeColor="DarkGray" NavigateUrl="http://game4upper.com">game4upper.com,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkhrnekkafe" runat="server" ForeColor="DarkGray" NavigateUrl="http://hrnek-kafe.cz">hrnek-kafe.cz,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkimperiumMOBI" runat="server" ForeColor="DarkGray" NavigateUrl="http://imperium.mobi">imperium.mobi,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkLiVEpAss" runat="server" ForeColor="DarkGray" NavigateUrl="https://live-pass.info">live-pass.info,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkmodraschranka" runat="server" ForeColor="DarkGray" NavigateUrl="http://modra-schranka.cz">modra-schranka.cz,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkrealprice" runat="server" ForeColor="DarkGray" NavigateUrl="http://real-price.info">real-price.info,</asp:HyperLink>
<asp:HyperLink ID="HyperLinktheor" runat="server" ForeColor="DarkGray" NavigateUrl="http://the-or.cz">the-or.cz,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkvalasINFO" runat="server" ForeColor="DarkGray" NavigateUrl="http://valas.info">valas.info,</asp:HyperLink>
<asp:HyperLink ID="HyperLinkwhitemagic" runat="server" ForeColor="DarkGray" NavigateUrl="http://white-magic.net">white-magic.net .</asp:HyperLink>
<br />
   </asp:Panel>
    </div>
    </form>
</body>

</html>

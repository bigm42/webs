﻿using System;
using System.Web;
using System.Web.UI.WebControls;

public partial class Vystrel : System.Web.UI.Page
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Request["napoveda"] != null)
        {
            LiteralNapoveda.Visible = true;
            HyperLinkZavritNapovedu.Visible = true;
        }

        if ((Session["hit"] != null)&&(Session["hit"].ToString()=="1"))
        {
            HttpCookie cook;
            if (Request["vystrel-hits"] != null) cook = new HttpCookie("vystrel-hits",(Convert.ToInt32(Request["vystrel-hits"].ToString())+1).ToString()); else cook = new HttpCookie("vystrel-hits", "1");
            cook.Expires = DateTime.Today.AddYears(1);
            Response.AppendCookie(cook);
            PanelZvuk.Visible = true;
        }
        if (Session["hit"] != null)
        {
            HttpCookie cook;
            if (Request["vystrel-count"] != null) cook = new HttpCookie("vystrel-count", (Convert.ToInt32(Request["vystrel-count"].ToString()) + 1).ToString()); else cook = new HttpCookie("vystrel-count", "1");
            cook.Expires = DateTime.Today.AddYears(1);
            Response.AppendCookie(cook);
            Session["hit"] = null;
        }
    }

    protected void LiteralScore_Load(object sender, EventArgs e)
    {
        int hits = 0;int shits = 0;int count = 0;int rcount=0;
        if (Request["vystrel-hits"] != null) hits = Convert.ToInt32(Request["vystrel-hits"].ToString());
        if (Session["hit"] != null) { shits = Convert.ToInt32(Session["hit"].ToString()); count = 1; }
        if (Request["vystrel-count"] != null) rcount=Convert.ToInt32(Request["vystrel-count"].ToString());
        (sender as Literal).Text = "<b>Z " +(rcount+count).ToString() + " (count) možných - " +(hits+shits).ToString() + " (hits) zásahů.</b>";
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/VystrelProvoz.aspx");
    }

    protected void souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) souhlas.Visible = false;
    }

    protected void souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        souhlas.Visible = false;
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void LiteralNapoveda_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = false;
    }

    protected void HyperLinkZavritNapovedu_Load(object sender, EventArgs e)
    {
        (sender as HyperLink).Visible = false;
    }
}
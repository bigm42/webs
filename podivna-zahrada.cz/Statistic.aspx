﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Statistic.aspx.cs" Inherits="Statistic" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Statistika podivné zahrady.</title>
<link rel="icon" href="~/kan-li_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/kan-li_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/kan-li_logo.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Menu podivné-zahrady.cz ."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9">

    <form id="form1" runat="server">
    <div>
        <br />
        <asp:Panel runat="server" HorizontalAlign="Center">
            <h1>Statistika podivné zahrady.</h1>
            <h3>sledováno od 18.10.2018 - watched from 2018/18/10</h3>
            <asp:Label ID="LabelCompWins" runat="server" OnPreRender="LabelCompWins_PreRender"></asp:Label>
            <asp:Table runat="server" HorizontalAlign="Center" GridLines="Both">
                <asp:TableRow><asp:TableCell BackColor="DarkGray">
                    <b>Filter:</b><br />
                    poč. vypis. řádků: <asp:TextBox ID="TextBoxFilterNum" runat="server" Width="50" Text="50"></asp:TextBox>
                </asp:TableCell><asp:TableCell>
                    Html stránka (hra):<br />
                    <asp:TextBox ID="TextBoxFilterFile" runat="server"></asp:TextBox>
                </asp:TableCell><asp:TableCell>
                    IP hráče: <asp:TextBox ID="TextBoxFilterIP" runat="server"></asp:TextBox><br />
                    jméno hráče: <asp:TextBox ID="TextBoxFilterName" runat="server"></asp:TextBox>
                </asp:TableCell><asp:TableCell>
                    scóre počítače mezi <asp:TextBox ID="TextBoxFilterCompMeze1" runat="server" Width="50"></asp:TextBox>
                    a  <asp:TextBox ID="TextBoxFilterCompMeze2" runat="server" Width="50"></asp:TextBox>
			        <asp:RadioButton ID="RadioButtonComp" runat="server" GroupName="radio" Text="X" BackColor="Red" />
                    <br />
                    hráčovo scóre mezi <asp:TextBox ID="TextBoxFilterPlayerMeze1" runat="server" Width="50"></asp:TextBox>
                    a  <asp:TextBox ID="TextBoxFilterPlayerMeze2" runat="server" Width="50"></asp:TextBox>
			        <asp:RadioButton ID="RadioButtonPlayer" runat="server" GroupName="radio" Text="X" BackColor="Green" />
                </asp:TableCell><asp:TableCell>
                    přídavný řetězec:<br />
                    <asp:TextBox ID="TextBoxFilterAddStr" runat="server" Width="100"></asp:TextBox>
                </asp:TableCell><asp:TableCell>
                    <asp:Button ID="ButtonVyhledat" runat="server" Text="Vyhledat" />
                    <br />
			        <asp:RadioButton ID="RadioButtonNull" runat="server" GroupName="radio" Text="X" BackColor="Yellow" />
			        <asp:RadioButton ID="RadioButton1" runat="server" GroupName="radio" Text="X"/>
                </asp:TableCell></asp:TableRow>
            </asp:Table>
            <asp:Label ID="LabelNalezeno" runat="server" Text=""></asp:Label>
            <br /><br />
		<asp:HyperLink ID="HyperLinkBack1" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky. (Back.)</asp:HyperLink>
		<br />
            <asp:LinkButton ID="LinkButtonDown1" runat="server" OnClick="LinkButtonDown_Click"><<<</asp:LinkButton>
            --------- 
            <asp:LinkButton ID="LinkButtonUp1" runat="server" OnClick="LinkButtonUp_Click">>>></asp:LinkButton>
            <br />
            <asp:Table ID="TableVypis" runat="server" HorizontalAlign="Center" GridLines="Both" OnPreRender="TableVypis_PreRender">
                <asp:TableHeaderRow BackColor="DarkGray" Font-Bold="True"><asp:TableCell>
                    Čas
                </asp:TableCell><asp:TableCell>
                    Hra (html stránka)
                </asp:TableCell><asp:TableCell>
                    IP hráče
                </asp:TableCell><asp:TableCell>
                    jméno hráče
                </asp:TableCell><asp:TableCell>
                    X
                </asp:TableCell><asp:TableCell>
                    scóre počítače
                </asp:TableCell><asp:TableCell>
                    scóre hráče
                </asp:TableCell><asp:TableCell>
                    přídavný řetězec
                </asp:TableCell></asp:TableHeaderRow>
            </asp:Table>
            <asp:LinkButton ID="LinkButtonDown2" runat="server" OnClick="LinkButtonDown_Click"><<<</asp:LinkButton>
            --------- 
            <asp:LinkButton ID="LinkButtonUp2" runat="server" OnClick="LinkButtonUp_Click">>>></asp:LinkButton>
		<br />
		<asp:HyperLink ID="HyperLinkBack2" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky. (Back.)</asp:HyperLink>
		<br />
        </asp:Panel>
       </div>
    </form>
</body>
</html>

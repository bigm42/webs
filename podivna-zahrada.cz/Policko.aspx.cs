﻿using System;
using System.Web.UI.WebControls;
using System.Web;
using System.Data.SqlClient;

public partial class Policko : System.Web.UI.Page
{
    protected string Pozice(int p)
    {
        if (p < 20) return ("10" + (p + 1).ToString("D2"));
        else
        {
            if (p < 29) return ((9 - (p - 20)).ToString("D2") + "20");
            else
            {
                if (p < 48) return ("01" + (19-(p -29)).ToString("D2"));
                else return ((p - 46).ToString("D2") + "01");
            }
        }
    }

    protected bool JeV(int kde)
    {
        for (int i = 0; i < Session["pole"].ToString().Length / 2; i++)
            if (Session["pole"].ToString().Substring(i * 2, 2) == kde.ToString("D2")) return true;
        return false;
    }

    protected bool JeV(string poz)
    {
        for (int i = 0; i < Session["pole"].ToString().Length / 2; i++)
            if (Pozice(Convert.ToInt32(Session["pole"].ToString().Substring(i * 2, 2))) == poz) return true;
        return false;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Request["napoveda"] != null)
        {
            LiteralNapoveda.Visible = true;
            HyperLinkZavritNapovedu.Visible = true;
        }

        if (Session["znovu"]==null)
        {
            Session["player"] = 0; Session["comp"] = 0;Session["znovu"] = 1;Session["ps"] = 0;Session["cs"] = 0;
            LabelComputer.Text = "";LabelPlayer.Text = "";
            string str = "";
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 1; i <= Convert.ToInt32(DropDownListVratnych.SelectedValue); i++)
                str += (rnd.Next(55) + 1).ToString("D2");
            Session["pole"] = str;
        }
    }

    protected void TableDvorek_Init(object sender, EventArgs e)
    {
        for (int i = 1; i <= 10; i++)
        {
            TableRow tr = new TableRow();
            for (int j = 1; j <= 20; j++)
            {
                TableCell tc = new TableCell();
                tc.HorizontalAlign = HorizontalAlign.Center;
                tc.VerticalAlign = VerticalAlign.Middle;
                if ((i == 1) || (i == 10) || (j == 1) || (j == 20))
                {
                    Image ib = new Image();
                    ib.Width = 50;
                    ib.ID = "I" + i.ToString("D2") + j.ToString("D2");
                    ib.PreRender += Button_PreRender;
                    tc.Controls.Add(ib);
                }
                if ((i == 5) && (j == 10))
                {
                    HyperLink hl = new HyperLink();
                    hl.ID = "HyperLinkNapoveda";
                    hl.NavigateUrl = "~/Policko.aspx?napoveda=1";
                    hl.Text = "? Nápo-<br>věda ?";
                    tc.Controls.Add(hl);
                }
                tr.Controls.Add(tc);
            }
            (sender as Table).Controls.Add(tr);
        }
    }

    protected void Button_PreRender(object sender, EventArgs e)
    {
        if (Pozice(Convert.ToInt32(Session["comp"].ToString())) == (sender as Image).ID.Substring(1, 4))
        {
            if (Session["player"].ToString() == Session["comp"].ToString())
                (sender as Image).ImageUrl = "~/policko_computer_player.png";
            else
                (sender as Image).ImageUrl = "~/policko_computer.png";
        }
        else
        {
            if (Pozice(Convert.ToInt32(Session["player"].ToString())) == (sender as Image).ID.Substring(1, 4))
                (sender as Image).ImageUrl = "~/policko_player.png";
            else
            {
                if (JeV((sender as Image).ID.Substring(1, 4)))
                    (sender as Image).ImageUrl = "~/policko_domu.png";
                else
                    (sender as Image).ImageUrl = "~/policko.png";
            }
        }
    }

    protected void LiteralScore_PreRender(object sender, EventArgs e)
    {
        (sender as Literal).Text = "<b>Na průchody je to Computer: " + Session["cs"].ToString() + ", hráč: " + Session["ps"].ToString() + ".</b>";
        if ((Session["first1"]==null)&&((Session["cs"].ToString()=="1")||(Session["ps"].ToString() == "1")))
        {
            SqlConnection conn = new SqlConnection(Constants.ConnStr);
            SqlCommand comm = new SqlCommand("select max(id) from [podivna-zahrada_wins]", conn);
            try
            {
                conn.Open();
                object obj = comm.ExecuteScalar();
                int id = -1;
                if (obj != DBNull.Value) id = Convert.ToInt32(obj);
                comm = new SqlCommand("insert into [podivna-zahrada_wins] (id,time,[file],ip,name,win,c_score,p_score,add_str) values(@id,@time,@file,@ip,@name,@win,@c_score,@p_score,@add_str)", conn);
                comm.Parameters.AddWithValue("id", id + 1);
                comm.Parameters.AddWithValue("time", DateTime.Now);
                comm.Parameters.AddWithValue("file", "Policko.aspx");
                comm.Parameters.AddWithValue("ip", Request.UserHostAddress);
                comm.Parameters.AddWithValue("name", TextBoxName.Text);
                if ((Session["cs"].ToString() == "1") && (Session["ps"].ToString() == "1"))
                    comm.Parameters.AddWithValue("win", DBNull.Value);
                else
                {
                    if (Session["cs"].ToString() == "1")
                        comm.Parameters.AddWithValue("win", 100);
                    else
                        comm.Parameters.AddWithValue("win", 0);

                }
                comm.Parameters.AddWithValue("c_score", Convert.ToInt32(Session["cs"]));
                comm.Parameters.AddWithValue("p_score", Convert.ToInt32(Session["ps"]));
                string str = DropDownListVratnych.SelectedValue;
                if (str.Length == 1) str = "0" + str;
                comm.Parameters.AddWithValue("add_str", str);
                comm.ExecuteNonQuery();
            }
            finally { conn.Close(); }
            Session["first1"] = 1;
        }
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Session["znovu"] = null; Session["first1"] = null;

    }

    protected void ButtonPokracovat_Click(object sender, EventArgs e)
    {
        Random rnd = new Random(DateTime.Now.Millisecond);
        int hodp = 0; int cp = 0;
    loop:
        int hodc = rnd.Next(6) + 1; int cc = 0;
        while (hodc == 6) { cc++; hodc = rnd.Next(6) + 1; }
        if (hodp == 0)
        {
            hodp = hodc;
            cp = cc;
            goto loop;
        }
        hodp += 6 * cp;
        hodc += 6 * cc;
        LabelComputer.Text = hodc.ToString();
        LabelPlayer.Text = hodp.ToString();
        hodc += Convert.ToInt32(Session["comp"].ToString());
        while (hodc > 55)
        {
            hodc = hodc - 56;
            Session["cs"] = Convert.ToInt32(Session["cs"].ToString()) + 1;
        }
        if (JeV(hodc))
            hodc = 0;
        Session["comp"] = hodc;
        if (Session["comp"].ToString() == Session["player"].ToString())
            Session["player"] = 0;
        hodp += Convert.ToInt32(Session["player"].ToString());
        while (hodp > 55)
        {
            hodp = hodp - 56;
            Session["ps"] = Convert.ToInt32(Session["ps"].ToString()) + 1;
            PanelZvuk.Visible = true;
        }
        if (JeV(hodp))
            hodp = 0;
        Session["player"] = hodp;
        if (Session["comp"].ToString() == Session["player"].ToString())
            Session["comp"] = 0;
    }

    protected void DropDownListVratnych_Init(object sender, EventArgs e)
    {
        for (int i=0;i<=20;i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            (sender as DropDownList).Items.Add(li);
        }
        (sender as DropDownList).Items[6].Selected = true;
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void LiteralNapoveda_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = false;
    }

    protected void HyperLinkZavritNapovedu_Load(object sender, EventArgs e)
    {
        (sender as HyperLink).Visible = false;
    }

    protected void Souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) { Souhlas.Visible = false; LabelBR.Visible = false; }
    }

    protected void Souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        Souhlas.Visible = false;
        LabelBR.Visible = false;
    }

    protected void TextBoxName_Init(object sender, EventArgs e)
    {
        if ((Request["login"] != null) && Request["login"].Contains("--")) (sender as TextBox).Text = Request["login"];
    }

    protected void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        if ((sender as TextBox).Text != "")
        {
            if (!(sender as TextBox).Text.Contains("--")) (sender as TextBox).Text = "-- " + (sender as TextBox).Text + " --";
            HttpCookie cook = new HttpCookie("login", (sender as TextBox).Text);
            cook.Expires = DateTime.Now.AddYears(1);
            Response.AppendCookie(cook);
        }
    }
}
﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class NajdiSlepici : System.Web.UI.Page
{
    protected void Souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) Souhlas.Visible = false;
    }

    protected void Souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        Souhlas.Visible = false;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Request["napoveda"] != null)
        {
            LiteralNapoveda.Visible = true;
            HyperLinkZavritNapovedu.Visible = true;
        }

        if (Session["tah"] == null)
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            Session["slepice"] = (rnd.Next(9) + 1).ToString() + (rnd.Next(9) + 1).ToString();
            Session["tahu"] = 0; Session["tahu_c"] = null;
        }
        else
        {
            if (Session["tah"].ToString() == Session["slepice"].ToString())
            {
                (TableDvorek.FindControl("B" + Session["tah"].ToString()) as ImageButton).ImageUrl = "~/slepice.jpg";
                if (Session["tahu_c"] == null)
                    Session["tahu"] = Convert.ToInt32(Session["tahu"]) + 1;
                Session["tahu_c"] = 1;
                LiteralScore.Text = "(You win!) Gratulace, zvládli jste najít slepici za " + Session["tahu"].ToString() + " tahů (turns).";
                PanelZvuk.Visible = true;

                HttpCookie cook;
                if (Request["slepice"] == null)
                    cook = new HttpCookie("slepice", Session["tahu"].ToString());
                else
                {
                    if (Convert.ToInt32(Request["slepice"]) <= Convert.ToInt32(Session["tahu"])) goto loop;
                    cook = new HttpCookie("slepice", Session["tahu"].ToString());
                }
                cook.Expires = DateTime.Now.AddYears(1);
                Response.AppendCookie(cook);
            loop:
                SqlConnection conn = new SqlConnection(Constants.ConnStr);
                SqlCommand comm = new SqlCommand("select max(id) from [podivna-zahrada_wins]", conn);
                try
                {
                    conn.Open();
                    object obj = comm.ExecuteScalar();
                    int id = -1;
                    if (obj != DBNull.Value) id = Convert.ToInt32(obj);
                    comm = new SqlCommand("insert into [podivna-zahrada_wins] (id,time,[file],ip,name,win,p_score,add_str) values(@id,@time,@file,@ip,@name,@win,@p_score,@add_str)", conn);
                    comm.Parameters.AddWithValue("id", id + 1);
                    comm.Parameters.AddWithValue("time", DateTime.Now);
                    comm.Parameters.AddWithValue("file", "NajdiSlepici.aspx");
                    comm.Parameters.AddWithValue("ip", Request.UserHostAddress);
                    comm.Parameters.AddWithValue("name", TextBoxName.Text);
                    comm.Parameters.AddWithValue("win", DBNull.Value);
                    comm.Parameters.AddWithValue("p_score",Convert.ToInt32(Session["tahu"]));
                    comm.Parameters.AddWithValue("add_str", "0"+Session["slepice"].ToString()[1]+",0"+ Session["slepice"].ToString()[0]);
                    comm.ExecuteNonQuery();
                }
                finally { conn.Close(); }
            }
            else
            {
                (TableDvorek.FindControl("B" + Session["tah"].ToString()) as ImageButton).ImageUrl = "~/pes.jpg";
                if (Convert.ToInt32(Session["tah"].ToString()[0].ToString()) > Convert.ToInt32(Session["slepice"].ToString()[0].ToString()))
                    (TableDvorek.FindControl("B" + (Convert.ToInt32(Session["tah"].ToString()[0].ToString()) - 1).ToString() + Session["tah"].ToString()[1].ToString()) as ImageButton).ImageUrl = "~/kam_nahoru.png";
                if (Convert.ToInt32(Session["tah"].ToString()[0].ToString()) < Convert.ToInt32(Session["slepice"].ToString()[0].ToString()))
                    (TableDvorek.FindControl("B" + (Convert.ToInt32(Session["tah"].ToString()[0].ToString()) + 1).ToString() + Session["tah"].ToString()[1].ToString()) as ImageButton).ImageUrl = "~/kam_dolu.png";
                if (Convert.ToInt32(Session["tah"].ToString()[1].ToString()) > Convert.ToInt32(Session["slepice"].ToString()[1].ToString()))
                    (TableDvorek.FindControl("B" + Session["tah"].ToString()[0].ToString() + (Convert.ToInt32(Session["tah"].ToString()[1].ToString()) - 1).ToString()) as ImageButton).ImageUrl = "~/kam_leva.png";
                if (Convert.ToInt32(Session["tah"].ToString()[1].ToString()) < Convert.ToInt32(Session["slepice"].ToString()[1].ToString()))
                    (TableDvorek.FindControl("B" + Session["tah"].ToString()[0].ToString() + (Convert.ToInt32(Session["tah"].ToString()[1].ToString()) + 1).ToString()) as ImageButton).ImageUrl = "~/kam_prava.png";
                if (Session["tahu_c"] == null)
                    Session["tahu"] = Convert.ToInt32(Session["tahu"]) + 1;
            }
        }
    }

    protected void LiteralScore_Load(object sender, EventArgs e)
    {
        if (Request["slepice"] != null)
            (sender as Literal).Text = "Váš rekord je (Your best is) " + Request["slepice"].ToString() + " tahů (turns).";
    }

    protected void TableDvorek_Init(object sender, EventArgs e)
    {
        for (int i = 1; i <= 9; i++)
        {
            TableRow tr = new TableRow();
            for (int j = 1; j <= 9; j++)
            {
                TableCell tc = new TableCell();
                tc.Height = 60;tc.Width = 60;
                tc.HorizontalAlign = HorizontalAlign.Center;
                tc.VerticalAlign = VerticalAlign.Middle;
                ImageButton ib = new ImageButton();
                ib.Click += Button_Click;
                ib.Load += Button_Load;
                ib.Width = 50;
                ib.ToolTip = "Lov slepici.";
                ib.ID = "B" + i.ToString() + j.ToString();
                tc.Controls.Add(ib);
                tr.Controls.Add(tc);
            }
            (sender as Table).Controls.Add(tr);
        }
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        Session["tah"] = (sender as ImageButton).ID.Substring(1, 2);
    }

    protected void Button_Load(object sender, EventArgs e)
    {
        (sender as ImageButton).ImageUrl = "~/kam.png";
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Session["tah"] = null;
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void LiteralNapoveda_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = false;
    }

    protected void HyperLinkZavritNapovedu_Load(object sender, EventArgs e)
    {
        (sender as HyperLink).Visible = false;
    }

    protected void TextBoxName_Init(object sender, EventArgs e)
    {
        if ((Request["login"] != null) && Request["login"].Contains("--")) (sender as TextBox).Text = Request["login"];
    }

    protected void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        if ((sender as TextBox).Text != "")
        {
            if (!(sender as TextBox).Text.Contains("--")) (sender as TextBox).Text = "-- " + (sender as TextBox).Text + " --";
            HttpCookie cook = new HttpCookie("login", (sender as TextBox).Text);
            cook.Expires = DateTime.Now.AddYears(1);
            Response.AppendCookie(cook);
        }
    }
}
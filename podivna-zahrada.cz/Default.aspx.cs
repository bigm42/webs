﻿using System;
using System.Web.UI.WebControls;
using System.Web;

public partial class _dEFAULT : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Session["login"] == null)
        {
            for (int i = 0; i < Request.Cookies.Count; i++)
            {
                if (Request.Cookies[i].Name.Equals("login") || Request.Cookies[i].Name.Equals("cookies"))
                {
                    HttpCookie cook = new HttpCookie(Request.Cookies[i].Name, Request.Cookies[i].Value);
                    cook.Expires = DateTime.Now.AddYears(1);
                    Response.Cookies.Set(cook);
                }
            }
            Session["login"] = 1;
        }

        if (Request["napoveda"] != null) (form1.FindControl("PanelNapoveda" + Request["napoveda"]) as Panel).Visible = true;

        if (Request["dale"] != null)
        {
            Session["cislo"] = null;
            Session["deal"] = null; Session["first"] = null;
            Session["tah"] = null;
            Session["znovu"] = null;
            Session["pakli"] = null;
            Session["i"] = null;
            Session["pz"] = null;
        }
        else Response.Redirect("~/Rozcestnik.aspx");
    }
}
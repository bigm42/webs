﻿using System;
using System.Web;
using System.Web.UI.WebControls;

public partial class Hejblatko : System.Web.UI.Page
{
    protected string VolnaVedle(int x,int y)
    {
        string str = "";
        if (x - 1 >= 1) str += (x - 1).ToString() + y.ToString();
        if (x + 1 <= 3) str += (x + 1).ToString() + y.ToString();
        if (y - 1 >= 1) str += x.ToString() + (y - 1).ToString();
        if (y + 1 <= 3) str += x.ToString() + (y + 1).ToString();
        return str;
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (Request["napoveda"] != null)
        {
            LiteralNapoveda.Visible = true;
            HyperLinkZavritNapovedu.Visible = true;
        }

        string str = "";
        if (Session["deal"] == null)
        {
            Session["deal"] = 1;
            int d = 1;
            for (int i = 1; i <= 3; i++)
                for (int j = 1; j <= 3; j++)
                {
                    Session[i.ToString() + j.ToString()] = d;
                    Session["D" + i.ToString() + j.ToString()] = d;
                    d++;
                }
            Session["33"] = " "; Session["D33"] = " ";

            if (Session["first"] == null)
            {
                Session["first"] = 1;
                str = VolnaVedle(3, 3);
                Session["x"] = 3; Session["y"] = 3;
                Session["file"] = "33";
                Session["turn"] = 1;
                goto loopend;
            }

            Random rnd = new Random(DateTime.Now.Millisecond);
            int num = 0;
            for (int i1 = 1; i1 < 20; i1++)
            {
                int x = 0; int y = 0;
                for (int i = 1; i <= 3; i++)
                    for (int j = 1; j <= 3; j++)
                        if (Session[i.ToString() + j.ToString()].ToString() == " ") { x = i; y = j; goto loop; }
                    loop:
                str = VolnaVedle(x, y);
                num = rnd.Next(Convert.ToInt32(str.Length / 2));
                Session["vymena"] = Session[str[num * 2].ToString() + str[num * 2 + 1].ToString()].ToString();
                Session[str[num * 2].ToString() + str[num * 2 + 1].ToString()] = Session[x.ToString() + y.ToString()].ToString();
                Session[x.ToString() + y.ToString()] = Session["vymena"].ToString();
            }
            Session["x"] = str[num * 2].ToString(); Session["y"] = str[num * 2 + 1].ToString();
            Session["file"] = Session["x"].ToString() + Session["y"].ToString();
            Session["turn"] = 1;
        }

        str = VolnaVedle(Convert.ToInt32(Session["x"]),Convert.ToInt32(Session["y"]));
    loopend:
        for (int i1 = 0; i1 < Convert.ToInt32(str.Length / 2); i1++)
        {
            (Table1.FindControl("B" + str.Substring(i1 * 2, 2)) as Button).Visible = true;
            (Table1.FindControl("L" + str.Substring(i1 * 2, 2)) as Literal).Visible = false;
        }

        base.OnPreRender(e);

        if (Session["dealed"] != null)
        {
            bool b = true;
            for (int i2 = 1; i2 <= 3; i2++)
                for (int j2 = 1; j2 <= 3; j2++)
                    if (Session["D" + i2.ToString() + j2.ToString()].ToString() != Session[i2.ToString() + j2.ToString()].ToString())
                    {
                        b = false; goto loopend1;
                    }
            if (b)
            {
                HttpCookie cook;
                if (Request["chejblatko"] != null) cook = new HttpCookie("chejblatko", (Convert.ToInt32(Request["chejblatko"]) + 1).ToString()); else cook = new HttpCookie("chejblatko", "1");
                cook.Expires = DateTime.Today.AddYears(1);
                Response.AppendCookie(cook);
                LiteralScore.Text = "Gratulujeme, máte sestaveno. (You win.)";
                Session["dealed"] = null;
                PanelZvuk.Visible = true;
            }
        }
    loopend1:;
    }

    protected void Zamena(int x,int y)
    {
        Session["vymena"] = Session[x.ToString() + y.ToString()].ToString();
        Session[x.ToString() + y.ToString()] = Session[Session["x"].ToString() + Session["y"].ToString()].ToString();
        Session[Session["x"].ToString() + Session["y"].ToString()] = Session["vymena"].ToString();
        Session["x"] = x;Session["y"] = y;
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["file"].ToString().Length / 2) != Convert.ToInt32(Session["turn"].ToString()))
            Session["file"] = Session["file"].ToString().Substring(0, Convert.ToInt32(Session["turn"].ToString()) * 2);

        Zamena(Convert.ToInt32((sender as Button).ID[1].ToString()),Convert.ToInt32((sender as Button).ID[2].ToString()));
        Session["file"] = Session["file"].ToString() + Session["x"].ToString() + Session["y"].ToString();
        Session["turn"] = Convert.ToInt32(Session["turn"].ToString()) + 1;
    }

    protected void ButtonBack_Click(object sender, EventArgs e)
    {
        int turn = Convert.ToInt32(Session["turn"].ToString());
        turn--;
        Zamena(Convert.ToInt32(Session["file"].ToString().Substring((turn - 1) * 2, 1)), Convert.ToInt32(Session["file"].ToString().Substring((turn - 1) * 2 + 1, 1)));
        Session["turn"] = turn;
    }

    protected void ButtonBack_PreRender(object sender, EventArgs e)
    {
        (sender as Button).Enabled = (Convert.ToInt32(Session["turn"].ToString()) > 1);
    }

    protected void ButtonFF_Click(object sender, EventArgs e)
    {
        int turn = Convert.ToInt32(Session["turn"].ToString());
        turn++;
        Zamena(Convert.ToInt32(Session["file"].ToString().Substring((turn - 1) * 2, 1)), Convert.ToInt32(Session["file"].ToString().Substring((turn - 1) * 2 + 1, 1)));
        Session["turn"] = turn;
    }

    protected void ButtonFF_PreRender(object sender, EventArgs e)
    {
        (sender as Button).Enabled = (Convert.ToInt32(Session["turn"].ToString()) < Convert.ToInt32(Session["file"].ToString().Length / 2));
    }

    protected void Literal_PreRender(object sender, EventArgs e)
    {
        (sender as Literal).Text = Session[(sender as Literal).ID.Substring(1, 2)].ToString();
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Session["deal"] = null;Session["dealed"] = 1;
    }

    protected void Button_PreRender(object sender, EventArgs e)
    {
        (sender as Button).Text = Session[(sender as Button).ID.Substring(1, 2)].ToString();
    }

    protected void Button_Load(object sender, EventArgs e)
    {
        (sender as Button).Visible = false;
    }

    protected void Literal_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = true;
    }

    protected void Souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) Souhlas.Visible = false;
    }

    protected void Souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        Souhlas.Visible = false;
    }

    protected void LiteralScore_Load(object sender, EventArgs e)
    {
        if (Request["chejblatko"] == null)
            (sender as Literal).Text = "";
        else
            (sender as Literal).Text = "Již jste " + Request["chejblatko"].ToString() + "x sestavili.";
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void LiteralNapoveda_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = false;
    }

    protected void HyperLinkZavritNapovedu_Load(object sender, EventArgs e)
    {
        (sender as HyperLink).Visible = false;
    }
}
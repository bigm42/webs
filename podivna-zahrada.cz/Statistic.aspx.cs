﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing;

public partial class Statistic : System.Web.UI.Page
{
    protected string Filter()
    {
        string str = "";
        if (TextBoxFilterFile.Text != "") str += " and [file] like '%" + TextBoxFilterFile.Text + "%'";
        if (TextBoxFilterIP.Text != "") str += " and ip like '%" + TextBoxFilterIP.Text + "%'";
        if (TextBoxFilterName.Text != "") str += " and name like '%" + TextBoxFilterName.Text + "%'";
        if (TextBoxFilterCompMeze1.Text != "")
        {
            if (TextBoxFilterCompMeze2.Text != "")
                str += " and c_score>=" + TextBoxFilterCompMeze1.Text + " and c_score<=" + TextBoxFilterCompMeze2.Text;
            else
            {
                if (TextBoxFilterCompMeze1.Text.ToUpper().Equals("NULL"))
                    str += " and c_score is null";
                else
                    str += " and c_score=" + TextBoxFilterCompMeze1.Text;
            }
        }
        if (TextBoxFilterPlayerMeze1.Text != "")
        {
            if (TextBoxFilterPlayerMeze2.Text != "")
                str += " and p_score>=" + TextBoxFilterPlayerMeze1.Text + " and p_score<=" + TextBoxFilterPlayerMeze2.Text;
            else
                str += " and p_score=" + TextBoxFilterPlayerMeze1.Text;
        }
        if (RadioButtonComp.Checked) str += " and win=100";
        if (RadioButtonPlayer.Checked) str += " and win=0";
        if (RadioButtonNull.Checked) str += " and win is null";
        if (TextBoxFilterAddStr.Text != "")
        {
            if (TextBoxFilterAddStr.Text.ToUpper().Equals("NULL"))
                str += " and add_str is null";
            else
                str += " and add_str like '%" + TextBoxFilterAddStr.Text + "%'";
        }
        if (str.Length > 0) str = "where" + str.Substring(4);
        return str;
    }

    protected void LabelCompWins_PreRender(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select avg(win) from [podivna-zahrada_wins] "+Filter(), conn);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            dr.Read();
            LabelCompWins.Text = "<h4>Computer má úspěšnost (fruitfulness is) " + dr[0].ToString() + "% z nalezených (from find).</h4>";
        }
        finally { conn.Close(); }
    }

    protected void TableVypis_PreRender(object sender, EventArgs e)
    {
        if (Session["starti"] == null) Session["starti"] = 0;
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("select count(id) from [podivna-zahrada_wins] " + Filter(), conn);
        try
        {
            conn.Open();
            int radku= Convert.ToInt32(comm.ExecuteScalar());
		Session["radku"] =radku;
		int max=Convert.ToInt32(Session["starti"]) + Convert.ToInt32(TextBoxFilterNum.Text);
		if (max>radku) max=radku;
            LabelNalezeno.Text = Session["radku"].ToString() + " záznamů nalezeno. (" +(Convert.ToInt32(Session["starti"])+1).ToString() + " - " + max.ToString() + " zobrazeno.)";
            comm = new SqlCommand("select * from [podivna-zahrada_wins] " + Filter()+" ORDER BY id DESC OFFSET "+Session["starti"].ToString()+" ROWS FETCH NEXT "+Convert.ToInt32(TextBoxFilterNum.Text).ToString()+" ROWS ONLY", conn);
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                    TableRow tr = new TableRow();
                    
		for(int i=1;i<=8;i++)
		{
		TableCell tc = new TableCell();
                    Label lb = new Label();
		if (i==5)
		{
	                if (dr[i] != DBNull.Value)
        	        {
                	    if (dr[i].ToString() == "0") { lb.Text = "P"; tc.BackColor = Color.Green; } else { lb.Text = "C"; tc.BackColor = Color.Red; }
                	}
                	else { lb.Text = "N"; tc.BackColor = Color.Yellow; }
		}else{
                	if (dr[i] != DBNull.Value)
                    	        lb.Text = dr[i].ToString();
                	else
                    	        lb.Text = "null";
		}
                tc.Controls.Add(lb);
                    tr.Cells.Add(tc);
		}


                TableVypis.Rows.Add(tr);
            }
        }
        finally { conn.Close(); }

    }

    protected void LinkButtonDown_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["starti"]) - Convert.ToInt32(TextBoxFilterNum.Text) >= 0)
            Session["starti"] = Convert.ToInt32(Session["starti"]) - Convert.ToInt32(TextBoxFilterNum.Text);
	else
	    Session["starti"] = 0;
    }

    protected void LinkButtonUp_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["starti"]) + Convert.ToInt32(TextBoxFilterNum.Text) < Convert.ToInt32(Session["radku"]))
            Session["starti"] = Convert.ToInt32(Session["starti"]) + Convert.ToInt32(TextBoxFilterNum.Text);
    }
}
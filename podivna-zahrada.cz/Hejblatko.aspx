﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Hejblatko.aspx.cs" Inherits="Hejblatko" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hejblátko na 9.</title>
<link rel="icon" href="~/hejblatko.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/hejblatko.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/hejblatko.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Tato hra spočívá v sestavování hlavolamu, kde je vždy pole na jeden kamen volné a tam nějaký jiný posunujeme. Cílem je sestavit kameny od 1 do 8 a místo devítky prázdné místo. Hodně zábavy..."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9">
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="PanelZvuk" runat="server" OnLoad="PanelZvuk_Load" Visible="false">
            <embed height="0px" width="0px" src="hejhola.wav" />
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="Souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="Souhlas_Load" OnClick="Souhlas_Click"/>
       <br />
        <br />
        <h1>Hejblátko na 9.</h1>
                    <asp:HyperLink ID="HyperLinkNapoveda" runat="server" NavigateUrl="~/Hejblatko.aspx?napoveda=1" Font-Size="20" ForeColor="Black">? Nápověda ?</asp:HyperLink>
            <br />
            <asp:Literal ID="LiteralNapoveda" runat="server" Visible="false" OnLoad="LiteralNapoveda_Load">
                Tato hra spočívá v sestavování hlavolamu, kde je vždy pole na jeden kamen volné a tam nějaký jiný posunujeme. Cílem je sestavit kameny od 1 do 8 a místo devítky mít
                prázdné místo. Hodně zábavy...<br />
            </asp:Literal>
                <asp:HyperLink ID="HyperLinkZavritNapovedu" runat="server" NavigateUrl="~/Hejblatko.aspx" Visible="false" OnLoad="HyperLinkZavritNapovedu_Load" 
                    ForeColor="Black">Zavřít nápovědu.</asp:HyperLink>
<br />
<asp:Literal ID="LiteralScore" runat="server" OnLoad="LiteralScore_Load"></asp:Literal>
        <br /><br />
            <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" >
                        <asp:Button ID="ButtonBack" runat="server" Text="<<" OnClick="ButtonBack_Click" Height="70" Width="70" Font-Size="30" OnPreRender="ButtonBack_PreRender" ToolTip="Zpět."></asp:Button>
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle">
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Button ID="ButtonFF" runat="server" Text=">>" OnClick="ButtonFF_Click" Height="70" Width="70" Font-Size="30" OnPreRender="ButtonFF_PreRender" ToolTip="Vpřed."></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L11" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B11" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L12" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B12" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L13" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B13" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L21" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B21" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L22" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B22" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L23" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B23" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L31" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B31" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L32" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B32" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                    <asp:TableCell Font-Size="50" Height="100" Width="100" HorizontalAlign="Center" VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="5">
                        <asp:Literal ID="L33" runat="server" OnPreRender="Literal_PreRender" OnLoad="Literal_Load"></asp:Literal>
                        <asp:Button ID="B33" runat="server" OnClick="Button_Click" OnLoad="Button_Load" OnPreRender="Button_PreRender" Height="70" Width="70" Font-Size="50" Visible="False" ToolTip="Vyměň za mezeru."></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        <br /><br />
            <asp:Button ID="ButtonZnovu" runat="server" Text="Rozmíchej kameny. (Deal.)" OnClick="ButtonZnovu_Click" Height="70" Width="550" Font-Size="30" ToolTip="Zamíchat kameny."></asp:Button>
        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz . (Back.)</asp:HyperLink><br />
</asp:Panel>    
    </div>
    </form>
</body>
</html>

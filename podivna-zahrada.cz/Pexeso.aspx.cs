﻿using System;
using System.Web.UI.WebControls;
using System.Web;
using System.Data.SqlClient;

public partial class Pexeso : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["comp"]==null)
        {
            Session["comp"] = 0;
            Session["player"] = 0;
        }

        if ((Session["tahu"] != null) && (Session["tahu"].ToString() == "2"))
        {
            if (Session[Session["hit1"].ToString()].ToString() != Session[Session["hit2"].ToString()].ToString())
            {
                (TableDvorek.FindControl("B" + Session["hit1"].ToString()) as ImageButton).ImageUrl = "~/kam.png";
                (TableDvorek.FindControl("B" + Session["hit2"].ToString()) as ImageButton).ImageUrl = "~/kam.png";
            }
            else
            {
                Random rnd = new Random(DateTime.Now.Millisecond);
                Session[Session["hit1"].ToString()] = rnd.Next(1000000000)+11; Session[Session["hit2"].ToString()] = rnd.Next(1000000000)+11;
                if (Session["tah"].ToString() == "comp")
                    Session["comp"] = Convert.ToInt32(Session["comp"].ToString()) + 1;
                else
                    Session["player"] = Convert.ToInt32(Session["player"].ToString()) + 1;
                if (Convert.ToInt32(Session["comp"].ToString())+ Convert.ToInt32(Session["player"].ToString())==10)
                {
                    SqlConnection conn = new SqlConnection(Constants.ConnStr);
                    SqlCommand comm = new SqlCommand("select max(id) from [podivna-zahrada_wins]", conn);
                    try
                    {
                        conn.Open();
                        object obj = comm.ExecuteScalar();
                        int id = -1;
                        if (obj != DBNull.Value) id = Convert.ToInt32(obj);
                        comm = new SqlCommand("insert into [podivna-zahrada_wins] (id,time,[file],ip,name,win,c_score,p_score) values(@id,@time,@file,@ip,@name,@win,@c_score,@p_score)", conn);
                        comm.Parameters.AddWithValue("id", id + 1);
                        comm.Parameters.AddWithValue("time", DateTime.Now);
                        comm.Parameters.AddWithValue("file", "Pexeso.aspx");
                        comm.Parameters.AddWithValue("ip", Request.UserHostAddress);
                        comm.Parameters.AddWithValue("name", TextBoxName.Text);
                        if (Convert.ToInt32(Session["player"].ToString()) >= Convert.ToInt32(Session["comp"].ToString()))
                        {
                            if (Convert.ToInt32(Session["player"].ToString()) == Convert.ToInt32(Session["comp"].ToString()))
                                comm.Parameters.AddWithValue("win", DBNull.Value);
                            else
                                comm.Parameters.AddWithValue("win", 0);
                        } else comm.Parameters.AddWithValue("win", 100);
                        comm.Parameters.AddWithValue("c_score", Convert.ToInt32(Session["comp"]));
                        comm.Parameters.AddWithValue("p_score", Convert.ToInt32(Session["player"]));
                        comm.ExecuteNonQuery();
                    }
                    finally { conn.Close(); }
                }
            }
            (TableDvorek.FindControl("C" + Session["hit1"].ToString()) as TableCell).BorderStyle = BorderStyle.None;
            (TableDvorek.FindControl("C" + Session["hit2"].ToString()) as TableCell).BorderStyle = BorderStyle.None;
            int i = 0;
            if (!Obsahuje(Session["record"].ToString(), Session["hit1"].ToString())) { Session["record"] = Session["record"].ToString() + Session["hit1"].ToString(); i++; }
            if (!Obsahuje(Session["record"].ToString(), Session["hit2"].ToString())){ Session["record"] = Session["record"].ToString() + Session["hit2"].ToString(); i++; }
            Session["i"] = Convert.ToInt32(Session["i"].ToString()) + i;
            Session["tahu"] = 0;
            if (Session["tah"].ToString() == "comp")
                Session["tah"] = "player";
            else
                Session["tah"] = "comp";
        }
    }

    protected void Zobraz(string str)
    {
        (TableDvorek.FindControl("B" + str) as ImageButton).ImageUrl = "~/pexeso/" + Session[str].ToString() + ".jpg";
        (TableDvorek.FindControl("C" + str) as TableCell).BorderStyle = BorderStyle.Solid;
    }

    protected bool Obsahuje(string record,string hit)
    {
        bool b = false;
        for (int i = 0; i < record.Length / 2; i++)
            if (record.Substring(i * 2, 2) == hit) b = true;
        return b;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Request["napoveda"] != null)
        {
            LiteralNapoveda.Visible = true;
            ButtonZavritNapovedu.Visible = true;
        }

        Random rnd = new Random(DateTime.Now.Millisecond);
        if (Session["znovu"] == null)
        {
            string[] str = new string[20];
            int c = -1;
            for (int i = 1; i <= 4; i++)
                for (int j = 1; j <= 5; j++)
                {
                    c++;
                    str[c] = i.ToString() + j.ToString();
                    (TableDvorek.FindControl("B" + i.ToString() + j.ToString()) as ImageButton).ImageUrl = "~/kam.png";
                    (TableDvorek.FindControl("C" + i.ToString() + j.ToString()) as TableCell).BorderStyle = BorderStyle.None;
                }
            for (int z = 0; z <= 9; z++)
                for (int z1 = 1; z1 <= 2; z1++)
                {
                    int por = rnd.Next(c + 1);
                    Session[str[por]] = z;
                    for (int z2 = por + 1; z2 <= c; z2++)
                        str[z2 - 1] = str[z2];
                    c--;
                }
            Session["i"] = 0; Session["record"] = ""; Session["tahu"] = 0; Session["comp"] = 0; Session["player"] = 0;Session["znovu"] = 1; Session["tah"] = "player";
            LiteralScore_Load(LiteralScore, e);
        }
        else
        {
            if (Session["tah"].ToString() == "comp")
            {
                bool b = false;
                for (int w = 1; w <= Convert.ToInt32(Session["i"].ToString()); w++)
                    for (int w1 = 1; w1 <= Convert.ToInt32(Session["i"].ToString()); w1++)
                        if ((w != w1) && (Session[Session["record"].ToString().Substring((w - 1) * 2, 2)].ToString() == Session[Session["record"].ToString().Substring((w1 - 1) * 2, 2)].ToString()))
                        {
                            Session["hit1"] = Session["record"].ToString().Substring((w - 1) * 2, 2);
                            Session["hit2"] = Session["record"].ToString().Substring((w1 - 1) * 2, 2);
                            b = true;
                        }
                if (!b)
                {
                    string[] str = new string[20];
                    int[] hit = new int[20];
                    int c = -1;int c1 = -1;
                    for (int q1 = 1; q1 <= 4; q1++)
                        for (int q2 = 1; q2 <= 5; q2++)
                        {
                            if (Convert.ToInt32(Session[q1.ToString() + q2.ToString()].ToString()) < 10)
                            {
                                c++;
                                str[c] = q1.ToString() + q2.ToString();
                                bool b3 = false;
                                for (int w1 = 1; w1 <= Convert.ToInt32(Session["i"].ToString()); w1++)
                                    if (Session["record"].ToString().Substring((w1 - 1) * 2, 2) == str[c])
                                        b3 = true;
                                if (!b3)
                                {
                                    c1++;
                                    hit[c1] = c;
                                }
                            }
                        }
                    if (c > 0)
                    {
                        bool b2 = false; int hit1 = 0; int hit2 = 0;
                        if (c1 >= 0)
                            hit1 = hit[rnd.Next(c1 + 1)];
                        else
                            hit1 = rnd.Next(c + 1);
                        for (int w1 = 1; w1 <= Convert.ToInt32(Session["i"].ToString()); w1++)
                            if ((Session["record"].ToString().Substring((w1 - 1) * 2, 2) != str[hit1]) && (Session[str[hit1]].ToString() == Session[Session["record"].ToString().Substring((w1 - 1) * 2, 2)].ToString()))
                            {
                                Session["hit1"] = str[hit1];
                                Session["hit2"] = Session["record"].ToString().Substring((w1 - 1) * 2, 2);
                                b2 = true;
                            }
                        if (!b2)
                        {
                            bool b1 = true;
                            while (b1)
                            {
                                if (c1 > 0)
                                    hit2 = hit[rnd.Next(c1 + 1)];
                                else
                                    hit2 = rnd.Next(c + 1);
                                b1 = (str[hit1] == str[hit2]);
                            }
                            Session["hit1"] = str[hit1];
                            Session["hit2"] = str[hit2];
                        }
                        Zobraz(Session["hit1"].ToString()); Zobraz(Session["hit2"].ToString());
                        Session["tahu"] = 2;
                        ButtonPokracovat.Enabled = true;
                    }
                }
                else
                {
                    Zobraz(Session["hit1"].ToString()); Zobraz(Session["hit2"].ToString());
                    Session["tahu"] = 2;
                    ButtonPokracovat.Enabled = true;
                }
            }
        }
    }

    protected void TableDvorek_Init(object sender, EventArgs e)
    {
        for (int i = 1; i <= 4; i++)
        {
            TableRow tr = new TableRow();
            for (int j = 1; j <= 5; j++)
            {
                TableCell tc = new TableCell();
                tc.HorizontalAlign = HorizontalAlign.Center;
                tc.VerticalAlign = VerticalAlign.Middle;
                tc.ID = "C" + i.ToString() + j.ToString();
                tc.BorderColor = System.Drawing.Color.Red;
                ImageButton ib = new ImageButton();
                ib.Click += Button_Click;
                ib.Width = 120;ib.Height=120;
                ib.ImageUrl = "~/kam.png";
                ib.ID = "B" + i.ToString() + j.ToString();
                tc.Controls.Add(ib);
                tr.Controls.Add(tc);
            }
            (sender as Table).Controls.Add(tr);
        }
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        if ((Convert.ToInt32(Session["tahu"].ToString()) < 2) && ((sender as ImageButton).ImageUrl == "~/kam.png") && (Session["tah"].ToString() == "player"))
        {
            Zobraz((sender as ImageButton).ID.Substring(1, 2));
            Session["tahu"] = Convert.ToInt32(Session["tahu"].ToString()) + 1;
            Session["hit" + Session["tahu"].ToString()] = (sender as ImageButton).ID.Substring(1, 2);
            if (Session["tahu"].ToString()=="2") ButtonPokracovat.Enabled = true;
        }
    }

    protected void LiteralScore_Load(object sender, EventArgs e)
    {
        (sender as Literal).Text = "<b>Computer: " + Session["comp"].ToString() + ", Player: " + Session["player"].ToString() + ".</b>";
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Session["znovu"] = null;
    }

    protected void ButtonPokracovat_Load(object sender, EventArgs e)
    {
        (sender as Button).Enabled = false;
    }

    protected void ButtonZavritNapovedu_Click(object sender, EventArgs e)
    {
        LiteralNapoveda.Visible = false;
        ButtonZavritNapovedu.Visible = false;
    }

    protected void ButtonNapoveda_Click(object sender, EventArgs e)
    {
        LiteralNapoveda.Visible = true;
        ButtonZavritNapovedu.Visible = true;
    }

    protected void Souhlas_Load(object sender, EventArgs e)
    {
        if ((Request["cookies"] != null) && Request["cookies"].ToString().Contains("ano")) Souhlas.Visible = false;
    }

    protected void Souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
        if (Request["cookies"] != null) cook = new HttpCookie("cookies", Request["cookies"].ToString() + "ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
        Souhlas.Visible = false;
    }

    protected void TextBoxName_Init(object sender, EventArgs e)
    {
        if ((Request["login"] != null) && Request["login"].Contains("--")) (sender as TextBox).Text = Request["login"];
    }

    protected void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        if ((sender as TextBox).Text != "")
        {
            if (!(sender as TextBox).Text.Contains("--")) (sender as TextBox).Text = "-- " + (sender as TextBox).Text + " --";
            HttpCookie cook = new HttpCookie("login", (sender as TextBox).Text);
            cook.Expires = DateTime.Now.AddYears(1);
            Response.AppendCookie(cook);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Vystrel.aspx.cs" Inherits="Vystrel" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Výstřel. (A shot.)</title>
<link rel="icon" href="~/vystrel.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/vystrel.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/vystrel.jpg"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Tato hra spočívá v uposlechnutí rozkazu PAL. Po stisknutí tlačítka vám budou nabízeny trojice písmen a vy musíte zmáčknout tlačítko až na příkaz PAL. Hodně zábavy..."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9">
    <form id="form1" runat="server">
        <div>
        <asp:Panel ID="PanelZvuk" runat="server" OnLoad="PanelZvuk_Load" Visible="false">
            <embed height="0px" width="0px" src="hejhola.wav" />
        </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="souhlas_Load" OnClick="souhlas_Click"/>
       <br /><br />
        <h1>Výstřel. (A shot.)</h1>
            <asp:HyperLink ID="HyperLinkNapoveda" runat="server" NavigateUrl="~/Vystrel.aspx?napoveda=1" Font-Size="20" ForeColor="Black">? Nápověda ?</asp:HyperLink>
            <br />
            <asp:Literal ID="LiteralNapoveda" runat="server" Visible="false" OnLoad="LiteralNapoveda_Load">
                Tato hra spočívá v uposlechnutí rozkazu PAL. Po stisknutí tlačítka vám budou nabízeny trojice písmen a vy musíte zmáčknout tlačítko až na příkaz PAL. Hodně zábavy...<br />
            </asp:Literal>
                <asp:HyperLink ID="HyperLinkZavritNapovedu" runat="server" NavigateUrl="~/Vystrel.aspx" Visible="false" OnLoad="HyperLinkZavritNapovedu_Load" 
                    ForeColor="Black">Zavřít nápovědu.</asp:HyperLink>
<br />
<asp:Literal ID="LiteralScore" runat="server" OnLoad="LiteralScore_Load"></asp:Literal>
        <br /><br />
            <asp:Button ID="ButtonZnovu" runat="server" Text="Střílet znovu. (Again.)" OnClick="ButtonZnovu_Click" Height="70" Width="550" Font-Size="30"></asp:Button>
        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz . (Back.)</asp:HyperLink><br />
</asp:Panel>
        </div>
    </form>
</body>
</html>

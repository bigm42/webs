﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Rozcestnik.aspx.cs" Inherits="Rozcestnik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Podivná zahrada.</title>
<link rel="icon" href="~/kan-li_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/kan-li_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/kan-li_logo.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Menu podivné-zahrady.cz ."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>

<body style="background-color:#e9e9e9
	">
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
        <br /><br />
	<asp:Image runat="server" ImageUrl="~/podivna-zahrada.cz_nadpis.png"/>
	<br /><br /><br />
	<asp:Table runat="server" HorizontalAlign="Center" CellSpacing="0" CellPadding="0"><asp:TableRow><asp:TableCell>
	<asp:ImageButton ID="Zpravnicek" runat="server" ImageUrl="~/rozc_chram.png" OnClick="Zpravnicek_Click"/>
</asp:TableCell></asp:TableRow><asp:TableRow><asp:TableCell>
	<asp:ImageButton ID="Upper" runat="server" ImageUrl="~/rozc_tarum.png" OnClick="Upper_Click"/>
</asp:TableCell></asp:TableRow><asp:TableRow><asp:TableCell>
	<asp:ImageButton ID="Ostatni" runat="server" ImageUrl="~/rozc_ostatni.png" OnClick="Ostatni_Click"/>
</asp:TableCell></asp:TableRow></asp:Table>
   </asp:Panel>
    </div>
    </form>
</body>

</html>

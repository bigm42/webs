﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Policko.aspx.cs" Inherits="Policko" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Políčko.</title>
<link rel="icon" href="~/policko.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/policko.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/policko.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
<meta name="author" content="Mickey Software">
<meta name="ROBOTS" content="index, follow">
<meta name="description" content="Tato hra je podobná hře Člověče, nezlob se. Taky se tady vyhazuje na začátek kola, když přistihneš soupeře na svém vydobytém poli. Ale jsou zde navíc červená pole, jejichž počet si jde volit v dropdownlistu před znovurozdáním, která taky vracejí na začátek kola. Když ti u počítače padne šestka, losuje se znovu. Počítají se dokončená kola. Hodně zábavy...">
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost">
</head>
<body style="background-color:#e9e9e9">
    <form id="form1" runat="server">
        <div>
        <asp:Panel ID="PanelZvuk" runat="server" OnLoad="PanelZvuk_Load" Visible="false">
            <embed height="0px" width="0px" src="hejhola.wav" />
        </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
       
	<asp:Button runat="server" ID="Souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="Souhlas_Load" OnClick="Souhlas_Click"/>
                <asp:Label ID="LabelBR" runat="server" Text="<br>"></asp:Label>
                Jméno hráče (name of player): <asp:TextBox ID="TextBoxName" runat="server" Width="80" AutoPostBack="True" OnInit="TextBoxName_Init" OnTextChanged="TextBoxName_TextChanged"></asp:TextBox>
        <h1>Políčko.</h1>
        
            <asp:Literal ID="LiteralNapoveda" runat="server" Visible="false" OnLoad="LiteralNapoveda_Load">
                Tato hra je podobná hře Člověče, nezlob se. Taky se tady vyhazuje na začátek kola, když přistihneš soupeře na svém vydobytém poli. Ale jsou zde navíc červená pole,
                jejichž počet si jde volit v dropdownlistu před znovurozdáním, která taky vracejí na začátek kola. Když ti u počítače padne šestka, losuje se znovu. Počítají se
                dokončená kola. Hodně zábavy...<br />
            </asp:Literal>
                <asp:HyperLink ID="HyperLinkZavritNapovedu" runat="server" NavigateUrl="~/Policko.aspx" Visible="false" OnLoad="HyperLinkZavritNapovedu_Load" 
                    ForeColor="Black">Zavřít nápovědu.<br /></asp:HyperLink>
<asp:Literal ID="LiteralScore" runat="server" OnPreRender="LiteralScore_PreRender"></asp:Literal>
        <br /><asp:Label ID="LabelComputer" runat="server" Text="" ForeColor="#CCCC00" Font-Size="90" Font-Bold="True"></asp:Label>
            <asp:Label ID="Label1" runat="server" Text=":" Font-Size="90" Font-Bold="True"></asp:Label>
            <asp:Label ID="LabelPlayer" runat="server" Text="" ForeColor="#006600" Font-Size="90" Font-Bold="True"></asp:Label>
<asp:Table ID="TableDvorek" runat="server" OnInit="TableDvorek_Init" HorizontalAlign="Center"></asp:Table>
            
        <br /><br />
            <asp:Button ID="ButtonPokracovat" runat="server" Text="Pokračovat. (Continue.)" OnClick="ButtonPokracovat_Click" Height="70" Width="350" Font-Size="15"></asp:Button>
            <asp:DropDownList ID="DropDownListVratnych" runat="server" OnInit="DropDownListVratnych_Init" Font-Size="15"></asp:DropDownList>
            <asp:Button ID="ButtonZnovu" runat="server" Text="Rozdej. (Deal.)" OnClick="ButtonZnovu_Click" Height="70" Width="450" Font-Size="30"></asp:Button>
        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz . (Back.)</asp:HyperLink><br />
</asp:Panel>
        </div>
    </form>
</body>
</html>

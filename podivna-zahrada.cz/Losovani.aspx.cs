﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Losovani : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        long l = DateTime.Now.Ticks;
        while (l > 2147483647) l = l - 2147483647;
        Random rnd = new Random(Convert.ToInt32(l));
            int[] c = new int[6];
            for (int i=0;i<c.Length;i++)
            {
                c[i] = rnd.Next(64) + 1;
                for (int j = 0; j < i; j++)
                { if (c[j] == c[i]) i--; }
            }
            for (int i = 0; i < c.Length; i++)
            {
                int min = i;
                for (int j = i + 1; j < c.Length; j++)
                { if (c[j] < c[min]) min = j; }
                int temp = c[i];
                c[i] = c[min];
                c[min] = temp;
            }

            SqlConnection conn = new SqlConnection("data source=mssql01;database=db_textovka_net;user=usr_textovka_net;password=ybgnhgnhtggffghfzMhgS2d");
            try
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("insert into lotto values(@t,@c1,@c2,@c3,@c4,@c5,@c6)", conn);
                comm.Parameters.AddWithValue("@t", DateTime.Now);
                comm.Parameters.AddWithValue("@c1", c[0]);
                comm.Parameters.AddWithValue("@c2", c[1]);
                comm.Parameters.AddWithValue("@c3", c[2]);
                comm.Parameters.AddWithValue("@c4", c[3]);
                comm.Parameters.AddWithValue("@c5", c[4]);
                comm.Parameters.AddWithValue("@c6", c[5]);
                comm.ExecuteNonQuery();
            }
            finally { conn.Close(); }
		Napis.Text="Losováno.";
    }
}
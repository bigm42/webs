﻿using System;
using System.Web.UI.WebControls;

public partial class Snih : System.Web.UI.Page
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Request["napoveda"] != null)
        {
            LiteralNapoveda.Visible = true;
            HyperLinkZavritNapovedu.Visible = true;
        }

        Random rnd = new Random(DateTime.Now.Millisecond);
        if (Session["znovu"]==null)
        {
            for (int i = 1; i <= 9; i++)
                for (int j = 1; j <= 9; j++)
                    Session[i.ToString("d2") + j.ToString("d2")] = 0;
            for (int i = 1; i <= Convert.ToInt32(DropDownListStart.SelectedValue); i++)
                Session[(rnd.Next(9)+1).ToString("d2") + (rnd.Next(9) + 1).ToString("d2")] = 1;
            Session["tahu"] = 0;Session["znovu"] = 1;Session["pad"] = DropDownListPridat.SelectedValue;Session["kpadu"] = 1;Session["zvuk"] = null;
        }
        else
        {
            bool b = false;
            for (int i = 1; i <= 9; i++)
                for (int j = 1; j <= 9; j++)
                    if (Session[i.ToString("d2") + j.ToString("d2")].ToString() == "1") b = true;
            if (b)
            {
                if (Convert.ToInt32(Session["kpadu"].ToString())== Convert.ToInt32(Session["pad"].ToString()))
                {
                    Session[(rnd.Next(9) + 1).ToString("d2") + (rnd.Next(9) + 1).ToString("d2")] = 1;
                    Session["kpadu"] = 1;
                }
                else
                {
                    if (Convert.ToInt32(Session["kpadu"].ToString()) < Convert.ToInt32(Session["pad"].ToString()))
                        Session["kpadu"] = Convert.ToInt32(Session["kpadu"].ToString()) + 1;
                }
            }
            else
            {
                if (Session["zvuk"] == null)
                {
                    PanelZvuk.Visible = true;
                    Session["zvuk"] = 1;
                }
            }
        }
    }

    protected void TableMore_Init(object sender, EventArgs e)
    {
        for (int i = 1; i <= 10; i++)
        {
            TableRow tr = new TableRow();
            for (int j = 0; j <= 9; j++)
            {
                TableCell tc = new TableCell();
                tc.HorizontalAlign = HorizontalAlign.Center;
                tc.VerticalAlign = VerticalAlign.Middle;
                if ((i == 10) || (j == 0))
                {
                    ImageButton ib = new ImageButton();
                    ib.Click += Button_Click;
                    if (j==0) ib.ImageUrl = "~/kam_prava.png";
                    else ib.ImageUrl = "~/kam_nahoru.png";
                    ib.ID = "C" + i.ToString("d2") + j.ToString("d2");
                    if (!((i == 10) && (j == 0))) tc.Controls.Add(ib);
                }
                else
                {
                    Image im = new Image();
                    im.PreRender += Image_PreRender;
                    im.ID= "C" + i.ToString("d2") + j.ToString("d2");
                    tc.Controls.Add(im);
                }
                tr.Controls.Add(tc);
            }
            (sender as Table).Controls.Add(tr);
        }
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        if ((sender as ImageButton).ID.Substring(1,2)=="10")
        {
            for (int i = 1; i <= 9; i++)
                Session[i.ToString("d2") + (sender as ImageButton).ID.Substring(3, 2)] = 0;
        }
        else
        {
            for (int i = 1; i <= 9; i++)
                Session[(sender as ImageButton).ID.Substring(1, 2)+i.ToString("d2")] = 0;
        }
        Session["tahu"] = Convert.ToInt32(Session["tahu"].ToString()) + 1;
    }

    protected void Image_PreRender(object sender, EventArgs e)
    {
        if (Session[(sender as Image).ID.Substring(1, 4)].ToString() == "1")
            (sender as Image).ImageUrl = "~/chodnik_snich.png";
        else
            (sender as Image).ImageUrl = "~/chodnik.png";
    }

    protected void LiteralScore_PreRender(object sender, EventArgs e)
    {
        (sender as Literal).Text = "<b>Toto je " + Session["tahu"].ToString() + ". tah.</b>"; 
    }

    protected void ButtonZnovu_Click(object sender, EventArgs e)
    {
        Session["znovu"] = null;
    }

    protected void DropDownListStart_Init(object sender, EventArgs e)
    {
        for (int i=2;i<=60;i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            (sender as DropDownList).Items.Add(li);
        }
        (sender as DropDownList).Items[10].Selected = true;
    }

    protected void DropDownListPridat_Init(object sender, EventArgs e)
    {
        for (int i = 0; i <= 5; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            (sender as DropDownList).Items.Add(li);
        }
        (sender as DropDownList).Items[2].Selected = true;
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void LiteralNapoveda_Load(object sender, EventArgs e)
    {
        (sender as Literal).Visible = false;
    }

    protected void HyperLinkZavritNapovedu_Load(object sender, EventArgs e)
    {
        (sender as HyperLink).Visible = false;
    }
}
﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Collections.Generic;

public partial class Zpravnicek : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

	form1.DefaultButton="Znovu";

       if (Request.Url.OriginalString.ToUpper().Contains("WWW"))
	{
		int i=0;
		while (Request.Url.OriginalString.ToUpper()[i]!='W') 
			i++;
		Response.Redirect(Request.Url.OriginalString.Substring(0,i)+Request.Url.OriginalString.Substring(i+4));
	}
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        HttpCookie cook = new HttpCookie("ty", Request.UserHostAddress.ToString());
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
string str="";
if (Request["login"]!=null) str=Request["login"].ToString();
if (!str.Contains("--"))
{
        cook = new HttpCookie("login", DateTime.Now.ToString());
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
}
    }

    protected void jmeno_Init(object sender, EventArgs e)
    {
       if ((Request["login"]!=null)&&Request["login"].ToString().Contains("--")) jmeno.Text=Request["login"].ToString();
    }

    protected void jmeno_TextCh(object sender, EventArgs e)
    {
        if (!jmeno.Text.Contains("--")) jmeno.Text="-- "+jmeno.Text+" --";
	HttpCookie cook = new HttpCookie("login", jmeno.Text);
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
	ChB_OChCh(sender,e);
    }

    protected void souhlas_Load(object sender, EventArgs e)
    {
       if ((Request["cookies"]!=null)&&Request["cookies"].ToString().Contains("ano")) souhlas.Visible=false;
    }

    protected void souhlas_Click(object sender, EventArgs e)
    {
        HttpCookie cook;
	if (Request["cookies"]!=null) cook = new HttpCookie("cookies", Request["cookies"].ToString()+"ano"); else cook = new HttpCookie("cookies", "ano");
        cook.Expires = DateTime.Today.AddYears(1);
        Response.AppendCookie(cook);
	souhlas.Visible=false;
	ChB_OChCh(sender,e);
    }

    protected void ISWB_OC(object sender, EventArgs e)
    {
        if (Request["cislo"]!=null) Response.Redirect("~/Zpravnicek.aspx");
    }

    protected void OL(object sender, EventArgs e)
    {
        DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/download/"));
        FileInfo[] fi = di.GetFiles();
        Label lb = new Label();
        lb.Text = "==================================<br>";
        pan.Controls.Add(lb);
        if (CheckBox1.Checked)
        {
            string[] str = new string[fi.Length];
            if (fi.Length>0) str[0] = fi[0].LastWriteTime.ToString("yyyy-MM-dd-HH:mm") + " -- " + fi[0].Name + "<br>";
            for (int i = 1; i < fi.Length; i++)
            {
                int i1 = 0;
                while ((i1 < i) && (str[i1].CompareTo(fi[i].LastWriteTime.ToString("yyyy-MM-dd-HH:mm") + " -- " + fi[i].Name + "<br>") > 0))
                    i1++;
                for (int i2 = i; i2 > i1; i2--)
                    str[i2] = str[i2 - 1];
                str[i1] = fi[i].LastWriteTime.ToString("yyyy-MM-dd-HH:mm") + " -- " + fi[i].Name + "<br>";
            }
            foreach (string st in str)
            {
                HyperLink hl = new HyperLink();
                hl.Text = st;
			hl.NavigateUrl = "~/download/" + st.Substring(20, st.Length - 24);
                hl.ForeColor = Color.DarkGray;
                pan.Controls.Add(hl);
            }
        }
        else
            foreach (FileInfo fimem in fi)
            {
                HyperLink hl = new HyperLink();
                hl.Text = fimem.Name + " (" + fimem.LastWriteTime.ToString("d.M.yyyy-HH:mm") + ")<br>";
 			hl.NavigateUrl = "~/download/" + fimem.Name;
               hl.ForeColor = Color.DarkGray;
                pan.Controls.Add(hl);
            }
        lb = new Label();
        lb.Text = "==================================<br>";
        pan.Controls.Add(lb);
    }

    protected void LabelCislo_PreRender(object sender, EventArgs e)
    {
        if (Request.QueryString["cislo"] == null)
        {
            string str = "";
            for (int i = 0; i < Request.Cookies.Count; i++)
                if (Request.Cookies.Get(i).Name.Equals("ty2"))
                    str = Request.Cookies.Get(i).Value;
            if (CheckBox1.Text.Contains("X"))
            {
                HttpCookie cook1 = new HttpCookie("ty2", str);
                cook1.Expires = DateTime.Today.AddYears(100);
                Response.AppendCookie(cook1);
                LabelCislo.Text = "(login " + str.Substring(str.Length - 5) + ") <br><br> Vaše čísla jsou: " + Response.Cookies.Get("ty2").Value;
		CheckBox1.Text = CheckBox1.Text.Substring(3);
            }
            else
            {
                string random = new Random().Next(100000).ToString("d5");
                HttpCookie cook = new HttpCookie("ty2", str + " " + random);
                cook.Expires = DateTime.Today.AddYears(100);
                Response.AppendCookie(cook);
                LabelLogin.Text = "(login " + random + ")";
                LabelCislo.Text = "Vaše čísla jsou: " + Response.Cookies.Get("ty2").Value;
            }
        }
        else
        {
            if (CheckBox1.Text.Contains("X"))
		CheckBox1.Text = CheckBox1.Text.Substring(3);
            if (Request.QueryString["cislo"].Length > 5)
                LabelCislo.Text = "(falešný neplacený login 99999)";
            else
                LabelCislo.Text = "(falešný neplacený login " + Request.QueryString["cislo"] + ")";
        }
    }

    protected void LabelZprava_PreRender(object sender, EventArgs e)
    {
        int count = 0; bool b = true;
        List<int> ind = new List<int>();
        List<int> indbr = new List<int>();
        for (int i = 0; i < LabelZpravnicek.Text.Length - 2; i++)
        {
            if (LabelZpravnicek.Text.Substring(i, 3).Equals("---"))
            { count++; ind.Add(i); }
            if (LabelZpravnicek.Text.Substring(i, 3).Equals("<br"))
                if (b) { indbr.Add(i); b = false; } else b = true;
        }
        int rnd;
        if (Request.QueryString["cislo"] == null)
            rnd = Convert.ToInt32(Response.Cookies["ty2"].Value.Substring(Response.Cookies["ty2"].Value.Length - 5));
        else
            if (Request.QueryString["cislo"].Length > 5)
                rnd = 99999;
            else
                rnd = Convert.ToInt32(Request.QueryString["cislo"]);
        rnd = Convert.ToInt32(Math.Round(Convert.ToDouble((count - 1) * rnd / 99999m)));
        if ((indbr[rnd] - ind[rnd] - 3)>2) 
		LabelZprava.Text = "<u>Zpráva losu:</u> " + LabelZpravnicek.Text.Substring(ind[rnd] + 3, indbr[rnd] - ind[rnd] - 3);
	else
		LabelZprava.Text = "<u>Zpráva losu:</u> --VYMAZÁNO.--";
    }

    protected void ChB_OChCh(object sender, EventArgs e)
    {
        if (!CheckBox1.Text.Contains("(X)")) CheckBox1.Text = "(X)" + CheckBox1.Text;
    }

    protected void ImageButtonDownload_Click(object sender, ImageClickEventArgs e)
    {
        if (pan.Visible == true) pan.Visible = false; else pan.Visible = true;
        if (!CheckBox1.Text.Contains("(X)")) CheckBox1.Text = "(X)" + CheckBox1.Text;
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NamorniBitva.aspx.cs" Inherits="NamorniBitva" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Námořní bitva. (Sea Fight.)</title>
<link rel="icon" href="~/lod.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/lod.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/lod.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250"/>
<meta name="author" content="Mickey Software"/>
<meta name="ROBOTS" content="index, follow"/>
<meta name="description" content="Tato hra spočívá v sestřelování soupeřových lodí. Lodě jsou velikosti 1 - 3 políčka. Rozpočet je 3x1políčko, 2x2políčka a 1x3políčka pro každého hráče. Rozmístění na počátku losuje počítač, a ani jeden z hráčů neví dopředu rozmístění svých lodí, takže se často stává, že se trefí do své lodě. Lodě jsou vždy rozmístěny tak, že žádná se nedotýká ani rohy i kdyby soupeřovi. Na políčka vedle již sestřelených lodí nemusíte klikat. Počítač má žluté lodě, hráč má zelené. Vyhrává ten kdo první sestřelí soupeřových 10 políček (3x1+2x2+3x1). Hodně zábavy..."/>
<meta name="keywords" content="kan-li, její, uvolnění, klid, výsost"/>
</head>
<body style="background-color:#e9e9e9">
    <form id="form1" runat="server">
        <div>
        <asp:Panel ID="PanelZvuk" runat="server" OnLoad="PanelZvuk_Load" Visible="false">
            <embed height="0px" width="0px" src="hejhola.wav" />
        </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
	<asp:Button runat="server" ID="Souhlas" Text="Klepnutím na toto tlačítko souhlasíte s uložením do a použitím cookie záznamu, což jsou trvale uložené informace ve vašem počítači. Děkuji za pochopení." OnLoad="Souhlas_Load" OnClick="Souhlas_Click"/>
       <br />
                Jméno hráče (name of player): <asp:TextBox ID="TextBoxName" runat="server" Width="80" AutoPostBack="True" OnInit="TextBoxName_Init" OnTextChanged="TextBoxName_TextChanged"></asp:TextBox>
        <br />
        <h1>Námořní bitva. (Sea Fight.)</h1>
            <asp:HyperLink ID="HyperLinkNapoveda" runat="server" NavigateUrl="~/NamorniBitva.aspx?napoveda=1" Font-Size="20" ForeColor="Black">? Nápověda ?</asp:HyperLink>
            <br />
            <asp:Literal ID="LiteralNapoveda" runat="server" Visible="false" OnLoad="LiteralNapoveda_Load">
                Tato hra spočívá v sestřelování soupeřových lodí. Lodě jsou velikosti 1 - 3 políčka. Rozpočet je 3x1políčko, 2x2políčka a 1x3políčka pro každého hráče.
                Rozmístění na počátku losuje počítač, a ani jeden z hráčů neví dopředu rozmístění svých lodí, takže se často stává, že se trefí do své lodě. Lodě jsou vždy
                rozmístěny tak, že žádná se nedotýká ani rohy i kdyby soupeřovi. Na políčka vedle již sestřelených lodí nemusíte klikat. Počítač má žluté lodě, hráč má zelené. 
                Vyhrává ten kdo první sestřelí soupeřových
                10 políček (3x1+2x2+3x1). Hodně zábavy...<br />
            </asp:Literal>
                <asp:HyperLink ID="HyperLinkZavritNapovedu" runat="server" NavigateUrl="~/NamorniBitva.aspx" Visible="false" OnLoad="HyperLinkZavritNapovedu_Load" 
                    ForeColor="Black">Zavřít nápovědu.</asp:HyperLink>
            <br />
<asp:Literal ID="LiteralScore" runat="server" OnPreRender="LiteralScore_PreRender"></asp:Literal>
        <br />
<asp:Table ID="TableMore" runat="server" OnInit="TableMore_Init" HorizontalAlign="Center"></asp:Table>
        <br /><br />
            <asp:Button ID="ButtonZnovu" runat="server" Text="Rozmísti lodě. (Deal.)" OnClick="ButtonZnovu_Click" Height="70" Width="550" Font-Size="30"></asp:Button>
        <br /><br />
<asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx?dale=pravdive" >Zpátky na podivnou-zahradu.cz . (Back.)</asp:HyperLink><br />
</asp:Panel>
        </div>
    </form>
</body>
</html>

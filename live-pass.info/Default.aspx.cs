﻿using System;
using System.Web.UI.WebControls;
using System.IO;

public partial class live_Default : System.Web.UI.Page
{
    protected void Label0_PreRender(object sender, EventArgs e)
    {
	Random rnd=new Random(DateTime.Now.Millisecond);
	Label0.Text=TextBox0.Text;
    for (int i = 0; i < TextBox0.Text.Length*2; i++)
    {
        int box = rnd.Next(TextBox0.Text.Length); int box1 = rnd.Next(TextBox0.Text.Length);
        char ch = Label0.Text[box];
        Label0.Text = Label0.Text.Substring(0, box) + Label0.Text[box1].ToString() + Label0.Text.Substring(box + 1, TextBox0.Text.Length - box - 1);
        Label0.Text = Label0.Text.Substring(0, box1) + ch.ToString() + Label0.Text.Substring(box1 + 1, TextBox0.Text.Length - box1 - 1);
    }
	Session["a"]=Label0.Text;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if ((Request.UserLanguages[0].Contains("cs") || Request.UserLanguages[0].Contains("sk") || Request.UserLanguages[0].Contains("pl"))&&(!Request.Url.OriginalString.Contains("cz"))&&(Request["set"]==null))
            Response.Redirect("https://www.live-pass.info/Default_cz.aspx");
        else		
        if ((Request.UserLanguages[0].Contains("af") || Request.UserLanguages[0].Contains("fr") || Request.UserLanguages[0].Contains("tn") || Request.UserLanguages[0].Contains("zu"))&&(!Request.Url.OriginalString.Contains("_fr"))&&(Request["set"]==null))
            Response.Redirect("https://www.live-pass.info/Default_fr.aspx");
        else		
        if ((Request.UserLanguages[0].Contains("az") || Request.UserLanguages[0].Contains("be") || Request.UserLanguages[0].Contains("hy") || Request.UserLanguages[0].Contains("kk") || Request.UserLanguages[0].Contains("ru") || Request.UserLanguages[0].Contains("tt") || Request.UserLanguages[0].Contains("uk") || Request.UserLanguages[0].Contains("uz"))&&(!Request.Url.OriginalString.Contains("_ru"))&&(Request["set"]==null))
            Response.Redirect("https://www.live-pass.info/Default_ru.aspx");
        else		
        if ((Request.UserLanguages[0].Contains("ca") || Request.UserLanguages[0].Contains("es") || Request.UserLanguages[0].Contains("eu") || Request.UserLanguages[0].Contains("gl") || Request.UserLanguages[0].Contains("pt") || Request.UserLanguages[0].Contains("qu"))&&(!Request.Url.OriginalString.Contains("_sp"))&&(Request["set"]==null))
            Response.Redirect("https://www.live-pass.info/Default_sp.aspx");
        else		
        if ((Request.UserLanguages[0].Contains("da") || Request.UserLanguages[0].Contains("de") || Request.UserLanguages[0].Contains("nl"))&&(!Request.Url.OriginalString.Contains("_ger"))&&(Request["set"]==null))
            Response.Redirect("https://www.live-pass.info/Default_ger.aspx");
		else
            if ((!Request.Url.ToString().Contains("www")) || (!Request.Url.ToString().Contains("https")))
        {
            string sr = Request.Url.ToString();
            int i=0;
            while (!sr.Substring(i,15).Equals("live-pass.info/"))
                i++;
            Response.Redirect("https://www."+sr.Substring(i));
        }
    }

    protected void PTBLP(object sender, EventArgs e)
    {
        string str=Session["a"].ToString();
	Random rnd = new Random();
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename=" + TextBox0.Text +"_"+ rnd.Next(100000000).ToString("d8")+".txt");
        Response.AddHeader("Content-Length", (str.Length).ToString());
        Response.ContentType = "text/plain";
        Response.Write(str);
        Response.End();
    }

    protected void PTBLP1(object sender, EventArgs e)
    {
        byte[] b=File.ReadAllBytes(Server.MapPath("~/LiVE-pAss_"+(sender as Button).ID+".exe"));
        
        Random random = new Random(DateTime.Now.Millisecond);
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename=LiVE-pAss_"+(sender as Button).ID+".exe");
        Response.AddHeader("Content-Length", b.Length.ToString());
        Response.ContentType = "plain/text";
        Response.BinaryWrite(b);
        Response.End();
    }
}
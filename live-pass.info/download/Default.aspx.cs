﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class download_Default : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

            if ((!Request.Url.ToString().Contains("www")) || (!Request.Url.ToString().Contains("https")))
        {
            string sr = Request.Url.ToString();
            int i=0;
            while (!sr.Substring(i,24).Equals("live-pass.info/download/"))
                i++;
            Response.Redirect("https://www."+sr.Substring(i));
        }
    }

    protected void Panel1_Init(object sender, EventArgs e)
    {
        for (int i = 0; i < Constants_download.MaxButt; i++)
        {
            Button but = new Button();
            but.Click += But_Click;
            but.Visible = false;
            Panel1.Controls.Add(but);
        }
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        for (int i1 = 0; i1 < Panel1.Controls.Count; i1++)
            if (Panel1.Controls[i1] is Button)
                Panel1.Controls[i1].Visible = false;
        Panel1.Visible = false;

        string[] str = File.ReadAllLines(Constants_download.Loc + Constants_download.PassTXTname);
        foreach (string s in str)
        {
            int i = 0;
            while (s[i] != ':')
                i++;
            if (s.Substring(0, i) == TextBox1.Text)
            {
                int i1 = 0; int i2 = 0;
                Panel1.Visible = true;
            loop:
                i1 = i + 1;
                while ((s[i1] != ',') && (s[i1] != ';'))
                    i1++;
                while (!(Panel1.Controls[i2] is Button))
                    i2++;
                (Panel1.Controls[i2] as Button).Text = s.Substring(i+1,i1-i-1);
                (Panel1.Controls[i2] as Button).Visible = true;
                i2++;
                i = i1;
                if (s[i1] == ',') goto loop;
            }
        }
        }

    protected void But_Click(object sender, EventArgs e)
    {
        byte[] b=File.ReadAllBytes(Constants_download.Loc + (sender as Button).Text);
        
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename=" + (sender as Button).Text);
        Response.AddHeader("Content-Length", b.Length.ToString());
        Response.ContentType = "application/zip";
        Response.BinaryWrite(b);
        Response.End();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
	if (TextBox1.Text=="") {
	        for (int i1 = 0; i1 < Panel1.Controls.Count; i1++)
        	    if (Panel1.Controls[i1] is Button)
                	Panel1.Controls[i1].Visible = false;
	        Panel1.Visible = false;
	}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Original : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        string str1 = "image/jpeg";
        if (Request["content"] != null)
            str1 = Request["content"].ToString();

        string str = "";
        if (Request["dir"] != null)
            str = Request["dir"].ToString() + "/";

        byte[] b = File.ReadAllBytes(Server.MapPath("~/" + str + Request["file"].ToString()));
        
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename="+ Request["file"].ToString());
        Response.AddHeader("Content-Length", b.Length.ToString());
        Response.ContentType = str1;
        Response.BinaryWrite(b);
        Response.End();
    }

}
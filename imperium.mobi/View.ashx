﻿<%@ WebHandler Language="C#" Class="View" %>

using System;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

public class View : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        string str = "";
        if (context.Request["sl"] != null) str="Sloupek"+context.Request["sl"];
        if (context.Request["c"] != null) str = "Center"+context.Request["c"];
        Bitmap bm = new Bitmap(Constants.Loc+str+"_"+context.Request["file"]);
        MemoryStream st = new MemoryStream();
        bm.Save(st, ImageFormat.Jpeg);
        context.Response.Clear();
        context.Response.ContentType = "image/jpeg";
        //context.Response.Buffer = true;
        //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BinaryWrite(st.GetBuffer());
        context.Response.Flush();
        //context.Response.End();
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}
﻿using System;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

public partial class Default : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        int slc = 0; int sl = 0;
        for (int i = 0; i < Main.Controls.Count; i++)
            if ((Main.Controls[i] is Panel) && ((Main.Controls[i] as Panel).ID != "Links")&& !(Main.Controls[i] as Panel).ID.Contains("Center"))
            {
                slc++;
                if ((Request["sl"] != null) || (Request["date"] != null)) (Main.Controls[i] as Panel).Visible = false;
            }
        if (Request["sl"] != null)
        {
            (Main.FindControl("Sloupek" + Request["sl"].ToString()) as Panel).Visible = true;
            sl = Convert.ToInt32(Request["sl"]);
        }
        else sl = slc;

	DirectoryInfo di = new DirectoryInfo(Constants.Loc);
        FileInfo[] fi = di.GetFiles();
        int slv = 0;
        if (Request["date"] != null)
        {
            for (int i = 0; i < Links.Controls.Count; i++)
                if ((Links.Controls[i] is HyperLink) && ((Links.Controls[i] as HyperLink).Text.Contains(Request["date"])))
                {
                    sl = Convert.ToInt32((Links.Controls[i] as HyperLink).NavigateUrl.Substring(18));
                    (Main.FindControl("Sloupek" + sl.ToString()) as Panel).Visible = true;
                    Uloz(Main,"Sloupek", sl, fi); slv++;
                }
            if (slv > 1) { HLDolu.Visible = false; HLNahoru.Visible = false; }
        }
        else Uloz(Main, "Sloupek", sl, fi);

        HLDolu.NavigateUrl = "~/Default.aspx?sl=" + (sl - 1).ToString();
        HLNahoru.NavigateUrl = "~/Default.aspx?sl=" + (sl + 1).ToString();
        if (sl == slc) HLNahoru.NavigateUrl = "~/Default.aspx";
        if (sl == 1) HLDolu.NavigateUrl = "~/Default.aspx";
    }

    protected void Uloz(Panel p,string id, int sl, FileInfo[] fi)
    {
        for (int i = 0; i < (p.FindControl(id + sl.ToString()) as Panel).Controls.Count; i++)
        {   if ((p.FindControl(id + sl.ToString()) as Panel).Controls[i] is ImageButton)
            {
                bool b = false;
                for (int j = 0; j < fi.Length; j++)
                    if (fi[j].Name.Contains(((p.FindControl(id + sl.ToString()) as Panel).Controls[i] as ImageButton).ID)&&fi[j].Name.Contains(id+sl.ToString()))
                        b = true;
                if (!b)
                { 
                Bitmap bm = new Bitmap(Server.MapPath("~/fotos/" + ((p.FindControl(id + sl.ToString()) as Panel).Controls[i] as ImageButton).ID + ".JPG"));
		if (bm.Height>bm.Width)
		{
                    Bitmap bm1 = new Bitmap(Convert.ToInt32(400 * bm.Width / bm.Height),400);
                    Graphics gr = Graphics.FromImage(bm1);
                    gr.DrawImage(bm, new Rectangle(0, 0, Convert.ToInt32(400 * bm.Width / bm.Height),400));
                    bm1.Save(Constants.Loc + id + sl.ToString() + "_" + ((p.FindControl(id + sl.ToString()) as Panel).Controls[i] as ImageButton).ID + ".JPG", ImageFormat.Jpeg);
		}
		else
		{
                    Bitmap bm1 = new Bitmap(400, Convert.ToInt32(400 * bm.Height / bm.Width));
                    Graphics gr = Graphics.FromImage(bm1);
                    gr.DrawImage(bm, new Rectangle(0, 0, 400, Convert.ToInt32(400 * bm.Height / bm.Width)));
                    bm1.Save(Constants.Loc + id + sl.ToString() + "_" + ((p.FindControl(id + sl.ToString()) as Panel).Controls[i] as ImageButton).ID + ".JPG", ImageFormat.Jpeg);
		}
                }
            }
            if ((p.FindControl(id + sl.ToString()) as Panel).Controls[i] is Panel) Uloz((p.FindControl(id + sl.ToString()) as Panel).Controls[i] as Panel,"Center",sl,fi);
	}
    }

    protected void ImageBut_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Original.aspx?dir=fotos&file=" + (sender as ImageButton).ID + ".JPG");
    }

    protected void Logo_Click(object sender, EventArgs e)
    {
        PanelZvuk.Visible = true;
    }

    protected void PanelZvuk_Load(object sender, EventArgs e)
    {
        (sender as Panel).Visible = false;
    }

    protected void Listovani_PreRender(object sender, EventArgs e)
    {
        if ((sender as HyperLink).NavigateUrl.Equals("~/Default.aspx")) (sender as HyperLink).Enabled=false;
    }
}
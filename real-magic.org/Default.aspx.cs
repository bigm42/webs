﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;

public partial class _Default : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        for (int i = 0; i < 4; i++)
        {
            ImageButton ib = new ImageButton();
            ib.Click += IBclick;
            ib.ID = "IB" + (i*2).ToString();
            ib.ImageUrl = "~/CHu.ashx";
            PanelSnu.Controls.Add(ib);
            TextBox tb = new TextBox();
            tb.Width = 200; tb.Height = 100;
            tb.ID = "TB" + i.ToString();
            tb.Visible = false;
            tb.TextChanged += TBchange;
            tb.AutoPostBack = true;
            tb.TextMode = TextBoxMode.MultiLine;
            PanelSnu.Controls.Add(tb);

            ib = new ImageButton();
            ib.Click += IBclick;
            ib.ID = "IB" + (i*2 + 1).ToString();
            ib.ImageUrl = "~/CHu.ashx";
            PanelSnu.Controls.Add(ib);
            tb = new TextBox();
            tb.Width = 200; tb.Height = 100;
            tb.ID = "TB" + (i + 1).ToString();
            tb.Visible = false;
            tb.TextMode = TextBoxMode.MultiLine;
            PanelSnu.Controls.Add(tb);

            Label lb = new Label();
            lb.Text = "<br/>";
            PanelSnu.Controls.Add(lb);
        }
    }

    protected Control FajnControl(Control c, string str)
    {
        for (int j=0;j<(c as Panel).Controls.Count;j++)
            if ((c as Panel).Controls[j].ID.Contains(str))
                return (c as Panel).Controls[j];
        return null;
    }
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        Random rnd = new Random(DateTime.Now.Millisecond);
        if (Session["refresh"] != null)
        {
            for (int i = 0; i < 8; i++)
            {
                (FajnControl(PanelSnu, "IB" + i.ToString()) as ImageButton).Visible = true;
                (FajnControl(PanelSnu, "TB" + i.ToString()) as ImageButton).Visible = false;
            }

            if (Session["ID"] == null)
            {
                SqlConnection conn = new SqlConnection(Consta.ConnStr);
                SqlCommand comm = new SqlCommand("select magic_num from our", conn);
                List<Int64> l = new List<Int64>();
                try
                {
                    conn.Open();
                    SqlDataReader dr = comm.ExecuteReader();
                    while (dr.Read())
                        l.Add(Convert.ToInt64(dr[1]));
                    dr.Close();
                }
                finally { conn.Close(); }

                for (int i = 0; i < 8; i++)
                {
                    int lrnd = rnd.Next(l.Count);
                    ImageButton ib = (FajnControl(PanelSnu, "IB" + i.ToString()) as ImageButton);
                    ib.ImageUrl = "~/CHu.ashx?num=" + rnd.Next(1000000) + "&man=" + l[lrnd].ToString();
                    ib.ID = "IB" + i.ToString() + l[lrnd].ToString();
                }
            }
            else
            {
                SqlConnection conn = new SqlConnection(Consta.ConnStr);
                SqlCommand comm = new SqlCommand("select Id from texts where Idour=" + Session["ID"].ToString(), conn);
                List<string> l = new List<string>();
                try
                {
                    conn.Open();
                    SqlDataReader dr = comm.ExecuteReader();
                    while (dr.Read())
                        l.Add(dr[0].ToString());
                    dr.Close();
                }
                finally { conn.Close(); }

                for (int i = 0; i < 8; i++)
                {
                    int lrnd = rnd.Next(l.Count);
                    ImageButton ib = (FajnControl(PanelSnu, "IB" + i.ToString()) as ImageButton);
                    ib.ImageUrl = "~/CHu.ashx?num=" + l[lrnd];
                    ib.ID = "IB" + i.ToString() + l[lrnd];
                }
            }
            Session["refresh"] = null;
        }
    }

    protected void OTChNum(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Consta.ConnStr);
        SqlCommand comm = new SqlCommand("select Id,user from our where magic_num=" + (sender as TextBox).Text, conn);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            if (dr.Read())
            {
                Session["ID"] = Convert.ToInt64(dr[0]);
                Session["r-o"] = "1";
                USER.Visible = false; PASS.Visible = false; PASS_LABEL.Text = dr[1].ToString(); USER_BUTTON.Text = "Log Out.";
            }
            dr.Close();
        }
        finally { conn.Close(); }

    }

    protected void Unnamed_Click(object sender, EventArgs e)
    {
        if ((sender as Button).Text.Equals("Log Out."))
        {
            USER.Visible = true; PASS.Visible = true; PASS_LABEL.Text = "-Password:"; (sender as Button).Text = "Enter the magic!";
            Session["ID"] = null;
        }
        else
        {
            SqlConnection conn = new SqlConnection(Consta.ConnStr);
            SqlCommand comm = new SqlCommand("select Id from our where user=" + USER.Text + " and pass=" + PASS.Text, conn);
            try
            {
                conn.Open();
                SqlDataReader dr = comm.ExecuteReader();
                if (dr.Read())
                {
                    Session["ID"] = Convert.ToInt64(dr[0]);
                    Session["r-o"] = "0";
                    USER.Visible = false; PASS.Visible = false; PASS_LABEL.Text = USER.Text; (sender as Button).Text = "Log Out.";
                }
                dr.Close();
            }
            finally { conn.Close(); }
        }
    }

    protected void Refresh_Click(object sender, EventArgs e)
    {
        Session["refresh"] = 1;
    }

    protected void IBclick(object sender, EventArgs e)
    {
           if (Session["ID"] == null)
           {
               SqlConnection conn = new SqlConnection(Consta.ConnStr);
               SqlCommand comm = new SqlCommand("select text from our where user=" + USER.Text + " and pass=" + PASS.Text, conn);
               try
               {
                   conn.Open();
                   SqlDataReader dr = comm.ExecuteReader();
                   if (dr.Read())
                   {
                       Session["ID"] = Convert.ToInt64(dr[0]);
                       Session["r-o"] = "0";
                       USER.Visible = false; PASS.Visible = false; PASS_LABEL.Text = USER.Text; (sender as Button).Text = "Log Out.";
                   }
                   dr.Close();
               }
               finally { conn.Close(); }
           }
 
    }

    protected void TBchange(object sender, EventArgs e)
    {
    }
}
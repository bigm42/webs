﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>real-magic.org</title>
<link rel="icon" href="~/magic.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/magic.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/magic_org.png"/> 
<meta name="msapplication-TileColor" content="black"/>
</head>
<body style="background-color:#191919;color:white;font-family:Calibri">
    <form id="form1" runat="server">
    <div>
	<center>
        Motto: Write down something, you want, we will pulished it by a magic way.
		<br/><br/><br/>
		<br/>
		<asp:Table runat="server"><asp:TableRow><asp:TableCell>
		<asp:Label runat="server" ID="USER_LABEL">Username:</asp:Label>
        <asp:TextBox ID="USER" runat="server" AutoPostBack="True" MaxLength="50" />
        <br/>
		<asp:Label runat="server" ID="PASS_LABEL">-Password:</asp:Label>
        <asp:TextBox ID="PASS" runat="server" AutoPostBack="True" TextMode="Password" MaxLength="50" />
		</asp:TableCell><asp:TableCell>
        <asp:Button runat="server" ID="REF_BUTTON" OnClick="Refresh_Click" Width="123" Text="Refresh the magic"/><br/>
        <asp:Button runat="server" ID="USER_BUTTON" OnClick="Unnamed_Click" Width="123" Text="-Enter the magic!-"/>
		</asp:TableCell></asp:TableRow></asp:Table>
        
		<asp:HyperLink runat="server" NavigateUrl="~/Registration.aspx" ForeColor="White">Registration</asp:HyperLink><br/>
		<br/><br/>
		Watch magic # <asp:TextBox runat="server" AutoPostBack="True" MaxLength="50" OnTextChange="OTChNum" TextMode="Number" /><br/>
		<br/><br/>
		<asp:Image runat="server" ImageUrl="~/magic_org.png" Width="400"/><br/>
		<asp:Panel runat="server" ID="PanelSnu"></asp:Panel><br/>
        (c) AD2015 S.M.A.R.T.14 {web by mitti}<br/>
	</center>
    </div>
    </form>
</body>
</html>

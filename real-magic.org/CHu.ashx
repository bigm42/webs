﻿<%@ WebHandler Language="C#" Class="CHu" %>

using System;
using System.Web;
using System.Drawing;

public class CHu : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        Color color = new Color();
        color = ColorTranslator.FromHtml("#" + context.Request["num"].ToString());
        Bitmap bm = new Bitmap(200, 100);
        Graphics gr = Graphics.FromImage(bm);
        gr.FillRectangle(new SolidBrush(color), 10, 10, 190, 90);
        if (context.Request["man"] == null)
            gr.DrawString("#" + context.Request["num"].ToString(), new Font("Calibri", 20f), Brushes.White, Consta.NumberOnShowerX, Consta.NumberOnShowerY);
        else
            gr.DrawString("#" + context.Request["man"].ToString(), new Font("Calibri", 20f), Brushes.White, Consta.NumberOnShowerX, Consta.NumberOnShowerY);

        System.IO.MemoryStream st = new System.IO.MemoryStream();
        bm.Save(st, System.Drawing.Imaging.ImageFormat.Png);
        context.Response.Clear();
        context.Response.ContentType = "image/png";
        //context.Response.Buffer = true;
        //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BinaryWrite(st.GetBuffer());
        context.Response.Flush();
        //context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
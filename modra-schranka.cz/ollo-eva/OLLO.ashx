﻿<%@ WebHandler Language="C#" Class="OLLO" %>

using System;
using System.Web;
using System.Drawing;

public class OLLO : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        Random rnd = new Random(DateTime.Now.Millisecond);

        Bitmap bm = new Bitmap(200,200);
        Graphics gr = Graphics.FromImage(bm);
        gr.FillRectangle(new SolidBrush(Color.FromArgb(rnd.Next(256),rnd.Next(256),rnd.Next(256))), 0, 0, 200, 200);
        //gr.DrawRectangle(Pens.Black, 0, 0, 200, 60);

        gr.DrawString(Char.ConvertFromUtf32(rnd.Next(224)+33), new Font("Webdings", 120f), Brushes.LimeGreen, 10f, 20f);

        System.IO.MemoryStream st = new System.IO.MemoryStream();
        bm.Save(st,System.Drawing.Imaging.ImageFormat.Png);
        context.Response.Clear();
        context.Response.ContentType = "image/png";
        //context.Response.Buffer = true;
        //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BinaryWrite(st.GetBuffer());
        context.Response.Flush();
        //context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
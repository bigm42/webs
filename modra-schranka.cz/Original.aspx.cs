﻿using System;
using System.IO;

public partial class Original : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        byte[] b=File.ReadAllBytes(Server.MapPath("~/Báseň_o_nich.txt"));
        
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename=Báseň_o_nich.txt");
        Response.AddHeader("Content-Length", b.Length.ToString());
        Response.ContentType = "plain/text";
        Response.BinaryWrite(b);
        Response.End();
    }

}
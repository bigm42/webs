﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
	if (Request["nosound"]!=null) {PanelZvuk1.Visible=false;PanelZvuk2.Visible=true;Session["morch"]=1;}
    }

    protected void pan_Load(object sender, EventArgs e)
   {
        DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/"));
        FileInfo[] fi = di.GetFiles();

foreach (FileInfo fimem in fi) if (fimem.Name.Contains("plus"))
            {
                HyperLink hl = new HyperLink();
                hl.Text = fimem.Name + "<br>";
 			hl.NavigateUrl = fimem.Name;
                pan.Controls.Add(hl);
            }
}

    protected string BezMezer(string str)
    {
        int i=0;
        while (i < str.Length)
        {
            if (str[i] == ' ')
                str = str.Substring(0, i) + str.Substring(i + 1);
            else
                i++;
        }
        return str;
    }
    
    protected void ButtonPost_OnClick(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        SqlCommand comm = new SqlCommand("insert into real_price_forum(usr,text,ip) values(@usr,@text,@ip)", conn);
        if (BezMezer(TextBoxUser.Text) == "")
            TextBoxUser.Text = "*nobody*";
        comm.Parameters.AddWithValue("@usr",TextBoxUser.Text);
        comm.Parameters.AddWithValue("@text", TextBoxComm.Text);
        comm.Parameters.AddWithValue("@ip", Request.UserHostAddress);
        try
        {
            conn.Open();
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
        TextBoxComm.Text = "";TextBoxUser.Text = "";
    }

    protected void PanelComm_OnPreRender(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);

        Constants cns=new Constants();
	string[] ipss =  cns.IPban;  //ips.ToArray();
        string str = "";
        for (int i = 0; i < ipss.Length; i++)
            str += "ip not like '" + ipss[i] + "' and ";
        if (str != "")
            str = "where " + str.Substring(0, str.Length - 5);
        SqlCommand comm = new SqlCommand("select usr,text,ip from real_price_forum " + str, conn);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Label lb = new Label();
                lb.ForeColor=Color.FromArgb(int.Parse("FF6666", System.Globalization.NumberStyles.HexNumber));
		lb.Font.Bold=true;
                lb.Text = dr["usr"].ToString() + " (";
                PanelComm.Controls.Add(lb);
                lb = new Label();
                lb.ForeColor=Color.FromArgb(int.Parse("FF6666", System.Globalization.NumberStyles.HexNumber));
		lb.Font.Bold=true;
                lb.Text = dr["ip"].ToString() + "): ";
                PanelComm.Controls.Add(lb);
                lb = new Label();
                lb.Text = dr["text"].ToString() + "<br>";
                PanelComm.Controls.Add(lb);
            }
            dr.Close();
        }
        finally { conn.Close(); }
    }

	protected void RP_OnClick(object sender, EventArgs e)
	{
		Response.Redirect("ws_Mask_Solder_1280x1024.jpg");
	}

	protected void OnClickDreams(object sender, EventArgs e)
	{
		if ((sender as ImageButton).ImageUrl=="~/dreams_reality.png")
			{(sender as ImageButton).ImageUrl="~/text3763.png";RP.ImageUrl=RP.ToolTip;RP.ToolTip="Click for real help.";}
		else	
			{(sender as ImageButton).ImageUrl="~/dreams_reality.png";RP.ToolTip=RP.ImageUrl;RP.ImageUrl="~/dreams_reality.jpg";}
	}


	protected void PanelZvuk_PreRender(object sender, EventArgs e)
	{
		if (Session["morch"]!=null) {PanelZvuk1.Visible=false;PanelZvuk2.Visible=true;}
		Session["morch"]=1;
	}
}
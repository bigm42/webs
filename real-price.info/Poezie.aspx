﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Poezie.aspx.cs" Inherits="Poezie" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Real Price Poezie (in czech)</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
        <br/><br/>    
<h2>Věčná otázka...</h2>
Já roztočím tyč,<br/>
nad světem tím.<br/>
Já roztočím tyč,<br/>
nad pápěřím.<br/><br/>
Ta tyč není má,<br/>
je konečné!<br/>
Ta tyč není má -<br/>
- sny věčné!
<h2><asp:Label runat="server" ForeColor="White">... zní dokáže to? ;o)</asp:Label>... zní dokáže to? ;o)</h2>
===================================================
<h2>O komunismu... (i o tom vonodobém)</h2>
Spojíme své prodlužky,<br/>
ukážeme Bohu,<br/>
oplácáme podružky<br/>
a dáme to ROHu.<br/>
<i>díky všem dobrým lidem na obou stranách</i><br/>
<br/>
===================================================
<h2>Beze jména...</h2>
Winstne, Winstne, vytas nůž,<br/>
sere se nám mise,<br/>
„Pokusím se získat vše!‟ -<br/>
- a je z tebe sysel. ;o)<br/>
<br/>
===================================================
<h2>INUNAN je pako...</h2>
V očích pohled nevinný,<br/>
v ruce svírá nůž.<br/><br/>
<br/><br/>
KONEC.<br/><br/><br/>
<asp:Image runat="server" ImageUrl="~/pomnik.png"/>
<h2>DOVĚTEK:</h2>
Muž:<br/>
Rozbil jsem si tágo o mučícího.<br/>
<i>(Nechtěl prodat to co je u nás každýho.)</i>
<br/><br/>
Žena:<br/>
Náhradu zas žádej u popravčího.<br/><br/>
<br/><br/>
<h2>DOVĚTEK mazaný:</h2>
... a uprostřed katastrofy<br/>
baví mě vždy říkat strofy.<br/>
Ostatní mě za to nesnášejí,<br/>
dary mi za to snášejí.<br/>
... a mě to nevadí. ;o)
<br/><br/>
<h2>DOVĚTEKy vtipnÉ:</h2>
Říká policejní bifokál policajtovi:<br/>
„Nevíš proč mám otevřenou pusu?‟ <br/>
„Protože se ti líbí bůh, a už toho nech, já s tim mam sám problém, hmm.‟<br/> 
<br/><br/>
Sedí dva poláci a drží se za ruce. Jeden povídá druhému: „Kdo koho bude příště mrdat?‟ Druhý ovětil: „Já nevim, já jsem polák!‟ a rozbil prvnímu hubu.
<br/><br/>
Ta naše polka česká,<br/>
ta je tak hezká, tak hezká,<br/>
že s toho blejeme, všichni se sereme,<br/>
na barák cosi nám pleská!
<br/><br/>
<b>K o c h a m <br/>[2015-3-9 cca *h]<br/>všichni!</b>
<br/><br/>
<h2>DOVĚTEK volný:</h2>
Chlape - ať žije zvěř!! ;o)
<br/><br/>
<h2>DOVĚTEK pravdivím příběhem: V<asp:Label runat="server" ForeColor="White">y</asp:Label>jímce</h2>
Lezu, lezu studnou,<br/>
snad nikdy už nepřijde magor Zeman!<br/> ;oD
<br/><br/>
<h2>DOVĚTEK zde trapný:</h2>
Láska je prý z Calladanu,<br/>
ukaž, synu, ten průkaz,<br/>
proto tady asi hynu,<br/>
tak, ženo, ukaž mi ten úkaz!<br/> ;o)
<br/><br/>
<h2>DOVĚTEK malý:</h2>
Otazníčku, co je ti?<br/>
„Nechci s nima žít!‟ <br/>
„Žijou zas v tom přepjetí, <br/>
já chci někde hnít!‟ 
<br/><br/>
<h2>DOVĚTEK smutný:</h2>
Žít jak žít se má,<br/>
to byla má doména.<br/>
Sprasil jsem to jak oni,<br/>
teď žiju beze jména.<br/>
<i>... ale porazil jsem je!</i> ;o)
<br/>
<asp:Image runat="server" ImageUrl="~/Beze jména.png" Width="300"/>
<h2>DOVĚTEK totální</h2>
Já jsem  prášek na srdce,<br/>
udělal jsem dobře,<br/>
že jsem se stavil nad srnce,<br/>
udělal jsem tě bobře!
<br/><br/>
;o)
<h2>DOVĚTEK poslední</h2>
Věřit v sebe,
jak málo stačí,
jen věřit v sebe,
a vše mi stačí.
<br/><br/>
<b>DOVĚTEK věcný:</b> <b><u>Ž</u></b>enská <b><u>I</u></b>nstituce <b><u>J</u></b>e <b><u>chlape</u></b>. (o;
<br/><br/>
<a href="http://modra-schranka.cz/Sama_Doma.aspx">... a jedeme dál!</a>
<br/>
konec dobří - všechno dobří ;o)
<br/>
</asp:Panel></div>
    </form>
</body>
</html>

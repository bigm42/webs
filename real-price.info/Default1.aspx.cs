﻿using System;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data.SqlClient;

public partial class RAP : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
	
	if (Request["poll"]!=null) Response.Redirect("~/Poll.aspx?set=1&poll="+Request["poll"].ToString());
    }

    protected void Poll_PreRender(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        string str="";
	if ((sender as HyperLink).ID.Equals("YES")) str="y"; else str="n";
	SqlCommand comm = new SqlCommand("select "+str+" from real_price_get_on where day=0", conn);
        try
        {
            conn.Open();
            (sender as HyperLink).Text=(sender as HyperLink).ID+"="+Convert.ToInt32(comm.ExecuteScalar()).ToString();
        }
        finally { conn.Close(); }
    }

    protected string RandomString(int size)
    {
        StringBuilder builder = new StringBuilder();
        Random random = new Random(DateTime.Now.Millisecond);
        char ch;
        for (int i = 0; i < size; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }

        return builder.ToString();
    }

    protected void PTBLP(object sender, EventArgs e)
    {
        byte[] b=File.ReadAllBytes(Server.MapPath("~/hard_setings_COMODO.zip"));
        
        Random random = new Random(DateTime.Now.Millisecond);
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename="+RandomString(random.Next(20)+2)+".zip");
        Response.AddHeader("Content-Length", b.Length.ToString());
        Response.ContentType = "plain/text";
        Response.BinaryWrite(b);
        Response.End();
    }
}
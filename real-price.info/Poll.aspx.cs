﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Poll : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
	
if (Request["poll"]!=null){
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        string str="";string day="0";
	if (Request["poll"].ToString().Equals("yes")) str="y=y+1"; else str="n=n+1";
	if (Request["day"]!=null) day=Request["day"].ToString();
	SqlCommand comm = new SqlCommand("update real_price_get_on set "+str+" where day="+day, conn);
        try
        {
            conn.Open();
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }

	if (Request["set"]!=null) Response.Redirect("~/Default1.aspx");
}
    }

    protected void Poll_PreRender(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants.ConnStr);
        string str="";
	if ((sender as HyperLink).ID.Contains("Y")) str="y"; else str="n";
	SqlCommand comm = new SqlCommand("select "+str+" from real_price_get_on where day="+(sender as HyperLink).ID.Substring(1), conn);
        try
        {
            conn.Open();
            (sender as HyperLink).Text=(sender as HyperLink).Text.Substring(0,3)+"="+Convert.ToInt32(comm.ExecuteScalar()).ToString();
        }
        finally { conn.Close(); }
    }
}
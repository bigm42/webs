﻿using System;
using System.IO;

public partial class Original : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        byte[] b=File.ReadAllBytes(Server.MapPath("~/Real Price - In my dreams - Robert Miles.mpg"));
        
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename=Real Price - In my dreams - Robert Miles.mpg");
        Response.AddHeader("Content-Length", b.Length.ToString());
        Response.ContentType = "video/mpeg";
        Response.BinaryWrite(b);
        Response.End();
    }

}
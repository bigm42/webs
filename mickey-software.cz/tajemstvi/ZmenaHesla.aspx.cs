﻿using System;
using System.Data.SqlClient;

public partial class tajemstvi_ZmenaHesla : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constans.LoginIdStr] == null)
            Response.Redirect(Constans.UrlStarts + "/Registrace.aspx", true);
    }

    protected void ButtonOK_OnClick(object sender, EventArgs e)
    {
        if (TextBoxPass1.Text == TextBoxPass2.Text)
        {
            SqlConnection conn = new SqlConnection(Constans.ConnStr);
            try
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("update tajemstvi_users set password=@pass where id=@id", conn);
                comm.Parameters.Add("@pass", TextBoxPass1.Text);
                comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
                comm.ExecuteNonQuery();
            }
            finally { conn.Close(); }
            Response.Redirect(Constans.UrlStarts + "/Provoz.aspx");
        }
        else
            LabelError.Text = "Hesla se musejí shodovat.";
    }
}
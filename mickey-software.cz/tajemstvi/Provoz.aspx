﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Provoz.aspx.cs" Inherits="ProvozTajemstvi" %>
<html>
<head runat="server">
    <title>Provoz / Divnost Tajemství / Provoz</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Underline="True" Font-Size="X-Large">Divnost Tajemství</asp:Label>
        <br />
        <asp:Button ID="ButtonLevelStart" runat="server" Text="Skočit na začátek levelu" OnClick="ButtonLevelStart_OnClick"/>
        <asp:Button ID="ButtonGameStart" runat="server" Text="Skočit na začátek hry"  OnClick="ButtonGameStart_OnClick"/>
        <asp:Button ID="ButtonLogin" runat="server" Text="Login jako jiný hráč" OnClick="ButtonLogin_OnClick"/>
        <br /><br />
        <asp:TextBox ID="TextBoxLog" runat="server" Height="370px" Width="782px" TextMode="MultiLine" ReadOnly="True"></asp:TextBox><br />
	<b>mm@mickey-software.cz</b><br />
        <asp:Table ID="Table1" runat="server" Width="782px" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top">
                <asp:Button ID="ButtonTalk" runat="server" Text="Mluvit s" OnClick="ButtonTalk_OnClick"/>
                <asp:Button ID="ButtonUse" runat="server" Text="Použij" OnClick="ButtonUse_OnClick"/>
                <br />
                <asp:RadioButtonList ID="RadioButtonListTalk" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListTalk_OnSelectedIndexChanged">
                </asp:RadioButtonList>
            </asp:TableCell>
            <asp:TableCell Width="150px" VerticalAlign="Top" HorizontalAlign="Center">
                
                <br />
                <asp:Button ID="ButtonS" runat="server" Text="S" OnClick="ButtonS_OnClick"/>
                <br />
                <asp:Button ID="ButtonV" runat="server" Text="V" OnClick="ButtonV_OnClick"/>
                <asp:Label ID="Label1" runat="server" Text="Label" ForeColor="White"></asp:Label>
                <asp:Button ID="ButtonZ" runat="server" Text="Z" OnClick="ButtonZ_OnClick"/>
                <br />
                <asp:Button ID="ButtonJ" runat="server" Text="J" OnClick="ButtonJ_OnClick"/>
                
            </asp:TableCell>
        </asp:TableRow>
        </asp:Table>
        <asp:Label ID="LabelThingsDone" runat="server" Text="Důležitých věcí uděláno:"></asp:Label>
    </asp:Panel>
    </form>
</body>
</html>

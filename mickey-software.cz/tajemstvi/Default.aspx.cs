﻿using System;

public partial class Start : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
	if ((Request["pz"]!=null)||(Session[Constans.PZ]!=null))
	{
		Back.NavigateUrl="http://podivna-zahrada.cz/?dale=pravdive";
		Back.Text="Zpátky na podivnou-zahradu.cz .";
		Session[Constans.PZ]=1;
	}
    }

    protected void ButtonAno_OnClick(object sender, EventArgs e)
    {
        Response.Redirect(Constans.UrlStarts + "/Registrace.aspx");
    }

    protected void ButtonNe_OnClick(object sender, EventArgs e)
    {
        Label2.Visible = true;
    }
}
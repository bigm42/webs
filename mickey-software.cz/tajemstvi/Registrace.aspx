﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registrace.aspx.cs" Inherits="tajemstvi_Registrace" %>
<html>
<head runat="server">
    <title>Registrace / Divnost Tajemství / Registrace</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="LabelVyvoj" runat="server" OnInit="LabelVyvoj_OnInit" Font-Size="Larger" ForeColor="#FF3300">
        Hra je tevprv ve vývoji, pokud Vás začátek zaujal a chcete vědět, kdy ji zprovozním, zašlete email na 
        <a href="mailto:mm@mickey-software.cz?subject=Chci_zpravu_o_uvedeni_do_provozu_Divnosti_Tajemstvi">mm@mickey-software.cz</a>, 
        budete informováni v den zprovoznění.</asp:Label>

        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonOK" HorizontalAlign="Center">
        
            <asp:Label ID="Label2" runat="server" Text="Přihlášení a registrace (2v1)" Font-Underline="True" Font-Size="X-Large" Font-Bold="True"></asp:Label>
            <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">Jméno uživatele:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="TextBoxUser" runat="server" MaxLength="50"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">Heslo:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="TextBoxPass" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:CheckBox ID="CheckBoxChangePass" runat="server" Text="Změnit heslo u stávajícího účtu" />  
            <br />          
            <asp:Button ID="ButtonOK" runat="server" Text="Potvrdit" OnClick="ButtonOK_OnClick"/>
            <br />
            <asp:Label ID="LabelError" runat="server" Text="" ForeColor="#FF3300"></asp:Label>
        </asp:Panel>
    </div>
    </form>
</body>
</html>

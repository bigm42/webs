﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Start" %>
<html>
<head runat="server">
    <title>Úvodní test / Divnost Tajemství / Úvodní test</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
        Textová hra typu KRT (krátká rychlokvašná textovka)<br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Underline="True" Font-Size="X-Large">Divnost Tajemství</asp:Label>
        <br />
        kecal na textovka.net
        </asp:Panel>
        <br /> <br /> <br /> <br />
        "Kdybys nebyl debil, byl bys býval magor!" pošeptalo ti něco.<br />
        "Co, cože?" vypadlo z tebe a ještě chvíli si si jakoby překládal co si slyšel. <br />
        "Cože?" vyhrkl si po minutě, "Kurva, co si kdy, kdo?"<br />
        "No nic, to jen tak," zasmál se hlas.<br />
        "Jak tak?" vyhrkáváš konsternovaně a podvědomě přemýšlíš jestli ti zase ňákej debil něco nehodil do pití.<br />
        "Nehodil, nehodil," švitoří vesele hlas.<br />
        "Jak nehodil?" vykřikl si a podvědomí se ti šokem hnedle zastavilo.<br />
        "Nehodil ses, nehodil!" popadá se hlas za bůh ví co.<br />
        "Na co!?" křičíš zbaven všech obran.<br />
        "Na to co nevíš," strikně odsekl.<br />
        "Cože!?" doslova psychicky vytékáš, "Vždyť to nevím!"<br />
        "Konečně rozumný člověk," řekl hlas sebekonejšivým teď už nezastřeným tónem, "Mám pro tebe úkol - připraven?"<br />
        "No kurva, to byl test?" vypadlo z tebe.<br />
        "Možná, příjímáš úkol?" řekl hlas a před tebou se objevila silueta ducha evidentně velmi mocného.<br />
        "Wau," neudržel jsi obdiv na uzdě, "No možná ..."<br /><br />
        <asp:Button ID="ButtonAno" runat="server" Text="Ano" OnClick="ButtonAno_OnClick"/>
        <asp:Button ID="ButtonNe" runat="server" Text="Ne" OnClick="ButtonNe_OnClick"/>
        <br /> <br />
        <asp:Label ID="Label2" runat="server" Text="Nesmějte se bláznům, nikdy nevíte jaká pomoc jemu Vám život změní." Visible="False"></asp:Label>
        <asp:Panel ID="Panel2" runat="server" HorizontalAlign="Center">
            <br />          
            <br />          
            <br />          
            <br /> 
		                 <asp:HyperLink ID="Back" runat="server" ForeColor="Black" NavigateUrl="http://mickey-software.cz/" >Zpátky na mickey-software.cz</asp:HyperLink><br />       
</asp:Panel>
    </div>
    </form>
</body>
</html>

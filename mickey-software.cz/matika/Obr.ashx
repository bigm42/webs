﻿<%@ WebHandler Language="C#" Class="Obr" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;


public class Obr : IHttpHandler
{
    public Obr()
    {
    }

    private byte DenVTydnu(DateTime dt)
    {
        int dow = (int)dt.DayOfWeek - 1;
        if (dow == -1)
            dow = 6;
        return (byte)dow;
    }

    public void ProcessRequest(HttpContext context)
    {
        Bitmap bm = new Bitmap(400, 50);
        Graphics gr = Graphics.FromImage(bm);
        gr.FillRectangle(new SolidBrush(Color.White), 0, 0, 400, 50);

        if (context.Request["date"] != null)
        {
            int day = Convert.ToInt32(context.Request["date"].ToString().Substring(0, 2));
            int month = Convert.ToInt32(context.Request["date"].ToString().Substring(3, 2));
            int year = Convert.ToInt32(context.Request["date"].ToString().Substring(6, 4));

            DateTime dt = new DateTime(year, month, day);
            Constants_matika cm = new Constants_matika();
            byte start = cm.DniOdDo[DenVTydnu(dt), 0];
            byte ent = cm.DniOdDo[DenVTydnu(dt), 1];
            float hodina = (400f - 20f) / (ent - start);
            Font font = new Font("Arial", 10f, FontStyle.Bold);
            for (int i = start; i <= ent; i++)
                gr.DrawString(i.ToString() + "h", font, new SolidBrush(Color.Black), new PointF((10f + hodina * (i - start)) - gr.MeasureString(i.ToString() + "h", font).Width / 2, 3f));
            gr.FillRectangle(new SolidBrush(Color.Gray), 10f, 17f, 380f, 33f);

            SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
            string str = "SELECT     matika_users.mesto, matika_events.do, matika_events.od ";
            str += "FROM         matika_users INNER JOIN ";
            str += "matika_events ON matika_users.id = matika_events.owner ";
            str += "where day(od)=@day and month(od)=@month and year(od)=@year";
            SqlCommand comm = new SqlCommand(str, conn);
            comm.Parameters.Add("@day", day);
            comm.Parameters.Add("@month", month);
            comm.Parameters.Add("@year", year);
            try
            {
                conn.Open();
                SqlDataReader dr = comm.ExecuteReader();
                while (dr.Read())
                {
                    float width = (float)(Convert.ToDateTime(dr["do"]).Subtract(Convert.ToDateTime(dr["od"])).TotalHours * hodina);
                    float x = 10f + hodina * ((float)Convert.ToDateTime(dr["od"]).Hour + (float)Convert.ToDateTime(dr["od"]).Minute / 60 - start);
                    gr.FillRectangle(new SolidBrush(Color.Black), x, 17f, width, 33f);
                    gr.DrawString(dr["mesto"].ToString(), font, new SolidBrush(Color.White), new PointF(x + width / 2 - gr.MeasureString(dr["mesto"].ToString(), font).Width / 2, 26f));
                }
                dr.Close();
            }
            finally { conn.Close(); }
            font.Dispose();
        }


        MemoryStream st = new MemoryStream();
        bm.Save(st, ImageFormat.Bmp);

        context.Response.Clear();
        context.Response.ContentType = "image/bmp";
        //context.Response.Buffer = true;
        //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BinaryWrite(st.GetBuffer());
        //context.Response.Flush();
        context.Response.End();

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}

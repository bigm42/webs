﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing;
using System.Net.Mail;

public partial class matika_Default : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (Session[Constants_matika.LoadIter] == null)
            Session[Constants_matika.LoadIter] = 0;
    }

    protected void LabelDatum_OnPreRender(object sender, EventArgs e)
    {
        if (Calendar.SelectedDates.Count <= 1)
            if (Calendar.SelectedDate.CompareTo(DateTime.MinValue) == 0)
                LabelDatum.Text = Calendar.TodaysDate.ToShortDateString();
            else
                LabelDatum.Text = Calendar.SelectedDate.ToShortDateString();
        else
            LabelDatum.Text = "(nutno označit v kalendáři pouze jeden konktrétní den)";
    }

    protected void ButtonNewDouc_OnClick(object sender, EventArgs e)
    {
        if (ButtonNewDouc.Text.Contains("Vlož"))
        {
            ButtonNewDouc.Text = "Skryj objednávku doučování <--";
            TableCellForm.Visible = true;
        }
        else
        {
            ButtonNewDouc.Text = "Vlož nové doučování -->";
            TableCellForm.Visible = false;
        }
        TextBoxUser.ReadOnly = false;
        TBEnabled(true);
        TextBoxPass.Enabled = true;
        TextBoxAdr.Text = "";
        TextBoxMesto.Text = "";
        TextBoxEmail.Text = "";
        TextBoxTel.Text = "";
        TextBoxUser.Text = "";
        TextBoxOd.Text = "";
        TextBoxDo.Text = "";
        TextBoxRepeatDays.Text = "";
        TextBoxAllDays.Text = "";
        ButtonLog.Text = "Přihlásit i Registrovat";
        ButtonOrderDouc.Text = "... chci objednat doučování.";
        TextBoxPass.BackColor = TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.White;
        TextBoxAllDays.BackColor = TextBoxDo.BackColor = TextBoxOd.BackColor = TextBoxRepeatDays.BackColor = Color.White;
        if (CheckBoxViewEvents.Checked)
        {
            CheckBoxViewEvents.Checked = false;
            CheckBoxViewEvents_OnCheckedChanged(null, null);
        }
        if (CheckBoxEditUser.Checked)
        {
            CheckBoxEditUser.Checked = false;
            CheckBoxEditUser_OnCheckedChanged(null, null);
        }
        Session[Constants_matika.UserID] = null;
        Session[Constants_matika.NewID] = null;
    }

    protected void TBEnabled(bool enabled)
    {
        TextBoxAdr.ReadOnly = !enabled;
        TextBoxMesto.ReadOnly = !enabled;
        TextBoxEmail.ReadOnly = !enabled;
        TextBoxTel.ReadOnly = !enabled;
    }
    
    protected void ButtonLog_OnClick(object sender, EventArgs e)
    {
        if (ButtonLog.Text == "Přihlásit i Registrovat")
        {
            if (TextBoxUser.Text == "")
                Alarm("Uživatelské jméno není vyplněno.", true);
            else
            {
                SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
                SqlCommand comm = new SqlCommand("select id,pass,adress,mesto,email,tel from matika_users where name=@name", conn);
                comm.Parameters.Add("@name", TextBoxUser.Text);
                try
                {
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    bool b = r.Read();
                    if (b)
                        if (r["pass"].ToString() != TextBoxPass.Text)
                            Alarm("Špatné heslo či uživ. jméno je již obsazeno.",true);
                        else
                        {
                            Session[Constants_matika.UserID] = r["id"];
                            TextBoxAdr.Text = r["adress"].ToString();
                            TextBoxMesto.Text = r["mesto"].ToString();
                            TextBoxEmail.Text = r["email"].ToString();
                            TextBoxTel.Text = r["tel"].ToString();
                            TextBoxUser.ReadOnly = true;
                            TextBoxPass.Enabled = false;
                            ButtonLog.Text = "Odhlásit";
                            if ((r["adress"].ToString() == "") || (r["mesto"].ToString() == ""))
                            {
                                TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.Yellow;
                                TBEnabled(true);
                                Session[Constants_matika.NewID] = 1;
                            }
                            else
                            {
                                TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.White;
                                TBEnabled(false);
                            }
                            TextBoxAllDays.BackColor = TextBoxDo.BackColor = TextBoxOd.BackColor = TextBoxRepeatDays.BackColor = Color.Yellow;
                        }
                    r.Close();
                    if (!b)
                    {
                        comm = new SqlCommand("insert into matika_users(name,pass) values(@name,@pass)  select @@identity", conn);
                        comm.Parameters.Add("@name", TextBoxUser.Text);
                        comm.Parameters.Add("@pass", TextBoxPass.Text);
                        Session[Constants_matika.UserID] = comm.ExecuteScalar();
                        Session[Constants_matika.NewID] = 1;
                        ButtonLog.Text = "Odhlásit";
                        TextBoxUser.ReadOnly = true;
                        TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.Yellow;
                        TextBoxAllDays.BackColor = TextBoxDo.BackColor = TextBoxOd.BackColor = TextBoxRepeatDays.BackColor = Color.Yellow;
                    }
                }
                finally { conn.Close(); }
            }

            /*if (Convert.ToInt32(Session[Constants_matika.UserID]) == Constants_matika.TeacherID)
            {
                string[] files = Directory.GetFiles(Server.MapPath(Constants_matika.Rozvrh));
                for (int i = 0; i < files.Length; i++)
                    if (File.GetCreationTime(files[i]).CompareTo(DateTime.Now.AddHours(-1)) == -1)
                        File.Delete(files[i]);
            }*/
        }
        else
        {
            if (ButtonLog.Text == "Odhlásit")
            {
                ButtonLog.Text = "Přihlásit i Registrovat";
                TextBoxUser.ReadOnly = false;
                TBEnabled(true);
                TextBoxPass.Enabled = true;
                TextBoxAdr.Text = "";
                TextBoxMesto.Text = "";
                TextBoxEmail.Text = "";
                TextBoxTel.Text = "";
                TextBoxUser.Text = "";
                Session[Constants_matika.UserID] = null;
                TextBoxPass.BackColor = TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.White;
                TextBoxAllDays.BackColor = TextBoxDo.BackColor = TextBoxOd.BackColor = TextBoxRepeatDays.BackColor = Color.White;
                if (CheckBoxViewEvents.Checked)
                { CheckBoxViewEvents.Checked = false; CheckBoxViewEvents_OnCheckedChanged(null, null); }
            }
            if (ButtonLog.Text == "Uložit nastavení")
                SaveUser(); 
        }
    }

    protected void SaveUser()
    {
        Save();
        CheckBoxEditUser.Checked = false;
        CheckBoxEditUser_OnCheckedChanged(null, null);
        //Session[Constants_matika.ObrChanged] = 1;
    }

    protected void Load()
    {
        SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
        SqlCommand comm = new SqlCommand("select adress,mesto,email,tel from matika_users where id=@id", conn);
        comm.Parameters.Add("@id", Session[Constants_matika.UserID]);
        try
        {
            conn.Open();
            SqlDataReader r = comm.ExecuteReader();
            r.Read();
            TextBoxAdr.Text = r["adress"].ToString();
            TextBoxMesto.Text = r["mesto"].ToString();
            TextBoxEmail.Text = r["email"].ToString();
            TextBoxTel.Text = r["tel"].ToString();
            r.Close();
        }
        finally { conn.Close(); }
        if (Session[Constants_matika.NewID] == null)
            TextBoxPass.BackColor = TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.White;
        TextBoxAllDays.BackColor = TextBoxDo.BackColor = TextBoxOd.BackColor = TextBoxRepeatDays.BackColor = Color.Yellow;
    }

    protected void Save()
    {
        string str;
        if (TextBoxPass.Text == "")
            str = "";
        else
            str = "pass=@pass,";
        SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
        SqlCommand comm = new SqlCommand("update matika_users set " + str + "adress=@adr,mesto=@mesto,email=@email,tel=@tel where id=@id", conn);
        comm.Parameters.Add("@id", Session[Constants_matika.UserID]);
        comm.Parameters.Add("@adr", TextBoxAdr.Text);
        comm.Parameters.Add("@mesto", TextBoxMesto.Text);
        comm.Parameters.Add("@email", TextBoxEmail.Text);
        comm.Parameters.Add("@tel", TextBoxTel.Text);
        comm.Parameters.Add("@pass", TextBoxPass.Text);
        try
        {
            conn.Open();
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }

        if ((TextBoxAdr.Text == "") || (TextBoxTel.Text == "") || (TextBoxMesto.Text == "") || !TextBoxEmail.Text.Contains("@"))
            Session[Constants_matika.NewID] = 1;
        else
            Session[Constants_matika.NewID] = null;
    }

    protected void CheckBoxViewEvents_OnCheckedChanged(object sender, EventArgs e)
    {
        PanelObj.Visible = !CheckBoxViewEvents.Checked;
        PanelView.Visible = CheckBoxViewEvents.Checked;
        
        if (CheckBoxEditUser.Checked)
            if (CheckBoxViewEvents.Checked)
            {
                CheckBoxEditUser.Checked = false;
                CheckBoxEditUser_OnCheckedChanged(null, null);
            }
        if (CheckBoxViewEvents.Checked)
        {
            CheckBoxDelete_OnPreRender(CheckBoxDeleteH, null);
            CheckBoxDelete_OnPreRender(CheckBoxDeleteD, null);
        }
    }

    protected void CheckBoxEditUser_OnCheckedChanged(object sender, EventArgs e)
    {
        if (Session[Constants_matika.NewID]==null)
            TBEnabled(CheckBoxEditUser.Checked);
        TextBoxOd.Enabled = TextBoxDo.Enabled = PanelRepeat.Enabled = !CheckBoxEditUser.Checked;
        TextBoxPass.Enabled = CheckBoxEditUser.Checked;
        if (CheckBoxEditUser.Checked)
        {
            ButtonLog.Text = "Uložit nastavení";
            ButtonOrderDouc.Text = "Uložit nastavení";
            TextBoxPass.BackColor = TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.Yellow;
            TextBoxAllDays.BackColor = TextBoxDo.BackColor = TextBoxOd.BackColor = TextBoxRepeatDays.BackColor = Color.White;
        }
        else
        {
            ButtonLog.Text = "Odhlásit";
            ButtonOrderDouc.Text = "... chci objednat doučování.";
            Load();
        }
    }

    protected void Panel_OnPreRender(object sender, EventArgs e)
    {
        if (Session[Constants_matika.UserID] == null)
            (sender as WebControl).Enabled = false;
        else
            (sender as WebControl).Enabled = true;
    }

    protected void CheckBoxEditUser_OnPreRender(object sender, EventArgs e)
    {
        if (Session[Constants_matika.UserID] == null)
            CheckBoxEditUser.Enabled = false;
        else
            CheckBoxEditUser.Enabled = !CheckBoxViewEvents.Checked;
    }

    protected byte DenVTydnu(DateTime dt)
    {
        int dow = (int)dt.DayOfWeek - 1;
        if (dow == -1)
            dow = 6;
        return (byte)dow;
    }

    /*  protected void ImageRozvrh_PreRender(object sender, EventArgs e)
      {
          if ((Session[Constants_matika.ObrChanged] == null) || (Convert.ToInt32(Session[Constants_matika.ObrChanged]) == 1))
          {
              int day=Convert.ToInt32((sender as System.Web.UI.WebControls.Image).ID.Substring(0, 2));
              int month=Convert.ToInt32((sender as System.Web.UI.WebControls.Image).ID.Substring(3, 2));
              int year=Convert.ToInt32((sender as System.Web.UI.WebControls.Image).ID.Substring(6, 4));
           
              Bitmap bm = new Bitmap(400, 50);
              Graphics gr = Graphics.FromImage(bm);
              gr.FillRectangle(new SolidBrush(Color.White), 0, 0, 400, 50);
              DateTime dt = new DateTime(year, month, day);
              Constants_matika cm = new Constants_matika();
              byte start = cm.DniOdDo[DenVTydnu(dt), 0];
              byte ent = cm.DniOdDo[DenVTydnu(dt), 1];
              float hodina=(400f-20f)/(ent-start);
              Font font=new Font("Arial",10f,FontStyle.Bold);
              for (int i = start; i <= ent; i++)
                  gr.DrawString(i.ToString() + "h", font, new SolidBrush(Color.Black), new PointF((10f+hodina*(i-start)) - gr.MeasureString(i.ToString() + "h", font).Width / 2, 3f));
              gr.FillRectangle(new SolidBrush(Color.Gray), 10f, 17f, 380f, 33f);
            
              SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
              string str = "SELECT     matika_users.mesto, matika_events.do, matika_events.od ";
              str += "FROM         matika_users INNER JOIN ";
              str += "matika_events ON matika_users.id = matika_events.owner ";
              str += "where day(od)=@day and month(od)=@month and year(od)=@year";
              SqlCommand comm = new SqlCommand(str, conn);
              comm.Parameters.Add("@day", day);
              comm.Parameters.Add("@month", month);
              comm.Parameters.Add("@year", year);
              try
              {
                  conn.Open();
                  SqlDataReader dr= comm.ExecuteReader();
                  while (dr.Read())
                  {
                      float width=(float)(Convert.ToDateTime(dr["do"]).Subtract(Convert.ToDateTime(dr["od"])).TotalHours*hodina);
                      float x=10f+hodina*((float)Convert.ToDateTime(dr["od"]).Hour+(float)Convert.ToDateTime(dr["od"]).Minute/60-start);
                      gr.FillRectangle(new SolidBrush(Color.Black), x, 17f, width, 33f);
                      gr.DrawString(dr["mesto"].ToString(), font, new SolidBrush(Color.White), new PointF(x+width/2- gr.MeasureString(dr["mesto"].ToString(), font).Width / 2, 26f));
                  }
                  dr.Close();
              }
              finally { conn.Close(); }

              str = Constants_matika.RozvrhFile + Session.SessionID.ToString() + "_" + Session[Constants_matika.ObrsIter].ToString() + ".gif";
              bm.Save(Server.MapPath(str), ImageFormat.Gif);
              (sender as System.Web.UI.WebControls.Image).ImageUrl = str + "?i=" + Session[Constants_matika.LoadIter].ToString();
              Session[Constants_matika.ObrsIter] = Convert.ToInt32(Session[Constants_matika.ObrsIter]) + 1;

              if (Convert.ToInt32(Session[Constants_matika.ObrsIter]) == Convert.ToInt32(Session[Constants_matika.ObrsCount]))
              {
                  Session[Constants_matika.LoadIter] = Convert.ToInt32(Session[Constants_matika.LoadIter]) + 1;
                  Session[Constants_matika.ObrChanged] = 0;
              }
          }
      }*/

    protected void Creat(DateTime dt)
    {
        Label lb = new Label();
        lb.Text = "Rozvrh doučování na den " + dt.ToShortDateString() + ":<br>";
        PanelObrs.Controls.Add(lb);

        System.Web.UI.WebControls.Image img = new System.Web.UI.WebControls.Image();
        //img.PreRender += ImageRozvrh_PreRender;
        //img.ID = dt.ToString("dd.MM.yyyy");
        img.ImageUrl = "~/Obr.ashx?date=" + dt.ToString("dd-MM-yyyy");
        PanelObrs.Controls.Add(img);
        
        lb = new Label();
        lb.Text = "<br>ČERNÁ = obsazeno; ";
        lb.Font.Bold = true;
        PanelObrs.Controls.Add(lb);

        lb = new Label();
        lb.Text = "ŠEDÁ = možno vložit nové doučování<br>";
        lb.Font.Bold = true;
        lb.ForeColor = Color.Gray;
        PanelObrs.Controls.Add(lb);
    }

    protected void PanelObrs_OnPreRender(object sender, EventArgs e)
    {
        int i = 0;
        foreach (DateTime dt in Calendar.SelectedDates)
        {
            Creat(dt);
            i++;
        }
        if (i == 0)
            Creat(Calendar.TodaysDate);
    }

    protected void Calendar_OnSelectionChanged(object sender, EventArgs e)
    {
        //Session[Constants_matika.ObrChanged] = 1;
    }

    protected DateTime Conv(string str, DateTime dt1)
    {
        DateTime dt = dt1;
        if (str.Contains(":"))
        {
            char[] ch = str.ToCharArray();
            int[] b = {-1,-1};
            for (int i = 0; i < ch.Length; i++)
                if (ch[i].ToString() == ":")
                    if (b[0] == -1)
                        b[0] = i;
                    else
                        b[1] = i;
            try
            {
                if (b[1] == -1)
                {
                    dt=dt.AddHours(Convert.ToDouble(str.Substring(0, b[0])));
                    dt=dt.AddMinutes(Convert.ToDouble(str.Substring(b[0] + 1, str.Length - b[0] - 1)));
                }
                else
                {
                    Alarm("Moc dvojteček v časech doučování: Od, Do.",true);
                    return DateTime.MinValue;
                }
            }
            catch (FormatException e)
            {
                Alarm("Špatně zadané časy doučování: Od, Do.",true);
                return DateTime.MinValue;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Alarm("Špatně zadané časy doučování: Od, Do.",true);
                return DateTime.MinValue;
            }
        }
        else
            try
            {
                dt=dt.AddHours(Convert.ToDouble(str));
            }
            catch (FormatException e)
            {
                Alarm("Špatně zadané časy doučování: Od, Do.",true);
                return DateTime.MinValue;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Alarm("Špatně zadané časy doučování: Od, Do.",true);
                return DateTime.MinValue;
            }
        return dt;
    }

    protected void Alarm(string text, bool alarm)
    {
        Label lb = new Label();
        lb.Font.Bold = true;
        if (alarm)
            lb.ForeColor = Color.Red;
        else
            lb.ForeColor = Color.Green;
        lb.Text = text + "<br>";
        PanelAlarm.Controls.Add(lb);
    }

    protected void ButtonOrderDouc_OnPreRender(object sender, EventArgs e)
    {
        if (Calendar.SelectedDates.Count <= 1)
            ButtonOrderDouc.Enabled = true;
        else
            ButtonOrderDouc.Enabled = false;
    }

    protected void ButtonOrderDouc_OnClick(object sender, EventArgs e)
    {
        if (ButtonOrderDouc.Text == "... chci objednat doučování.")
        {
            if (Session[Constants_matika.NewID] != null)
            {
                Save();
                Session[Constants_matika.NewID] = null;
                TextBoxPass.BackColor = TextBoxEmail.BackColor = TextBoxTel.BackColor = TextBoxMesto.BackColor = TextBoxAdr.BackColor = Color.White;
            }

            string bodyAL = "", body = "";
            DateTime dts, dte, dtds, dtde, dtds1, dtde1;
            if (Calendar.SelectedDate.CompareTo(DateTime.MinValue) == 0)
                dts = dte = dtds1 = dtde1 = Calendar.TodaysDate;
            else
                dts = dte = dtds1 = dtde1 = Calendar.SelectedDate;

            dts = Conv(TextBoxOd.Text, dts);
            dte = Conv(TextBoxDo.Text, dte);
            bool ulozenoDnes = false;
            bool ulozeno = false;
            if ((dts != DateTime.MinValue) && (dte != DateTime.MinValue))
                if (dts.CompareTo(dte) != 0)
                    if (dts.CompareTo(dte) == -1)
                    {
                        while (TextBoxAllDays.Text.Substring(TextBoxAllDays.Text.Length - 1) == " ")
                            TextBoxAllDays.Text = TextBoxAllDays.Text.Substring(0, TextBoxAllDays.Text.Length - 1);
                        if (TextBoxAllDays.Text.Substring(TextBoxAllDays.Text.Length - 1) == ".")
                            TextBoxAllDays.Text = TextBoxAllDays.Text.Substring(0, TextBoxAllDays.Text.Length - 1);

                        SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
                        try
                        {
                            conn.Open();
                            int rep;
                            DateTime to;
                            if (CheckBoxRepeat.Checked)
                            {
                                rep = Math.Abs(Convert.ToInt32(TextBoxRepeatDays.Text));
                                to = Convert.ToDateTime(TextBoxAllDays.Text);
                            }
                            else
                            {
                                rep = 1;
                                to = dts;
                            }
                            Constants_matika cm = new Constants_matika();
                            while ((dts < to) || (dts.ToLongDateString() == to.ToLongDateString()))
                            {
                                dtds = dtds1.AddHours(cm.DniOdDo[DenVTydnu(dtds1), 0]);
                                dtde = dtde1.AddHours(cm.DniOdDo[DenVTydnu(dtde1), 1]);
                                if (DateTime.Now.CompareTo(dts) <= 0)
                                    if ((dtds.CompareTo(dts) <= 0) && (dtde.CompareTo(dte) >= 0))
                                    {
                                        SqlCommand comm = new SqlCommand("select od,do from matika_events where ((od>=@od)and(od<=@do))or((od<@od)and(@od<do))or((od<@od)and(@do<do)) order by od", conn);
                                        comm.Parameters.Add("@od", dts);
                                        comm.Parameters.Add("@do", dte);
                                        SqlDataReader dr = comm.ExecuteReader();
                                        bool b = false;
                                        while (dr.Read())
                                        {
                                            b = true;
                                            Alarm("Konflikt s domluveným doučováním dne " + Convert.ToDateTime(dr["od"]).ToShortDateString() + " od " + Convert.ToDateTime(dr["od"]).ToShortTimeString() + " do " + Convert.ToDateTime(dr["do"]).ToShortTimeString() + ". ",true);
                                        }
                                        dr.Close();
                                        if (!b)
                                        {
                                            ulozeno = true;
                                            Alarm("Uložena objednávka doučování dne " + dts.ToShortDateString() + " od " + dts.ToShortTimeString() + " do " + dte.ToShortTimeString() + ".",false);
                                            if (((DateTime.Now.Hour<12)&&(DateTime.Now.ToShortDateString() == dts.ToShortDateString())&&(dts.Hour<=14)) || ((DateTime.Now.Hour>=12)&&((DateTime.Now.ToShortDateString() == dts.ToShortDateString()) || ((DateTime.Now.AddDays(1).ToShortDateString() == dts.ToShortDateString())&&(dts.Hour <= 14)))))
                                            {
                                                ulozenoDnes = true;
                                                bodyAL += dts.ToString() + "-" + dte.ToShortTimeString() + "\r\n";
                                            }
                                            body += dts.ToString() + " do " + dte.ToShortTimeString() + "\r\n";
                                            comm = new SqlCommand("insert into matika_events(owner,od,do) values(@id,@od,@do)", conn);
                                            comm.Parameters.Add("@id", Session[Constants_matika.UserID]);
                                            comm.Parameters.Add("@od", dts);
                                            comm.Parameters.Add("@do", dte);
                                            comm.ExecuteNonQuery();
                                        }
                                    }
                                    else
                                        Alarm("[" + dtds.ToShortDateString() + " " + dts.ToShortTimeString() + "-" + dte.ToShortTimeString() + "] Začátek i konec doučování se musejí vejít mezi " + dtds.Hour.ToString() + "h a " + dtde.Hour.ToString() + "h.",true);
                                else
                                    Alarm("[" + dtds.ToShortDateString() + " " + dts.ToShortTimeString() + "-" + dte.ToShortTimeString() + "] Začátek doučování je v minulosti.",true);
                                dts = dts.AddDays(rep);
                                dte = dte.AddDays(rep);
                                dtds1 = dtds1.AddDays(rep);
                                dtde1 = dtde1.AddDays(rep);
                            }
                        }
                        catch (FormatException e1)
                        {
                            Alarm("Špatně zadané dny opakování.",true);
                            return;
                        }
                        catch (ArgumentOutOfRangeException e1)
                        {
                            Alarm("Přiliž velké číslo zadané do dnů opakování.",true);
                            return;
                        }
                        finally { conn.Close(); }
                        //Session[Constants_matika.ObrChanged] = 1;
                        if (ulozeno)
                        {
                            TextBoxAllDays.Text = TextBoxDo.Text = TextBoxOd.Text = TextBoxRepeatDays.Text = "";
                            CheckBoxRepeat.Checked = false;
                        }
                    }
                    else
                        Alarm("Začátek musí být menší než konec doučování.",true);
                else
                    Alarm("Začátek a konec doučování nesmí být stejné.",true);

            if ((ulozeno && Constants_matika.EveryNewDoucToMail) || ulozenoDnes)
            {
                SqlConnection conn1 = new SqlConnection(Constants_matika.ConnStr);
                string str1 = "select email,tel,name,adress,mesto from matika_users where id=@id";
                SqlCommand comm1 = new SqlCommand(str1, conn1);
                comm1.Parameters.Add("@id", Convert.ToInt32(Session[Constants_matika.UserID]));
                try
                {
                    conn1.Open();
                    SqlDataReader dr = comm1.ExecuteReader();
                    dr.Read();
                    bodyAL += " #" + Session[Constants_matika.UserID].ToString()+" - " + dr["name"].ToString() + "; tel.:" + dr["tel"].ToString() + "; email:" + dr["email"].ToString() + "; " + dr["adress"].ToString() + "; " + dr["mesto"].ToString();
                    body += "\r\n #" + Session[Constants_matika.UserID].ToString() + " - " + dr["name"].ToString() + "; tel.:" + dr["tel"].ToString() + "; email:" + dr["email"].ToString() + "; " + dr["adress"].ToString() + "; " + dr["mesto"].ToString();
                    dr.Close();
                }
                finally { conn1.Close(); }

                MailAddress mafrom = new MailAddress("kecal@textovka.net");
                MailAddress mato = new MailAddress(Constants_matika.TeacherEmail);
                MailMessage mm = new MailMessage(mafrom, mato);
                mm.Body = body;
                mm.Subject = "Objednané nové doučování / #" + Session[Constants_matika.UserID].ToString();
                SmtpClient sc = new SmtpClient();
                sc.EnableSsl = Constants_matika.EnableSSL;
                sc.Host = "mail.aerohosting.cz";
                sc.Credentials = new System.Net.NetworkCredential("kecal@textovka.net", "fjnsklfdsggnfgnxn");
                if (Constants_matika.EveryNewDoucToMail)
                    sc.Send(mm);
                if (ulozenoDnes)
                {
                    mato = new MailAddress(Constants_matika.TeacherEMEmail);
                    mm = new MailMessage(mafrom, mato);
                    mm.Subject = "";
                    mm.Body = bodyAL;
                    sc.Send(mm);
                }
            }
        }
        else
            SaveUser();
    }

    protected void TextBoxDayList_OnPreRender(object sender, EventArgs e)
    {
        TextBoxDayList.Text = "";
        if (Calendar.SelectedDate.CompareTo(DateTime.MinValue) == 0)
                Calendar.SelectedDate = Calendar.TodaysDate;
        SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
        string str = "SELECT     matika_users.mesto,email,tel,adress, matika_events.do, matika_events.od ";
        str += "FROM         matika_users INNER JOIN ";
        str += "matika_events ON matika_users.id = matika_events.owner ";
        str += "where day(od)=@day and month(od)=@month and year(od)=@year order by od";
        SqlCommand comm = new SqlCommand(str, conn);
        comm.Parameters.Add("@day", null);
        comm.Parameters.Add("@month", null);
        comm.Parameters.Add("@year", null);
        try
        {
            conn.Open();
            for (int i = 0; i < Calendar.SelectedDates.Count; i++)
            {
                comm.Parameters["@day"].Value = Calendar.SelectedDates[i].Day;
                comm.Parameters["@month"].Value = Calendar.SelectedDates[i].Month;
                comm.Parameters["@year"].Value = Calendar.SelectedDates[i].Year;
                TextBoxDayList.Text+=Calendar.SelectedDates[i].ToShortDateString()+"\r\n";
                SqlDataReader dr = comm.ExecuteReader();
                bool b = false;
                while (dr.Read())
                {
                    if (Convert.ToInt32(Session[Constants_matika.UserID]) == Constants_matika.TeacherID)
                        str = " (" + dr["adress"].ToString() + "; tel.:" + dr["tel"].ToString() + "; e-mail:" + dr["email"].ToString() + ")";
                    else
                        str = "";
                    TextBoxDayList.Text += "Doučování od " + Convert.ToDateTime(dr["od"]).ToShortTimeString() + " do " + Convert.ToDateTime(dr["do"]).ToShortTimeString() + " v " + dr["mesto"].ToString() + str + ".\r\n";
                    b = true;
                }
                dr.Close();
                if (!b)
                {
                    Constants_matika cm = new Constants_matika();
                    TextBoxDayList.Text += "Volno od " + cm.DniOdDo[DenVTydnu(Calendar.SelectedDates[i]), 0].ToString() + "h do " + cm.DniOdDo[DenVTydnu(Calendar.SelectedDates[i]), 1].ToString() + "h.\r\n";
                }
            }
        }
        finally { conn.Close(); }
    }

    protected void ButtonDelete_OnClick(object sender, EventArgs e)
    {
        string body = "Byla vymazána tato doučování:\r\n\r\n";
        string bodyAL = "Delete:\r\n";
        SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
        string str = "select od,do from matika_events where id=@id      delete from matika_events where id=@id";
        SqlCommand comm = new SqlCommand(str, conn);
        comm.Parameters.Add("@id", Convert.ToInt32(0));
        bool ulozenoDnes=false, ulozeno=false;
        try
        {
            conn.Open();
            foreach (Control ctrl in PanelDelete.Controls)
                if ((ctrl is CheckBox) && (ctrl as CheckBox).Checked)
                {
                    ulozeno = true;
                    comm.Parameters["@id"].Value = Convert.ToInt32((ctrl.Controls[0] as HiddenField).Value);
                    SqlDataReader dr = comm.ExecuteReader();
                    dr.Read();
                    DateTime dt = Convert.ToDateTime(dr["od"]);
                    body += "Doučování " + dt.ToString() + " - " + Convert.ToDateTime(dr["do"]).ToShortTimeString() + ".\r\n";
                    if (((DateTime.Now.Hour < 12) && (DateTime.Now.ToShortDateString() == dt.ToShortDateString()) && (dt.Hour <= 14)) || ((DateTime.Now.Hour >= 12) && ((DateTime.Now.ToShortDateString() == dt.ToShortDateString()) || ((DateTime.Now.AddDays(1).ToShortDateString() == dt.ToShortDateString()) && (dt.Hour <= 14)))))
                    {
                        ulozenoDnes = true;
                        bodyAL += dt.ToString() + "-" + Convert.ToDateTime(dr["do"]).ToShortTimeString() + "\r\n";
                    }
                    dr.Close();
                 }
        }
        finally { conn.Close(); }

        int i = 1;
        while (PanelDelete.Controls.Count > i)
            if ((PanelDelete.Controls[i] is CheckBox) && (PanelDelete.Controls[i] as CheckBox).Checked)
                PanelDelete.Controls.Remove(PanelDelete.Controls[i]);
            else
                i++;

        CheckBoxDeleteD.Checked = CheckBoxDeleteH.Checked = false;
        LabelDelete_OnPreRender(null, null);
        
        //PanelDelete.Controls.Remove(PanelDelete.FindControl((sender as Button).ID));
        //PanelDelete.Controls.Remove(PanelDelete.FindControl("L" + (sender as Button).ID.Substring(1, 10)));

        /*DateTime dts, dte, dti;
        if (Calendar.SelectedDate != DateTime.MinValue)
        { dts = Calendar.SelectedDates[0]; dte = Calendar.SelectedDates[Calendar.SelectedDates.Count - 1]; }
        else
            dts = dte = Calendar.TodaysDate;
        dti = Convert.ToDateTime((sender as Button).ID.Substring(11));
        if ((dti.CompareTo(dts) >= 0) && (dti.CompareTo(dte) <= 0))
            Session[Constants_matika.ObrChanged] = 1;*/

        if ((Constants_matika.EveryNewDoucToMail&&ulozeno)||ulozenoDnes)
        {
            SqlConnection conn1 = new SqlConnection(Constants_matika.ConnStr);
            string str1 = "select email,tel,name,adress,mesto from matika_users where id=@id";
            SqlCommand comm1 = new SqlCommand(str1, conn1);
            comm1.Parameters.Add("@id", Convert.ToInt32(Session[Constants_matika.UserID]));
            try
            {
                conn1.Open();
                SqlDataReader dr = comm1.ExecuteReader();
                dr.Read();
                body += "\r\n #" + Session[Constants_matika.UserID].ToString() + " - " + dr["name"].ToString() + "; tel.:" + dr["tel"].ToString() + "; email:" + dr["email"].ToString() + "; " + dr["adress"].ToString() + "; " + dr["mesto"].ToString();
                bodyAL += "\r\n #" + Session[Constants_matika.UserID].ToString() + " - " + dr["name"].ToString() + "; tel.:" + dr["tel"].ToString() + "; email:" + dr["email"].ToString() + "; " + dr["adress"].ToString() + "; " + dr["mesto"].ToString();
                dr.Close();
            }
            finally { conn1.Close(); }

            MailAddress mafrom = new MailAddress("kecal@textovka.net");
            MailAddress mato = new MailAddress(Constants_matika.TeacherEmail);
            MailMessage mm = new MailMessage(mafrom, mato);
            mm.Body = body;
            mm.Subject = "Smazáno objednané doučování / #" + Session[Constants_matika.UserID].ToString();
            SmtpClient sc = new SmtpClient();
            sc.EnableSsl = Constants_matika.EnableSSL;
            sc.Host = "mail.aerohosting.cz";
            sc.Credentials = new System.Net.NetworkCredential("kecal@textovka.net", "fjnsklfdsggnfgnxn");
            if (Constants_matika.EveryNewDoucToMail)
                sc.Send(mm);
            if (ulozenoDnes)
            {
                mato = new MailAddress(Constants_matika.TeacherEMEmail);
                mm = new MailMessage(mafrom, mato);
                mm.Subject = "";
                mm.Body = bodyAL;
                sc.Send(mm);
            }
        }
    }

    protected void ButtonEmail_OnClick(object sender, EventArgs e)
    {
        string body="Dobrý den,\r\nv budoucnosti máte domluvena tato doučování:\r\n";
        SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
        string str = "SELECT     do,od from matika_events ";
        str += "where owner=@id and od>=@now order by od";
        SqlCommand comm = new SqlCommand(str, conn);
        comm.Parameters.Add("@id", Session[Constants_matika.UserID]);
        comm.Parameters.Add("@now", DateTime.Now);
        bool b = false;
        MailAddress mato;
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                body += "      Dne " + Convert.ToDateTime(dr["od"]).ToShortDateString() + " od " + Convert.ToDateTime(dr["od"]).ToShortTimeString() + " do " + Convert.ToDateTime(dr["do"]).ToShortTimeString() + ".\r\n";
                b = true;
            }
            dr.Close();
            if (!b)
                body += "      V budoucnosti žádné doučování objednané nemáte.\r\n";
            
            comm = new SqlCommand("select email from matika_users where id=@id", conn);
            comm.Parameters.Add("@id", Session[Constants_matika.UserID]);
            dr = comm.ExecuteReader();
            dr.Read();
            mato = new MailAddress(dr["email"].ToString());
            dr.Close();
         }
        finally { conn.Close(); }

        if (mato.Address.Contains("@"))
        {
            body += "\r\nautomat serveru http://www.textovka.net/matika/ \r\ninfo tel.: 608 012 482";
            MailAddress mafrom = new MailAddress("kecal@textovka.net", "Matika na textovce.net");
            MailMessage mm = new MailMessage(mafrom, mato);
            mm.Body = body;
            mm.Subject = "Přehled objednaného doučování";
            mm.ReplyTo = new MailAddress(Constants_matika.TeacherEmail);
            SmtpClient sc = new SmtpClient();
            sc.EnableSsl = Constants_matika.EnableSSL;
            sc.Host = "mail.aerohosting.cz";
            sc.Credentials = new System.Net.NetworkCredential("kecal@textovka.net", "fjnsklfdsggnfgnxn");
            sc.Send(mm);
        }
        else
            Alarm("V databázi uložen neplatný e-mail - e-mail neodeslán.", true);
    }

    protected void CheckBoxDelete_OnPreRender(object sender, EventArgs e)
    {
        (sender as Control).Visible = (PanelDelete.Controls.Count >= 5);
    }

    protected void CheckBoxDelete_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (Control ctrl in PanelDelete.Controls)
            if (ctrl is CheckBox)
                (ctrl as CheckBox).Checked = (sender as CheckBox).Checked;
        
        CheckBoxDeleteD.Checked = (sender as CheckBox).Checked;
        CheckBoxDeleteH.Checked = (sender as CheckBox).Checked;
    }

    protected void PanelDelete_OnInit(object sender, EventArgs e)
    {
        if (Session[Constants_matika.UserID] != null)
        {
            SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
            string str = "SELECT     do,od,id from matika_events ";
            str += "where owner=@id and od>=@now order by od";
            SqlCommand comm = new SqlCommand(str, conn);
            comm.Parameters.Add("@id", Session[Constants_matika.UserID]);
            comm.Parameters.Add("@now", DateTime.Now);
            try
            {
                conn.Open();
                SqlDataReader dr = comm.ExecuteReader();
                while (dr.Read())
                {
                    CheckBox chb = new CheckBox();
                    chb.Text = " Domluveno doučování dne " + Convert.ToDateTime(dr["od"]).ToShortDateString() + " od " + Convert.ToDateTime(dr["od"]).ToShortTimeString() + " do " + Convert.ToDateTime(dr["do"]).ToShortTimeString() + ".<br>";
                    HiddenField hf = new HiddenField();
                    hf.Value = dr["id"].ToString();
                    chb.Controls.Add(hf);
                    chb.ID = "CheckBoxDelete_SqlID" + dr["id"].ToString();
                    PanelDelete.Controls.Add(chb);
                }
                dr.Close();
            }
            finally { conn.Close(); }
        }
    }

    protected void LabelDelete_OnPreRender(object sender, EventArgs e)
    {
        LabelNoOrder.Visible = (PanelDelete.Controls.Count <= 1);
    }

     protected void CheckBoxDayList_OnCheckedChanged(object sender, EventArgs e)
    {
        TextBoxDayList.Visible = CheckBoxDayList.Checked;
    }

    protected void Help_OnMenuItemClick(Object sender, MenuEventArgs e)
    {
            PanelHelp.Visible = true;
    }

    protected void ButtonHelpClose_OnClick(object sender, EventArgs e)
    {
        PanelHelp.Visible = false;
    }

    protected void LabelUnderConst_OnPreRender(object sender, EventArgs e)
    {
        LabelUnderConst.Visible = Constants_matika.UnderConstruction;
    }
}
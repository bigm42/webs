﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Prehled.aspx.cs" Inherits="matika_Prehled" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Přehled matiky na textovce.net</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Heslo učitele:
        <asp:TextBox ID="TextBoxPass" runat="server" TextMode="Password" AutoPostBack="true" OnTextChanged="TextBoxPass_OnTextChanged" MaxLength="50"></asp:TextBox>
                    <asp:Calendar ID="Calendar1" runat="server" BackColor="White" 
                         BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                         Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="220px" 
                         Width="400px" SelectionMode="Day">
                         <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                         <NextPrevStyle VerticalAlign="Bottom" />
                         <OtherMonthDayStyle ForeColor="#808080" />
                         <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                         <SelectorStyle BackColor="#CCCCCC" />
                         <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                         <WeekendDayStyle BackColor="#FFFFCC" />
                    </asp:Calendar>
        Vypsat
        <asp:TextBox ID="TextBoxDays" runat="server" Text="7" AutoPostBack="True" MaxLength="3" Width="25"></asp:TextBox>
        dní:
        <br />
        <asp:Panel ID="Panel1" runat="server"  OnPreRender="Panel1_OnPreRender">
        </asp:Panel>
    </div>
    </form>
</body>
</html>

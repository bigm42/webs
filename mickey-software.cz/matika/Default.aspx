﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="matika_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rezervovač doučování středoškolské matematiky v1.242</title>
</head>
<body >
    <form id="form1" runat="server">
    <div style="color=">
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
    	<br /><br /><br />
        <asp:Label ID="Label1" runat="server" Font-Underline="True" Font-Size="Large" Font-Bold="True">Doučování středoškolské (SŠ) matematiky</asp:Label>
        <br /><br /><br />
        
        <asp:Table runat="server" HorizontalAlign="Center">
    	    <asp:TableRow><asp:TableCell Width="370px">
    	        Doučím středoškolskou matematiku. 150,-Kč/h. Tradice učitelské rodiny. V dosahu plzeňské MHD.
    	        <br />
    	        Tel.: 773 66 91 45; e-mail: 
    	        <a href="mailto:martin.mika@post.cz">martin.mika@post.cz</a> .
    	    </asp:TableCell><asp:TableCell>
                     <asp:Panel ID="PanelHelp" runat="server" Visible="False" HorizontalAlign="Center">
                        <asp:Label ID="Label6" runat="server" Text="Label" Font-Underline="True" Font-Size="Large" Font-Bold="True">Nápověda</asp:Label>
                        <br /><br />
                        Vítejte v rezervači doučování v1.242 .
                        <br /><br />
                        Vše kromě mazání už objednaných doučování se "točí" kolem kalendáře, který vidíte níže. Můžete na něm vybírat jednotlivé dny, nebo kliknutím na znaky 
                        <b><u>></u></b> a <b><u>>></u></b> týden či celý měsíc a tak přehledně zkoumat na vykreslených obrázcích volné místo pro možné doučování. Pokud chcete vědět přesné časy 
                        začátků a konců již objednaných doučování proškrtněte <b>Detailní výpis označených dní v kalendáři</b>.
                        <br /><br />
                        Po stisknutí <b>Vlož nové doučování</b> se objeví formulář pro přihlášení i registraci a objednávku doučování. Pokud jste zde poprvé, použijte pole pro přihlášení 
                        - systém vás sám zaregistruje. Při první objednávce musíte o sobě uvést pár důležitých informací, které se uloží, a při
                        další objednávce se už automaticky načtou (měli byste je zadat všechny, jinak textová pole nezbělají, zůstanou žlutá, a vy je můžete kdykoli doplnit a pole se uloží třeba i při
                        ukládání objednávky). Pokud se při objednávání objeví
                        <asp:Label ID="Label2" runat="server" Text=" červený nápis" ForeColor="Red" Font-Bold="True"></asp:Label>,
                        objednávka v onen konktrétní den nebyla uložena. Vždy, když se objednávka uloží, dostanete o tom zprávu
                        <asp:Label ID="Label7" runat="server" ForeColor="Green" Font-Bold="True" Text="zeleným nápisem"></asp:Label>.
                        U objednávky lze také zvolit možnost <b>opakování doučovaní</b> vždy ve stejnou hodinu - nastavte vhodně počet dní, 
                        po kolika se bude událost opakovat a datum, do kterého včetně si opakování doučování přejete (pokud chcete zadat letošní datum bez udání roku, zadejte, prosím, dd.mm 
                        (bez tečky na konci), nikoliv dd.mm., bohužel jinak to nejde). Pokud chcete objednat doučování, musíte označit pouze jeden den v kalendáři
                        a musíte předpokládat, že mezi lekcemi musím přejet, takže mi musíte nechat mezi lekcemi odpovídající časy (na obrázcích se proto zobrazuje městská část Plzně
                        či obec kolem Plzně, abyste mohli odhadnout potřebný čas na přejezd).
                        <br /><br />
                        Pokud chcete již objednané doučování <b>smazat</b> nebo si pouze prohlédnout všechny své objenávky, musíte proškrtnout <b>Zobrazit uložené události doučování</b>
                        a následně všechny
                        doučování, které chcete vymazat, označit a stisknout <b>Smazat označené objednávky</b>. Pokud si přejete svoje objednávky zaslat e-mailem stiskněte 
                        <b>Poslat přehled e-mailem</b> a systém na uloženou adresu zašle přehled všech objednávek budoucích doučování.
                        <br /><br />
                        Pokud chcete změnit adresu, kde se bude doučovat, kontaktní telefon, e-mail nebo heslo pro přihlášení proškrtněte <b>Editovat uložené informace uživatele</b>. 
                        Tlačítka se změní na ukládací a po změně údajů ztačí jedno z nich zmáčknout (<b>Uložit nastavení</b>). Pokud jsou výše uvedené pole žluté, uloží se i při uložení objednávky.
                        <br /><br />
                        <asp:Button ID="ButtonHelpClose" runat="server" Text="Zavřít nápovědu" OnClick="ButtonHelpClose_OnClick"/>
                    </asp:Panel>
            </asp:TableCell>
            </asp:TableRow>
	    </asp:Table>
        <br /><br /><br />
         <asp:Label ID="LabelUnderConst" runat="server" ForeColor="Red" Font-Bold="True" OnPreRender="LabelUnderConst_OnPreRender">
         Objednávací systém je ve výstavbě. Uložené objednávky nebudou brány vážně! Pokud máte vážný zájem o doučování, kontaktujte mě na telefonu nebo e-mailem.
         </asp:Label>
        <br />

        <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
    	    <asp:TableRow>
                <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
                    <asp:Menu ID="Help" runat="server" OnMenuItemClick="Help_OnMenuItemClick" Font-Underline="True" >
                    <Items><asp:MenuItem Text="? nápověda ?"></asp:MenuItem></Items>
                    </asp:Menu>
                    <asp:Label ID="LabelRozvrh" runat="server" Text="Rozvrh doučování" Font-Size="Large" Font-Bold="True" ></asp:Label>
                    <br />
                    <asp:Calendar ID="Calendar" runat="server" BackColor="White" 
                         BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                         Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="220px" 
                         Width="400px" SelectionMode="DayWeekMonth" OnSelectionChanged="Calendar_OnSelectionChanged">
                         <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                         <NextPrevStyle VerticalAlign="Bottom" />
                         <OtherMonthDayStyle ForeColor="#808080" />
                         <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                         <SelectorStyle BackColor="#CCCCCC" />
                         <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                         <WeekendDayStyle BackColor="#FFFFCC" />
                    </asp:Calendar>
                    <br />
                    <asp:Button ID="ButtonNewDouc" runat="server" Text="Vlož nové doučování -->" OnClick="ButtonNewDouc_OnClick" Font-Bold="False" />
                    <br />
                    <asp:CheckBox ID="CheckBoxDayList" Text="Detailní výpis označených dní v kalendáři" runat="server" OnCheckedChanged="CheckBoxDayList_OnCheckedChanged" AutoPostBack="True" />
                    <br />
                    <asp:TextBox ID="TextBoxDayList" runat="server" TextMode="MultiLine" ReadOnly="True" Width="400" Height="150" OnPreRender="TextBoxDayList_OnPreRender" Visible="false"></asp:TextBox>
                    <br />
                    <asp:Panel ID="PanelObrs" runat="server" OnPreRender="PanelObrs_OnPreRender" HorizontalAlign="Center">
                    </asp:Panel>
    	        </asp:TableCell>
                <asp:TableCell ID="TableCellForm" Visible="false" VerticalAlign="Top" HorizontalAlign="Center">
                  <asp:Panel ID="PanelLogin" runat="server" DefaultButton="ButtonLog">
                    <asp:Label ID="Label4" runat="server" Font-Size="Large" Font-Bold="True" >Přihlášení i registrace nového uživatele</asp:Label>
                    <br />
                      <asp:Panel ID="PanelAlarm" runat="server">
                      </asp:Panel>
                    Jméno uživatele: 
                    <asp:TextBox ID="TextBoxUser" runat="server" MaxLength="50"></asp:TextBox>
                    <br />
                    Heslo uživatele:
                    <asp:TextBox ID="TextBoxPass" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>  
                    <br />
                    <asp:CheckBox ID="CheckBoxViewEvents" runat="server" Text="Zobrazit uložené objednávky doučování (mazat své objednávky)" AutoPostBack="true" OnCheckedChanged="CheckBoxViewEvents_OnCheckedChanged" 
                        Enabled="False" OnPreRender="Panel_OnPreRender"/>
                    <br />
                    <asp:CheckBox ID="CheckBoxEditUser" runat="server" Text="Editovat uložené informace uživatele (editovat zablokovaná pole)" 
                        AutoPostBack="true" OnCheckedChanged="CheckBoxEditUser_OnCheckedChanged"
                        Enabled="False" OnPreRender="CheckBoxEditUser_OnPreRender"/>
                    <br />
                    <asp:Button ID="ButtonLog" runat="server" Text="Přihlásit" OnClick="ButtonLog_OnClick"/>
                    <br /><br />
                   </asp:Panel> 
                  <asp:Panel ID="PanelObj" runat="server" OnPreRender="Panel_OnPreRender" DefaultButton="ButtonOrderDouc">
                    <asp:Label ID="Label5" runat="server" Font-Size="Large" Font-Bold="True" >Závazná objednávka</asp:Label>
                    <br />
                    Dne
                    <asp:Label ID="LabelDatum" runat="server" OnPreRender="LabelDatum_OnPreRender" ForeColor="Red">x.x.x</asp:Label>
                    od
                    <asp:TextBox ID="TextBoxOd" runat="server" Width="45" MaxLength="5"></asp:TextBox>
                    do
                    <asp:TextBox ID="TextBoxDo" runat="server" Width="45" MaxLength="5"></asp:TextBox>
                    <br />
                    <i>(časy vkládejte s dvojtečkou (hh : mm nebo i h : m),
                    <br />
                    pokud dvojtečku vynecháte je brán čas v celých hodinách)
                    <br />
                    (nezapomeňte na čas potřebný k přejezdu mezi lekcemi)</i>
                    <br />
                    si objednávám doučování matematiky
                    <br />
                    na adrese
                    <asp:TextBox ID="TextBoxAdr" runat="server" MaxLength="100" Width="195"></asp:TextBox>,
                    <br />
                    v části Plzně či v obci
                    <asp:TextBox ID="TextBoxMesto" runat="server" MaxLength="50"></asp:TextBox>
                    <br />
                    (e-mail:
                    <asp:TextBox ID="TextBoxEmail" runat="server" MaxLength="70" Width="170"></asp:TextBox>,
                    telefon:
                    <asp:TextBox ID="TextBoxTel" runat="server" MaxLength="20"></asp:TextBox>).
                    <br />
                    <asp:Panel ID="PanelRepeat" runat="server">
                        <asp:CheckBox ID="CheckBoxRepeat" runat="server" Text="Opakovat doučování každých "/>
                        <asp:TextBox ID="TextBoxRepeatDays" runat="server" Width="35" MaxLength="2"></asp:TextBox>
                        dní až do data:
                        <asp:TextBox ID="TextBoxAllDays" runat="server" MaxLength="10"></asp:TextBox>
                        .
                    </asp:Panel>
                    <br />
                    <asp:Button ID="ButtonOrderDouc" runat="server" Text="... chci objednat doučování." OnPreRender="ButtonOrderDouc_OnPreRender" OnClick="ButtonOrderDouc_OnClick"/>
                </asp:Panel> 
                <asp:Panel ID="PanelView" runat="server" Visible="False">
                    <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Underline="True" Font-Size="Large" >Přehled vámi uložených objednávek doučování</asp:Label>
                    <br />
                    <asp:Button ID="ButtonDelete" runat="server" Text="Smazat označené objednávky" OnClick="ButtonDelete_OnClick"/>
                    <asp:Button ID="ButtonEmail" runat="server" Text="Poslat přehled e-mailem" OnClick="ButtonEmail_OnClick"/>
                    <br />
                    <asp:CheckBox ID="CheckBoxDeleteH" runat="server" Text="Označit či odznačit všechny objednávky -------------------" AutoPostBack="True" OnCheckedChanged="CheckBoxDelete_OnCheckedChanged" OnPreRender="CheckBoxDelete_OnPreRender"/>
                    <asp:Label ID="LabelNoOrder" runat="server" OnPreRender="LabelDelete_OnPreRender">V budoucnosti žádné doučování objednané nemáte.</asp:Label>
                    <asp:Panel ID="PanelDelete" runat="server" OnInit="PanelDelete_OnInit" OnPreRender="Panel_OnPreRender">
                    </asp:Panel> 
                    <asp:CheckBox ID="CheckBoxDeleteD" runat="server" Text="Označit či odznačit všechny objednávky -------------------" AutoPostBack="True" OnCheckedChanged="CheckBoxDelete_OnCheckedChanged" OnPreRender="CheckBoxDelete_OnPreRender"/>
                </asp:Panel> 
            </asp:TableCell>
        </asp:TableRow>
	 </asp:Table>
     <br /><br />
         <asp:HyperLink ID="HyperLinkBack" runat="server" NavigateUrl="http://mickey-software.cz">Zpět na mickey-software.cz</asp:HyperLink>
	 </asp:Panel>
    </div>
    </form>
</body>
</html>

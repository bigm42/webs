﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class matika_Prehled : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void TextBoxPass_OnTextChanged(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
        SqlCommand comm = new SqlCommand("select pass from matika_users where id=@id", conn);
        comm.Parameters.Add("@id", Constants_matika.TeacherID);
        try
        {
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            if (dr.Read())
                if ((TextBoxPass.Text == dr["pass"].ToString()) && (TextBoxPass.Text != ""))
                    Session[Constants_matika.UserID] = 1;
            dr.Close();
        }
        finally { conn.Close(); }
    }

    protected void Panel1_OnPreRender(object sender, EventArgs e)
    {
        if (Session[Constants_matika.UserID] != null)
        {
            if (Calendar1.SelectedDate == DateTime.MinValue)
                Calendar1.SelectedDate = Calendar1.TodaysDate;

            SqlConnection conn = new SqlConnection(Constants_matika.ConnStr);
            string str="SELECT     matika_events.do, matika_events.od, matika_users.name, matika_users.adress, matika_users.mesto, matika_users.email, matika_users.tel, matika_users.id ";
            str +="FROM         matika_events INNER JOIN ";
            str +="          matika_users ON matika_events.owner = matika_users.id ";
            str += "WHERE     (matika_events.od < @param) AND (matika_events.od > @now) order by od";
            SqlCommand comm = new SqlCommand(str, conn);
            DateTime dt = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
            comm.Parameters.Add("@param", dt.AddDays(Convert.ToInt32(TextBoxDays.Text)));
            comm.Parameters.Add("@now", DateTime.Now);
            bool b = false;
            Label lb;
            try
            {
                conn.Open();
                SqlDataReader dr = comm.ExecuteReader();
                dt = DateTime.MinValue;
                while (dr.Read())
                {
                    b = true;
                    DateTime dt1=Convert.ToDateTime(dr["od"]);
                    if (dt.ToShortDateString() != dt1.ToShortDateString())
                    {
                        lb = new Label();
                        lb.Font.Bold = true;
                        lb.Font.Size = FontUnit.Larger;
                        lb.Font.Underline = true;
                        lb.Text = dt1.ToShortDateString() + "<br>";
                        Panel1.Controls.Add(lb);
                        dt=dt1;
                    }
                    lb = new Label();
                    lb.Text = dt1.ToString() + " - " + Convert.ToDateTime(dr["do"]).ToShortTimeString() + ": #" + dr["id"].ToString() + " - " + dr["name"].ToString() + "; " + dr["adress"].ToString() + ", " + dr["mesto"].ToString() + "; tel.:" + dr["tel"].ToString() + "; emyl:" + dr["email"].ToString() + "<br>";
                    Panel1.Controls.Add(lb);
                }
                dr.Close();
            }
            finally { conn.Close(); }
            if (!b)
            {
                lb = new Label();
                lb.Text = "Nic k vypsání.";
                Panel1.Controls.Add(lb);
            }
        }
    }
}
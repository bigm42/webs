﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MICKEYsoftware</title>
<link rel="icon" href="~/mickey.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/mickey.ico" type="image/x-icon" />
</head>
<body style="color:#ff8080;font-family:Calibri
	">
    <form id="form1" runat="server">
    <div>
	
			<br/>
			<br/>
			<br/>
			<br/>
		<asp:Table runat="server" HorizontalAlign="Center"><asp:TableRow><asp:TableCell Width="266" HorizontalAlign="Center">
			Zde se vám nabízí pružná vývojová buňka MICKEYsoftware. Kolem webů umíme cokoliv, od návrhu, 
			přes profi fotky, po webmastering ve velkém. S naší tvorby můžeme uvést textovou hru
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://kino.mickey-software.cz/">KINO</asp:HyperLink> nebo
            <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://tajemstvi.mickey-software.cz/">tajemství</asp:HyperLink>, 
			dále užitečnou pomůcku
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://matika.mickey-software.cz/">Rezervovač doučování</asp:HyperLink>     
			(200,-Kč/rok pro lektora a pro případ dodělávek 200,-Kč/h) či hru pro pobavení a k tréningu boje obecně 
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="https://game4upper.com/">Tarum bitka na 4 (game4upper)</asp:HyperLink>.
			<br/>     
			<br/>
			<br/>
			Martin Míka<br/>
			Na Líše 242/28<br/>
			32600 Plzeň-Koterov<br/>
			mm@mickey-software.cz<br/>
			<br/>
			tel.: +420 773 66 91 45<br/>
			DIČ: cz7804292078<br/>
			IČ: 87069873<br/>
			<br/>
			<br/>
			Spolupracovali jsme na webech jako
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://www.real-price.info/">www.real-price.info</asp:HyperLink>,
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://www.white-magic.net/">www.white-magic.net</asp:HyperLink>,
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://www.podivna-zahrada.cz/">www.podivna-zahrada.cz/</asp:HyperLink>,
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://www.hrnek-kafe.cz/">www.hrnek-kafe.cz</asp:HyperLink>,
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://www.cup-of-coffee.org/">www.cup-of-coffee.org</asp:HyperLink>,
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://www.live-pass.info/">www.live-pass.info</asp:HyperLink>
	nebo
        <asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="http://www.blue-envelope.net/">www.blue-envelope.net</asp:HyperLink> .
			<br/>
			<br/>
			<br/>
			<asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="~/Default_eng.aspx?set=1">... English version...</asp:HyperLink><br/>     
			
		</asp:TableCell><asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
			
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<asp:Image runat="server" ImageUrl="~/mickey_logo.png"/><br/><br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<asp:HyperLink runat="server" ForeColor="#87aade" NavigateUrl="~/mickey_desktop.png">... a pro příklad firemní Obrázek Na Plochu...</asp:HyperLink><br/>     
			
		</asp:TableCell></asp:TableRow></asp:Table>
	
	</div>
	</form>
</body>
</html>

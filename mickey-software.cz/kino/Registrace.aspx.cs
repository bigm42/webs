﻿using System;
using System.Data.SqlClient;

public partial class kino_Registrace : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session[Constants_kino.RoomIdStr] = 0;

	if (Session[Constants_kino.PZ]!=null)
	{
		Back.NavigateUrl="http://podivna-zahrada.cz/?dale=pravdive";
		Back.Text="Zpátky na podivnou-zahradu.cz .";
	}
    }

    protected void LabelVyvoj_OnInit(object sender, EventArgs e)
    {
        LabelVyvoj.Visible = Constants_kino.LoginOnly;
    }

    protected void ButtonOK_OnClick(object sender, EventArgs e)
    {
        if (TextBoxUser.Text != "")
        {
            SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
            try
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("select id,password from kino_users where name like @name", conn);
                comm.Parameters.Add("@name", TextBoxUser.Text);
                SqlDataReader r = comm.ExecuteReader();
                bool login = false;
                bool create = false;
                int id = 0;
                if (r.Read())
                    if (r["password"].ToString() == TextBoxPass.Text)
                    {
                        login = true;
                        id=Convert.ToInt32(r["id"]);
                        Session[Constants_kino.LoginIdStr] = id;
                    }
                    else
                        LabelError.Text = "Špatně zadané heslo nebo uživatel s tímto jménem již existuje.";
                else
                    create = !Constants_kino.LoginOnly;
                r.Close();

                if (login)
                {
                    comm = new SqlCommand("update kino_users set last_visit=@lv where id=@id", conn);
                    comm.Parameters.Add("@lv", DateTime.Now);
                    comm.Parameters.Add("@id", id);
                    comm.ExecuteNonQuery();
                    if (CheckBoxChangePass.Checked)
                        Response.Redirect(Constants_kino.UrlStarts + "/ZmenaHesla.aspx");
                    else
                        Response.Redirect(Constants_kino.UrlStarts + "/Provoz.aspx");
                }
                if (create)
                {
                    comm = new SqlCommand("insert into kino_users(name,password,last_visit) values(@name,@pass,@lv) select @@identity", conn);
                    comm.Parameters.Add("@name", TextBoxUser.Text);
                    comm.Parameters.Add("@pass", TextBoxPass.Text);
                    comm.Parameters.Add("@lv", DateTime.Now);
                    id = Convert.ToInt32(comm.ExecuteScalar());
                    Session[Constants_kino.LoginIdStr] = id;
                    Response.Redirect(Constants_kino.UrlStarts + "/Provoz.aspx");
                }
            }
            finally { conn.Close(); }
        }
        else
            LabelError.Text = "Zadej uživatelské jméno.";
    }
}
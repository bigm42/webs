﻿using System;
using System.Data.SqlClient;

public partial class kino_ZmenaHesla : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants_kino.LoginIdStr] == null)
            Response.Redirect(Constants_kino.UrlStarts + "/Registrace.aspx", true);
    }

    protected void ButtonOK_OnClick(object sender, EventArgs e)
    {
        if (TextBoxPass1.Text == TextBoxPass2.Text)
        {
            SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
            try
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("update kino_users set password=@pass where id=@id", conn);
                comm.Parameters.Add("@pass", TextBoxPass1.Text);
                comm.Parameters.Add("@id", Session[Constants_kino.LoginIdStr]);
                comm.ExecuteNonQuery();
            }
            finally { conn.Close(); }
            Response.Redirect(Constants_kino.UrlStarts + "/Provoz.aspx");
        }
        else
            LabelError.Text = "Hesla se musejí shodovat.";
    }
}
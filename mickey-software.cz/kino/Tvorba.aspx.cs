﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Tvorba : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        SqlCommand comm = null;
        try
        {
            conn.Open();
            comm = new SqlCommand("select max(id) from kino_rooms", conn);
            LabelMax.Text = "ROOMs max id: " + comm.ExecuteScalar().ToString();
            comm = new SqlCommand("select max(id) from kino_computer_talk", conn);
            LabelMax.Text += "; C_TALK max id: " + comm.ExecuteScalar().ToString();
        }
        finally { conn.Close(); }
    }

    protected string BRRN(string str)
    {
        for (int i = 0; i < str.Length - 3; i++)
            if (str.Substring(i, 4) == "<br>")
                str = str.Substring(0, i) + "\r\n" + str.Substring(i + 4);
        return str;
    }

    protected string RNBR(string str)
    {
        for (int i = 0; i < str.Length - 1; i++)
            if (str.Substring(i, 2) == "\r\n")
                str = str.Substring(0, i) + "<br>" + str.Substring(i + 2);
        return str;
    }

    protected void ButtonPass_Click(object sender, EventArgs e)
    {
        if (ButtonPass.Text == "Zavři!")
        {
            ButtonPass.Text = "Přijmi!"; PanelPass.Visible = false; TextBoxUser.Text = "";
            if (RadioButtonListMain.SelectedValue!="")
            {
                (form1.FindControl("Panel" + RadioButtonListMain.SelectedValue) as Panel).Visible = false;
                Clear(form1.FindControl("Panel" + RadioButtonListMain.SelectedValue) as Panel);
            }
        }
        else
        {
            string[] lines = System.IO.File.ReadAllLines(Constants_kino.PassTXTname);
            int i = 0;
            while ((i < lines[0].Length) && (lines[0][i] != ':')) i++;
            if ((TextBoxUser.Text == lines[0].Substring(0, i)) && (TextBoxPass.Text == lines[0].Substring(i + 1)))
            { PanelPass.Visible = true; ButtonPass.Text = "Zavři!"; }
        }
   }

    protected void RadioButtonListMain_SelectedIndexChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < form1.Controls.Count; i++)
            if ((form1.Controls[i] is Panel)&&((form1.Controls[i] as Panel).ID!="PanelPass")) (form1.Controls[i] as Panel).Visible = false;
        (form1.FindControl("Panel" + (sender as RadioButtonList).SelectedValue) as Panel).Visible = true;
    }

    protected void CheckBoxPerson_CheckedChanged(object sender, EventArgs e)
    {
        PanelPerson.Visible = (sender as CheckBox).Checked;
    }

    protected void CheckBoxThing_CheckedChanged(object sender, EventArgs e)
    {
        PanelThing.Visible = (sender as CheckBox).Checked;
    }

    protected void ButtonRoom_Click(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        SqlCommand comm = null; int p = 0; int t = 0;
        try
        {
            conn.Open();
            if (CheckBoxPerson.Checked)
            {
                comm = new SqlCommand("insert into kino_persons(view_str,ctalk_id) values (@pstr,@pid) select @@identity", conn);
                comm.Parameters.AddWithValue("@pstr", TextBoxPersonVStr.Text);
                comm.Parameters.AddWithValue("@pid", Convert.ToInt32(TextBoxPersonCTID.Text));
                p = Convert.ToInt32(comm.ExecuteScalar().ToString());
            }
            if (CheckBoxThing.Checked)
            {
                comm = new SqlCommand("insert into kino_things(view_str,after_str,block_room,things_needed) values (@tvstr,@tastr,@tbr,@ttn) select @@identity", conn);
                comm.Parameters.AddWithValue("@tvstr", TextBoxThingVStr.Text);
                comm.Parameters.AddWithValue("@tastr", RNBR(TextBoxThingAStr.Text));
                if (CheckBoxThingBlockR.Checked)
                    comm.Parameters.AddWithValue("@tbr", Convert.ToInt32(TextBoxThingBlockR.Text));
                else
                    comm.Parameters.AddWithValue("@tbr", DBNull.Value);
                if (CheckBoxThingSNeeded.Checked)
                    comm.Parameters.AddWithValue("@ttn", Convert.ToInt32(TextBoxThingSNeeded.Text));
                else
                    comm.Parameters.AddWithValue("@ttn", DBNull.Value);
                t = Convert.ToInt32(comm.ExecuteScalar().ToString());
            }
            comm = new SqlCommand("insert into kino_rooms(view_str,person,thing,s,j,v,z) values (@rstr,@rp,@rt,@rs,@rj,@rv,@rz)", conn);
            comm.Parameters.AddWithValue("@rstr", RNBR(TextBoxRoom.Text));
            if (CheckBoxPerson.Checked)
                comm.Parameters.AddWithValue("@rp", p);
            else
                comm.Parameters.AddWithValue("@rp", DBNull.Value);
            if (CheckBoxThing.Checked)
                comm.Parameters.AddWithValue("@rt", t);
            else
                comm.Parameters.AddWithValue("@rt", DBNull.Value);
            if (CheckBoxS.Checked)
                comm.Parameters.AddWithValue("@rs", Convert.ToInt32(TextBoxS.Text));
            else
                comm.Parameters.AddWithValue("@rs", DBNull.Value);
            if (CheckBoxJ.Checked)
                comm.Parameters.AddWithValue("@rj", Convert.ToInt32(TextBoxJ.Text));
            else
                comm.Parameters.AddWithValue("@rj", DBNull.Value);
            if (CheckBoxV.Checked)
                comm.Parameters.AddWithValue("@rv", Convert.ToInt32(TextBoxV.Text));
            else
                comm.Parameters.AddWithValue("@rv", DBNull.Value);
            if (CheckBoxZ.Checked)
                comm.Parameters.AddWithValue("@rz", Convert.ToInt32(TextBoxZ.Text));
            else
                comm.Parameters.AddWithValue("@rz", DBNull.Value);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
        Clear(PanelRoom);
    }

    protected void Clear(Panel p)
    {
        for (int i = 0; i < p.Controls.Count; i++)
        {
            if (p.Controls[i] is CheckBox) (p.Controls[i] as CheckBox).Checked = false;
            if (p.Controls[i] is TextBox) (p.Controls[i] as TextBox).Text = "";
            if (p.Controls[i] is Panel) { (p.Controls[i] as Panel).Visible = false; Clear(p.Controls[i] as Panel); }
        }
    }

    protected void ButtonTalk_Click(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        SqlCommand comm = null; int[] p = new int[3];
        try
        {
            conn.Open();
            for (int i = 1; i <= 3; i++)
            {
                if ((PanelTalk.FindControl("CheckBoxTalkP" + i.ToString() + "Str") as CheckBox).Checked)
                {
                    comm = new SqlCommand("insert into kino_person_talk(str,c1) values (@pstr,@pcid) select @@identity", conn);
                    comm.Parameters.AddWithValue("@pstr", (PanelTalk.FindControl("TextBoxTalkP" + i.ToString() + "Str") as TextBox).Text);
                    comm.Parameters.AddWithValue("@pcid", Convert.ToInt32((PanelTalk.FindControl("TextBoxTalkCid" + i.ToString()) as TextBox).Text));
                    p[i - 1] = Convert.ToInt32(comm.ExecuteScalar().ToString());
                }
            }
            comm = new SqlCommand("insert into kino_computer_talk(str,p1,p2,p3,room) values (@cstr,@cp1,@cp2,@cp3,@cr)", conn);
            comm.Parameters.AddWithValue("@cstr", RNBR(TextBoxTalkCTStr.Text));
            for (int i = 1; i <= 3; i++)
            {
                if ((PanelTalk.FindControl("CheckBoxTalkP" + i.ToString() + "Str") as CheckBox).Checked)
                    comm.Parameters.AddWithValue("@cp" + i.ToString(), p[i - 1]);
                else
                    comm.Parameters.AddWithValue("@cp" + i.ToString(), DBNull.Value);
            }
            if (CheckBoxTalkRoom.Checked)
                comm.Parameters.AddWithValue("@cr", Convert.ToInt32(TextBoxTalkRoom.Text));
            else
                comm.Parameters.AddWithValue("@cr", DBNull.Value);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
        Clear(PanelTalk);

    }

    protected void CheckBoxEdit_CheckedChanged(object sender, EventArgs e)
    {
        DropDownListEdit.Visible = (sender as CheckBox).Checked;
        ButtonRoom.Enabled = !(sender as CheckBox).Checked;
        ButtonTalk.Enabled = !(sender as CheckBox).Checked;
    }

    protected void DropDownListEdit_PreRender(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        SqlCommand comm = null; int max = 0;
        try
        {
            conn.Open();
            if (RadioButtonListMain.SelectedValue == "Room")
            {
                comm = new SqlCommand("select max(id) from kino_rooms", conn);
                max = Convert.ToInt32(comm.ExecuteScalar().ToString());
            }
            if (RadioButtonListMain.SelectedValue == "Talk")
            {
                comm = new SqlCommand("select max(id) from kino_computer_talk", conn);
                max = Convert.ToInt32(comm.ExecuteScalar().ToString());
            }
        }
        finally { conn.Close(); }
        bool b = false;
        if (Session["maxid"] != null)
        {
            if (Session["maxid"].ToString()!=max.ToString())
            { (sender as DropDownList).Items.Clear(); b = true;Session["maxid"] = max; }
        }
        else
        { b = true; Session["maxid"] = max; }
        if (b)
        {
            for (int i = 1; i <= max; i++)
            {
                ListItem li = new ListItem("id" + i.ToString(), i.ToString());
                (sender as DropDownList).Items.Add(li);
            }
        }
    }

    protected void DropDownListEdit_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Constants_kino.ConnStr);
        SqlCommand comm = null; string commstr = "";
        try
        {
            conn.Open();
            if (RadioButtonListMain.SelectedValue == "Room")
            {
                commstr += "select kino_rooms.view_str as rstr, kino_rooms.s as rs, kino_rooms.j as rj, kino_rooms.v as rv, kino_rooms.z as rz, kino_persons.id as pid,";
                commstr += "kino_persons.view_str as pstr, kino_persons.ctalk_id as pc, kino_things.id as tid, kino_things.view_str as tstr, kino_things.after_str tastr,";
                commstr += "kino_things.block_room as tb, kino_things.things_needed as tt from kino_rooms left join kino_persons on kino_rooms.person=kino_persons.id left join ";
                commstr += "kino_things on kino_rooms.thing=kino_things.id where (kino_rooms.id=@id)";
                comm = new SqlCommand(commstr, conn);
                comm.Parameters.AddWithValue("@id", Convert.ToInt32(DropDownListEdit.SelectedValue));
                SqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                Clear(PanelRoom);
                TextBoxRoom.Text = BRRN(dr["rstr"].ToString());
                if (dr["rs"] != DBNull.Value) { CheckBoxS.Checked = true; TextBoxS.Text = dr["rs"].ToString(); }
                if (dr["rj"] != DBNull.Value) { CheckBoxJ.Checked = true; TextBoxJ.Text = dr["rj"].ToString(); }
                if (dr["rv"] != DBNull.Value) { CheckBoxV.Checked = true; TextBoxV.Text = dr["rv"].ToString(); }
                if (dr["rz"] != DBNull.Value) { CheckBoxZ.Checked = true; TextBoxZ.Text = dr["rz"].ToString(); }
                if (dr["pid"] != DBNull.Value)
                {
                    CheckBoxPerson.Checked = true; PanelPerson.Visible = true;
                    TextBoxPersonVStr.Text = "id" + dr["pid"].ToString() + "; " + dr["pstr"].ToString();
                    TextBoxPersonCTID.Text = dr["pc"].ToString();
                }
                if (dr["tid"] != DBNull.Value)
                {
                    CheckBoxThing.Checked = true; PanelThing.Visible = true;
                    TextBoxThingVStr.Text = "id" + dr["tid"].ToString() + "; " + dr["tstr"].ToString();
                    TextBoxThingAStr.Text = BRRN(dr["tastr"].ToString());
                    TextBoxThingBlockR.Text = dr["tb"].ToString();
                    TextBoxThingSNeeded.Text = dr["tt"].ToString();
                }
            }
            if (RadioButtonListMain.SelectedValue == "Talk")
            {
                commstr += "select kino_computer_talk.str as cstr, kino_person_talk.str AS pt1, kino_person_talk_2.str AS pt3, kino_person_talk_1.str AS pt2,";
                commstr += "               kino_person_talk.c1 AS cid1, kino_person_talk_2.c1 AS cid3, kino_person_talk_1.c1 AS cid2, kino_computer_talk.room AS room, ";
                commstr += "               kino_person_talk.id AS pid1, kino_person_talk_2.id AS pid3, kino_person_talk_1.id AS pid2 ";
                commstr += "from kino_computer_talk left join kino_person_talk ON kino_computer_talk.p1 = kino_person_talk.id LEFT JOIN";
                commstr += "              kino_person_talk AS kino_person_talk_1 ON kino_computer_talk.p2 = kino_person_talk_1.id LEFT JOIN";
                commstr += "              kino_person_talk AS kino_person_talk_2 ON kino_computer_talk.p3 = kino_person_talk_2.id";
                commstr += " WHERE     (kino_computer_talk.id= @id)";
                comm = new SqlCommand(commstr, conn);
                comm.Parameters.AddWithValue("@id", Convert.ToInt32(DropDownListEdit.SelectedValue));
                SqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                Clear(PanelTalk);
                TextBoxTalkCTStr.Text = BRRN(dr["cstr"].ToString());
                if (dr["pt1"] != DBNull.Value)
                { CheckBoxTalkP1Str.Checked = true; TextBoxTalkP1Str.Text = "id" + dr["pid1"].ToString() + "; " + dr["pt1"].ToString(); TextBoxTalkCid1.Text = dr["cid1"].ToString(); }
                if (dr["pt2"] != DBNull.Value)
                { CheckBoxTalkP2Str.Checked = true; TextBoxTalkP2Str.Text = "id" + dr["pid2"].ToString() + "; " + dr["pt2"].ToString(); TextBoxTalkCid2.Text = dr["cid2"].ToString(); }
                if (dr["pt3"] != DBNull.Value)
                { CheckBoxTalkP3Str.Checked = true; TextBoxTalkP3Str.Text = "id" + dr["pid3"].ToString() + "; " + dr["pt3"].ToString(); TextBoxTalkCid3.Text = dr["cid3"].ToString(); }
                if (dr["room"] != DBNull.Value) {CheckBoxTalkRoom.Checked = true; TextBoxTalkRoom.Text = dr["room"].ToString(); }
            }
        }
        finally { conn.Close(); }
    }
}
﻿using System;

public partial class kino_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
	if (Request["pz"]!=null)
	{
		Session[Constants_kino.PZ]=1;
	}
    }

    protected void Unnamed_Click(object sender, EventArgs e)
    {
        Response.Redirect(Constants_kino.UrlStarts + "/Registrace.aspx");
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Tvorba.aspx.cs" Inherits="Tvorba" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>KINO.edit</title>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="ButtonPass">
        <div>
            user><asp:TextBox ID="TextBoxUser" runat="server"></asp:TextBox><br />
            password><asp:TextBox ID="TextBoxPass" runat="server" TextMode="Password"></asp:TextBox>
            <asp:Button ID="ButtonPass" runat="server" Text="Přijmi!" OnClick="ButtonPass_Click"/><br /><br /><br />
            <asp:Panel ID="PanelPass" runat="server" Visible="False">
                <asp:RadioButtonList ID="RadioButtonListMain" runat="server" OnSelectedIndexChanged="RadioButtonListMain_SelectedIndexChanged" AutoPostBack="True">
                    <asp:ListItem Value="Room">-ROOM-</asp:ListItem>
                    <asp:ListItem Value="Talk">-TALKING-</asp:ListItem>
                </asp:RadioButtonList>
                <asp:CheckBox ID="CheckBoxEdit" runat="server" Text="Editovat?" OnCheckedChanged="CheckBoxEdit_CheckedChanged" AutoPostBack="true"/>
                <asp:DropDownList ID="DropDownListEdit" runat="server" OnPreRender="DropDownListEdit_PreRender" OnSelectedIndexChanged="DropDownListEdit_SelectedIndexChanged"
                    AutoPostBack="true" Visible="false"></asp:DropDownList>
                <asp:Label ID="LabelMax" runat="server"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="PanelRoom" runat="server" HorizontalAlign="Center" Visible="False">
                <asp:CheckBox ID="CheckBoxPerson" runat="server"  Text="PERSON in room (talking before)" AutoPostBack="true" OnCheckedChanged="CheckBoxPerson_CheckedChanged"/>
                <asp:CheckBox ID="CheckBoxThing" runat="server"  Text="THING in room" AutoPostBack="true" OnCheckedChanged="CheckBoxThing_CheckedChanged"/><br />
                Popis místnosti:
                <asp:TextBox ID="TextBoxRoom" runat="server" Height="200" Width="600" TextMode="MultiLine"></asp:TextBox><br />
                <asp:CheckBox ID="CheckBoxS" runat="server"  Text="S"/>
                <asp:TextBox ID="TextBoxS" runat="server" Width="50"></asp:TextBox>
                <asp:CheckBox ID="CheckBoxJ" runat="server"  Text="J"/>
                <asp:TextBox ID="TextBoxJ" runat="server" Width="50"></asp:TextBox>
                <asp:CheckBox ID="CheckBoxV" runat="server"  Text="V"/>
                <asp:TextBox ID="TextBoxV" runat="server" Width="50"></asp:TextBox>
                <asp:CheckBox ID="CheckBoxZ" runat="server"  Text="Z"/>
                <asp:TextBox ID="TextBoxZ" runat="server" Width="50"></asp:TextBox>
                <br />
                <asp:Panel ID="PanelPerson" runat="server" Visible="false">
                    Popis osoby: <asp:TextBox ID="TextBoxPersonVStr" runat="server" Width="600"></asp:TextBox>
                    ------- číslo computer talku: <asp:TextBox ID="TextBoxPersonCTID" runat="server" Width="50"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="PanelThing" runat="server" Visible="false">
                    <b>Věc:</b>
                    view_str <asp:TextBox ID="TextBoxThingVStr" runat="server" Width="600"></asp:TextBox><br />
                    after_str <asp:TextBox ID="TextBoxThingAStr" runat="server" Width="600" Height="200" TextMode="MultiLine"></asp:TextBox><br />
                    <asp:CheckBox ID="CheckBoxThingBlockR" runat="server" Text="block_room"/><asp:TextBox ID="TextBoxThingBlockR" runat="server" Width="50"></asp:TextBox>
                    <asp:CheckBox ID="CheckBoxThingSNeeded" runat="server" Text="things_needed"/><asp:TextBox ID="TextBoxThingSNeeded" runat="server" Width="50"></asp:TextBox>
                </asp:Panel>
                <asp:Button ID="ButtonRoom" runat="server" Text="edit new ROOM" OnClick="ButtonRoom_Click"/>
            </asp:Panel>
            <asp:Panel ID="PanelTalk" runat="server" HorizontalAlign="Center" Visible="False">
                CT str <asp:TextBox ID="TextBoxTalkCTStr" runat="server" Width="600" Height="200" TextMode="MultiLine"></asp:TextBox><br />
                <asp:CheckBox ID="CheckBoxTalkP1Str" runat="server" Text="P1 str"/><asp:TextBox ID="TextBoxTalkP1Str" runat="server" Width="600"></asp:TextBox>
                CT id1 <asp:TextBox ID="TextBoxTalkCid1" runat="server" Width="50"></asp:TextBox><br />
                <asp:CheckBox ID="CheckBoxTalkP2Str" runat="server" Text="P2 str"/><asp:TextBox ID="TextBoxTalkP2Str" runat="server" Width="600"></asp:TextBox>
                CT id2 <asp:TextBox ID="TextBoxTalkCid2" runat="server" Width="50"></asp:TextBox><br />
                <asp:CheckBox ID="CheckBoxTalkP3Str" runat="server" Text="P3 str"/><asp:TextBox ID="TextBoxTalkP3Str" runat="server" Width="600"></asp:TextBox>
                CT id3 <asp:TextBox ID="TextBoxTalkCid3" runat="server" Width="50"></asp:TextBox><br />
                <asp:CheckBox ID="CheckBoxTalkRoom" runat="server" Text="room"/><asp:TextBox ID="TextBoxTalkRoom" runat="server" Width="50"></asp:TextBox><br />
                <asp:Button ID="ButtonTalk" runat="server" Text="edit new CONVERSATION" OnClick="ButtonTalk_Click"/>
            </asp:Panel>
        </div>
    </form>
</body>
</html>

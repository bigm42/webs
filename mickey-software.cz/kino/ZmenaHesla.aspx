﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZmenaHesla.aspx.cs" Inherits="kino_ZmenaHesla" %>

<head runat="server">
    <title>Změna hesla / kino / Změna hesla</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <br /> <br />   <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonOK" HorizontalAlign="Center">
        
            <asp:Label ID="Label2" runat="server" Text="Změna hesla" Font-Underline="True" Font-Size="X-Large" Font-Bold="True"></asp:Label>
            <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">Nové heslo:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="TextBoxPass1" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">Nové heslo znovu:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="TextBoxPass2" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:Button ID="ButtonOK" runat="server" Text="Potvrdit" OnClick="ButtonOK_OnClick"/>
            <br />
            <asp:Label ID="LabelError" runat="server" Text="" ForeColor="#FF3300"></asp:Label>
        </asp:Panel>

    </div>
    </form>
</body>
</html>

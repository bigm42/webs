﻿using System;

public partial class _Default : System.Web.UI.Page
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (!(Request.UserLanguages[0].Contains("cs") || Request.UserLanguages[0].Contains("sk") || Request.UserLanguages[0].Contains("pl"))&&(!Request.Url.OriginalString.Contains("eng"))&&(Request["set"]==null))
			Response.Redirect("~/Default_eng.aspx");
		else
			if ((Request.UserLanguages[0].Contains("cs") || Request.UserLanguages[0].Contains("sk") || Request.UserLanguages[0].Contains("pl"))&&(!Request.Url.OriginalString.Contains("t.aspx"))&&(Request["set"]==null))
				Response.Redirect("~/Default.aspx");
	}

}
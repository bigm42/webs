using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tarum : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["i"] == null)
            Session["i"] = 0;

	if ((Request["pz"]!=null)&&(!HyperLinkBack.NavigateUrl.Contains("pz=1"))) HyperLinkBack.NavigateUrl+="?pz=1";
    }

    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        string[] url = { "blazen", "imag", "veleknezka", "cisarovna", "cisar", "veleknez", "zamilovani", "vuz", "spravedlnost", "poustevnik", "kolostesti", "sila", "viselec", "smrt", "umeni", "dabel", "vez", "hvezda", "mesic", "slunce", "aeon", "vesmir" };
        string[] urleng = { "the-fool", "the-magician", "the-high-priestess", "the-empress", "the-emperor", "the-hierophant", "the-lovers", "the-chariot", "justice", "the-hermit", "wheel-of-fortune", "strength", "the-hanged-man", "death", "temperance", "the-devil", "the-tower", "the-star", "the-moon", "the-sun", "judgement", "the-world" };
        string[] strings = { "Bl�zen (The Fool).", "M�g (The Magician).", "Velekne�ka (The High Priestess).", "C�sa�ovna (The Empress).", "C�sa� (The Emperor).", "Velekn�z (The Hierophant).", "Zamilovan� (The Lovers).", "V�z (The Chariot).", "Spravedlnost (Justice).", "Poustevn�k (The Hermit).", "Kolo �t�st� (Wheel of Fortune).", "S�la/Cht�� (Strength).", "Viselec (The Hanged Man).", "Smrt/Znovuzrozen� (Death).", "Um�n�/Um�rn�nost (Temperance).", "��bel/Pan (The Devil).", "V� (The Tower).", "Hv�zda (The Star).", "M�s�c (The Moon).", "Slunce (The Sun).", "Aeon/�sudek (Judgement).", "Vesm�r (The World)." };

        Random rnd = new Random(DateTime.Now.Millisecond);
        if ((sender as ImageButton).ImageUrl.Equals("~/21karty/zada.png"))
        {
            bool b = true; int i = 0;
            while (b)
            {
                i = rnd.Next(22);
                bool b1 = true;
                for (int i1 = 0; i1 < Convert.ToInt32(Session["i"]); i1++)
                    if (Convert.ToInt32(Session[i1.ToString()]) == i)
                    { b1 = false; Label.Text += "*"; }
                b = !b1;
            }
            (sender as ImageButton).ImageUrl = "~/21karty/" + i.ToString() + ".png";
            (sender as ImageButton).ToolTip = i.ToString() + " " + strings[i];
            Session[Convert.ToInt32(Session["i"]).ToString()] = i;
            Session["i"] = Convert.ToInt32(Session["i"]) + 1;
        }
        else
            if (Session["jazyk"] == null)
                if (Request.UserLanguages[0].Contains("cs") || Request.UserLanguages[0].Contains("sk") || Request.UserLanguages[0].Contains("pl"))
                    Response.Redirect("http://www.osudovesymboly.estranky.cz/clanky/velka-arkana.html#" + url[Convert.ToInt32((sender as ImageButton).ToolTip.Substring(0, 2))]);
                else
                    Response.Redirect("http://www.tarotlore.com/tarot-cards/" + urleng[Convert.ToInt32((sender as ImageButton).ToolTip.Substring(0, 2))]+"/");
            else
                if (Session["jazyk"].Equals("cz"))
                    Response.Redirect("http://www.osudovesymboly.estranky.cz/clanky/velka-arkana.html#" + url[Convert.ToInt32((sender as ImageButton).ToolTip.Substring(0, 2))]);
                else
                    Response.Redirect("http://www.tarotlore.com/tarot-cards/" + urleng[Convert.ToInt32((sender as ImageButton).ToolTip.Substring(0, 2))]+"/");
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        ImageButton1.ImageUrl = "~/21karty/zada.png";
        ImageButton1.ToolTip = "Jak to je (How it is).";
        ImageButton2.ImageUrl = "~/21karty/zada.png";
        ImageButton2.ToolTip = "Jak to bylo (How it was).";
        ImageButton3.ImageUrl = "~/21karty/zada.png";
        ImageButton3.ToolTip = "Jak to bude (How it will be).";
        ImageButton4.ImageUrl = "~/21karty/zada.png";
        ImageButton4.ToolTip = "Jak to doopravdy je (How it really is).";
        ImageButton5.ImageUrl = "~/21karty/zada.png";
        ImageButton5.ToolTip = "Sny (Dreams).";
        ImageButton6.ImageUrl = "~/21karty/zada.png";
        ImageButton6.ToolTip = "Sny (Dreams).";
	Label.Text="";
        Session["i"] = 0;
    }

    protected void HL_Click(object sender, EventArgs e)
    {
        if (HL.Text.Contains("links"))
            Session["jazyk"] = "us";
        else
            Session["jazyk"] = "cz";
    }

    protected void HL_PreRender(object sender, EventArgs e)
    {
        if (Session["jazyk"] == null)
            if (Request.UserLanguages[0].Contains("cs") || Request.UserLanguages[0].Contains("sk") || Request.UserLanguages[0].Contains("pl"))
            { HL.Text = "English links."; }
            else
            { HL.Text = "�esk� odkazy."; }
        else
            if (Session["jazyk"].Equals("cz"))
            { HL.Text = "English links."; }
            else
            { HL.Text = "�esk� odkazy."; }
    }
}
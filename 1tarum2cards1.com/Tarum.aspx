<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Tarum.aspx.cs" Inherits="Tarum" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tarum</title>
<link rel="icon" href="~/tarum.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/tarum.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/21karty/zada.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
</head>
<body style="background-color:#e9e9e9
	">
    <form id="form1" runat="server">
    <div>
        <asp:Panel runat="server" HorizontalAlign="Center">
        <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center"  >
                    <br /><br /><br />
                    <h2>Tarum v�m! (Tarum to you!)</h2><br />
                    <br /><br />
                </asp:TableCell>
                <asp:TableCell><asp:Label ID="Label" runat="server" Font-Underline="True"></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell BorderStyle="Solid" BorderWidth="4" BorderColor="#CC9900">
<center>                    <asp:ImageButton ID="ImageButton1" runat="server"  OnClick="ImageButton_Click" ImageUrl="~/21karty/zada.png" ToolTip="Jak to je. (How it is.)"/>
                    <asp:ImageButton ID="ImageButton2" runat="server"  OnClick="ImageButton_Click" ImageUrl="~/21karty/zada.png"  ToolTip="Jak to bylo. (How it was.)"/>
                    <asp:ImageButton ID="ImageButton3" runat="server"  OnClick="ImageButton_Click" ImageUrl="~/21karty/zada.png"  ToolTip="Jak to bude. (How it will be.)"/>
                    <asp:ImageButton ID="ImageButton4" runat="server"  OnClick="ImageButton_Click" ImageUrl="~/21karty/zada.png"  ToolTip="Jak to doopravdy je. (How it really is.)"/></center>
                </asp:TableCell>
                 <asp:TableCell BorderStyle="Solid" BorderWidth="4" BorderColor="#FF0066"><center>
                    <asp:ImageButton ID="ImageButton5" runat="server"  OnClick="ImageButton_Click" ImageUrl="~/21karty/zada.png"  ToolTip="Sny. (Dreams.)"/>
                    <asp:ImageButton ID="ImageButton6" runat="server"  OnClick="ImageButton_Click" ImageUrl="~/21karty/zada.png"  ToolTip="Sny. (Dreams.)"/></center>
                 </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
            <br /><br /><br />
            <asp:Button ID="Button" runat="server" Text="Znovu... (Again...)"  OnClick="Button_Click"/>
            <br /><br /><br />
        --- <asp:HyperLink ID="HyperLink" runat="server" ForeColor="Black" NavigateUrl="~/Tarum Cards.png" >V�echny karty. (All cards.)</asp:HyperLink> ---
            <asp:Button ID="HL" runat="server" Text=""  OnClick="HL_Click" OnPreRender="HL_PreRender"/>      
        --- <asp:HyperLink ID="HyperLinkBack" runat="server" ForeColor="Black" NavigateUrl="~/Default.aspx" >Zp�tky. (Back.)</asp:HyperLink> --- 
	<br />
        </asp:Panel>   
     </div>
    </form>
</body>
</html>

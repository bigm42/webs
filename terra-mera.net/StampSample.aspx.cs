﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class StampSample : System.Web.UI.Page
{
    protected void ISB_OC(object sender, EventArgs e)
    {
       Response.Redirect("http://www.terra-mera.net/");
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        ImageStamp.ImageUrl = "~/Stamp.ashx";
        DirectoryInfo di = new DirectoryInfo(Constants.StampsLoc);
        FileInfo[] fi = di.GetFiles();
        foreach (FileInfo fimem in fi)
            if (Request.UserLanguages[0].Contains(RealName(fimem.Name)))
                ImageStamp.ImageUrl = "~/Stamp.ashx?" + Constants.CodeStr + "=" + RealName(fimem.Name);
        for (int i=1;i<DropDownListStampCountry.Items.Count;i++)
	    if (Request.UserLanguages[0].Contains(DropDownListStampCountry.Items[i].Text))
			DropDownListStampCountry.SelectedIndex=i;
    }

    protected string RealName(string file)
    {
        for (int i = 0; i < file.Length; i++)
            if (file[i] == '.')
                return (file.Substring(0, i));
        return (file);
    }

    protected void DropDownListStampCountry_OnInit(object sender, EventArgs e)
    {
        if (DropDownListStampCountry.Items.Count <= 2)
        {
            DirectoryInfo di = new DirectoryInfo(Constants.StampsLoc);
            FileInfo[] fi = di.GetFiles();
            foreach (FileInfo fimem in fi)
            {
                ListItem li = new ListItem();
                li.Text = RealName(fimem.Name);
                DropDownListStampCountry.Items.Add(li);
            }
        }
    }

    protected void DropDownListStampCountry_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListStampCountry.SelectedIndex == 0)
        {
            ImageStamp.ImageUrl = "~/Stamp.ashx";
            DirectoryInfo di = new DirectoryInfo(Constants.StampsLoc);
            FileInfo[] fi = di.GetFiles();
            foreach (FileInfo fimem in fi)
                if (Request.UserLanguages[0].Contains(RealName(fimem.Name)))
                    ImageStamp.ImageUrl = "~/Stamp.ashx?" + Constants.CodeStr + "=" + RealName(fimem.Name);
	    for (int i=1;i<DropDownListStampCountry.Items.Count;i++)
		if (Request.UserLanguages[0].Contains(DropDownListStampCountry.Items[i].Text))
			DropDownListStampCountry.SelectedIndex=i;
        }
        else
            ImageStamp.ImageUrl = "~/Stamp.ashx?" + Constants.CodeStr + "=" + DropDownListStampCountry.SelectedItem.Text;
    }
}
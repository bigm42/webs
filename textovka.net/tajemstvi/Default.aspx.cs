﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Start : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
	Response.Redirect("http://tajemstvi.the-or.cz/");
    }

    protected void ButtonAno_OnClick(object sender, EventArgs e)
    {
        Response.Redirect(Constans.UrlStarts + "/Registrace.aspx");
    }

    protected void ButtonNe_OnClick(object sender, EventArgs e)
    {
        Label2.Visible = true;
    }
}
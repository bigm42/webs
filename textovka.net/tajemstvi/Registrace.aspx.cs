﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class tajemstvi_Registrace : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session[Constans.RoomIdStr] = 0;
    }

    protected void LabelVyvoj_OnInit(object sender, EventArgs e)
    {
        LabelVyvoj.Visible = Constans.LoginOnly;
    }

    protected void ButtonOK_OnClick(object sender, EventArgs e)
    {
        if (TextBoxUser.Text != "")
        {
            SqlConnection conn = new SqlConnection(Constans.ConnStr);
            try
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("select id,password from tajemstvi_users where name like @name", conn);
                comm.Parameters.Add("@name", TextBoxUser.Text);
                SqlDataReader r = comm.ExecuteReader();
                bool login = false;
                bool create = false;
                int id = 0;
                if (r.Read())
                    if (r["password"].ToString() == TextBoxPass.Text)
                    {
                        login = true;
                        id=Convert.ToInt32(r["id"]);
                        Session[Constans.LoginIdStr] = id;
                    }
                    else
                        LabelError.Text = "Špatně zadané heslo nebo uživatel s tímto jménem již existuje.";
                else
                    create = !Constans.LoginOnly;
                r.Close();

                if (login)
                {
                    comm = new SqlCommand("update tajemstvi_users set last_visit=@lv where id=@id", conn);
                    comm.Parameters.Add("@lv", DateTime.Now);
                    comm.Parameters.Add("@id", id);
                    comm.ExecuteNonQuery();
                    if (CheckBoxChangePass.Checked)
                        Response.Redirect(Constans.UrlStarts + "/ZmenaHesla.aspx");
                    else
                        Response.Redirect(Constans.UrlStarts + "/Provoz.aspx");
                }
                if (create)
                {
                    comm = new SqlCommand("insert into tajemstvi_users(name,password,last_visit) values(@name,@pass,@lv) select @@identity", conn);
                    comm.Parameters.Add("@name", TextBoxUser.Text);
                    comm.Parameters.Add("@pass", TextBoxPass.Text);
                    comm.Parameters.Add("@lv", DateTime.Now);
                    id = Convert.ToInt32(comm.ExecuteScalar());
                    Session[Constans.LoginIdStr] = id;
                    Response.Redirect(Constans.UrlStarts + "/Provoz.aspx");
                }
            }
            finally { conn.Close(); }
        }
        else
            LabelError.Text = "Zadej uživatelské jméno.";
    }
}
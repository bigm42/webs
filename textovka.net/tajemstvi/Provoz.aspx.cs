﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class ProvozTajemstvi : System.Web.UI.Page
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Session[Constans.LoginIdStr] == null)
            Response.Redirect(Constans.UrlStarts + "/Registrace.aspx", true);
        else
        {
            SqlConnection conn = new SqlConnection(Constans.ConnStr);
            try
            {
                conn.Open();
                string commStr = "SELECT     tajemstvi_walk_register.things_done AS things_done, tajemstvi_rooms.id AS room_id, tajemstvi_rooms.view_str AS room_str, tajemstvi_persons.view_str AS person_str, tajemstvi_things.view_str AS thing_str, tajemstvi_rooms.s, tajemstvi_rooms.j, tajemstvi_rooms.v, tajemstvi_rooms.z";
                commStr += " FROM         tajemstvi_walk_register LEFT JOIN";
                commStr += "           tajemstvi_rooms ON tajemstvi_walk_register.room = tajemstvi_rooms.id LEFT JOIN";
                commStr += " tajemstvi_persons ON tajemstvi_rooms.person = tajemstvi_persons.id LEFT JOIN";
                commStr += " tajemstvi_things ON tajemstvi_rooms.thing = tajemstvi_things.id";
                commStr += " WHERE     (tajemstvi_walk_register.user_id = @id)";
                SqlCommand comm = null;
                SqlDataReader r = null;
loop:           comm = new SqlCommand(commStr, conn);
                comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
                r=comm.ExecuteReader();
                bool createUser=false;
                bool level = false;
                if (r.Read())
                {
                    string str = r["room_str"].ToString();
                    switch (str.Substring(0, 2))
                    {
                        case "L&":
                            level = true;
                            Session[Constans.LevelStr] = 1;
                            str = str.Substring(2, str.Length - 2);
                            break;
                        case "B&":
                            Session[Constans.PointUpStr] = 1;
                            str = str.Substring(2, str.Length - 2);
                            break;
                    }
                    if (Convert.ToInt32(Session[Constans.RoomIdStr]) != Convert.ToInt32(r["room_id"]))
                        TextBoxLog.Text=TextBoxLog.Text+ str + "\r\n \r\n";
                    Session[Constans.RoomIdStr] = r["room_id"];

                    LabelThingsDone.Text = "Důležitých věcí uděláno: " + r["things_done"].ToString();
                    if (r["person_str"] == DBNull.Value)
                    {
                        ButtonTalk.Enabled = false;
                        ButtonTalk.Text = "Mluvit s";
                    }
                    else
                    {
                        ButtonTalk.Enabled = true;
                        ButtonTalk.Text = "Mluvit s "+r["person_str"].ToString();
                    }
                    if (r["thing_str"] == DBNull.Value)
                    {
                        ButtonUse.Enabled = false;
                        ButtonUse.Text = "Použít";
                    }
                    else
                    {
                        ButtonUse.Enabled = true;
                        ButtonUse.Text = "Použít " + r["thing_str"].ToString();
                    }
                    if (r["s"] == DBNull.Value)
                    {
                        ButtonS.Enabled = false;
                        Session[Constans.SStr] = 0;
                    }
                    else
                    {
                        ButtonS.Enabled = true;
                        Session[Constans.SStr] = r["s"];
                    }
                    if (r["j"] == DBNull.Value)
                    {
                        ButtonJ.Enabled = false;
                        Session[Constans.JStr] = 0;
                    }
                    else
                    {
                        ButtonJ.Enabled = true;
                        Session[Constans.JStr] = r["j"];
                    }
                    if (r["v"] == DBNull.Value)
                    {
                        ButtonV.Enabled = false;
                        Session[Constans.VStr] = 0;
                    }
                    else
                    {
                        ButtonV.Enabled = true;
                        Session[Constans.VStr] = r["v"];
                    }
                    if (r["z"] == DBNull.Value)
                    {
                        ButtonZ.Enabled = false;
                        Session[Constans.ZStr] = 0;
                    }
                    else
                    {
                        ButtonZ.Enabled = true;
                        Session[Constans.ZStr] = r["z"];
                    }
                 }
                else
                    createUser = true;
                r.Close();
                
                if (createUser)
                {
                    comm = new SqlCommand("insert into tajemstvi_walk_register(user_id,room,level) values(@id,@sroom,@sroom)", conn);
                    comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
                    comm.Parameters.Add("@sroom", Constans.StartRoom);
                    comm.ExecuteNonQuery();
                    goto loop;
                }
                if (level)
                {
                    comm = new SqlCommand("update tajemstvi_walk_register set level=room, things_done=0 where user_id=@id", conn);
                    comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
                    comm.ExecuteNonQuery();
                }
            }
            finally { conn.Close(); }
        }
    }

    protected void ButtonGameStart_OnClick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session[Constans.RoomIdStr]) == Constans.StartRoom)
            Session[Constans.RoomIdStr] = 0;
        TextBoxLog.Text = "";
        RadioButtonListTalk.Items.Clear();
        SqlConnection conn=new SqlConnection(Constans.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand("update tajemstvi_walk_register set room=@sroom,level=@sroom,score=0 where user_id=@id", conn);
            comm.Parameters.Add("@sroom", Constans.StartRoom);
            comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
    }

    protected void ButtonLevelStart_OnClick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session[Constans.LevelStr]) == 1)
            Session[Constans.RoomIdStr] = 0;
        TextBoxLog.Text = "";
        RadioButtonListTalk.Items.Clear();
        SqlConnection conn = new SqlConnection(Constans.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand("update tajemstvi_walk_register set room=level where user_id=@id", conn);
            comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
    }

    protected void ZmenRoom(int room)
    {
        TextBoxLog.Text = "";
        if (Convert.ToInt32(Session[Constans.LevelStr]) == 1)
            Session[Constans.LevelStr] = 0;
        string str1 = "";
        if (Convert.ToInt32(Session[Constans.PointUpStr]) == 1)
        {
            str1 = "score=score+1,";
            Session[Constans.PointUpStr] = 0;
        }
        SqlConnection conn = new SqlConnection(Constans.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand("update tajemstvi_walk_register set " + str1 + "room=@room where user_id=@id", conn);
            comm.Parameters.Add("@room", room);
            comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
            comm.ExecuteNonQuery();
        }
        finally { conn.Close(); }
    }

    protected void ButtonUse_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        string commStr = "SELECT     tajemstvi_things.after_str, tajemstvi_things.block_room, tajemstvi_things.view_str, tajemstvi_things.things_needed, tajemstvi_walk_register.things_done";
        commStr += " FROM         tajemstvi_walk_register LEFT JOIN";
        commStr += "                       tajemstvi_rooms ON tajemstvi_walk_register.room = tajemstvi_rooms.id LEFT JOIN";
        commStr += "            tajemstvi_things ON tajemstvi_rooms.thing = tajemstvi_things.id";
        commStr += " WHERE     (tajemstvi_walk_register.user_id = @id)";
        SqlConnection conn = new SqlConnection(Constans.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand(commStr, conn);
            comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
            SqlDataReader r = comm.ExecuteReader();
            r.Read();
            bool ThingsDoneInc = false;
            if (r["things_needed"] == DBNull.Value)
            {
                if (r["block_room"] != DBNull.Value)
                    ZmenRoom(Convert.ToInt32(r["block_room"]));
                else
                    ThingsDoneInc = true;
                TextBoxLog.Text = TextBoxLog.Text + "Použils " + r["view_str"].ToString() + " s výsledkem: " + r["after_str"].ToString() + "\r\n \r\n";
            }
            else
                if (Convert.ToInt32(r["things_needed"]) == Convert.ToInt32(r["things_done"]))
                {
                    if (r["block_room"] != DBNull.Value)
                        ZmenRoom(Convert.ToInt32(r["block_room"]));
                    TextBoxLog.Text = TextBoxLog.Text + "Použils " + r["view_str"].ToString() + " s výsledkem: " + r["after_str"].ToString() + "\r\n \r\n";
                }
                else
                    TextBoxLog.Text += "Málo důležitých věcí uděláno, ještě musíš vykonat " + (Convert.ToInt32(r["things_needed"]) - Convert.ToInt32(r["things_done"])).ToString() + " věcí.\r\n\r\n";
            r.Close();
            if (ThingsDoneInc)
            {
                comm = new SqlCommand("update tajemstvi_walk_register set things_done=things_done+1 where id=@id", conn);
                comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
                comm.ExecuteNonQuery();
            }
        }
        finally { conn.Close(); }
    }

    protected void ButtonTalk_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        string commStr = "SELECT     tajemstvi_computer_talk.str AS c_talk, tajemstvi_person_talk.str AS p_talk1, tajemstvi_person_talk_2.str AS p_talk3, tajemstvi_person_talk_1.str AS p_talk2,";
        commStr += "               tajemstvi_person_talk.c1 AS cid1, tajemstvi_person_talk_2.c1 AS cid3, tajemstvi_person_talk_1.c1 AS cid2, tajemstvi_computer_talk.room AS room, tajemstvi_persons.view_str AS pstr";
        commStr += " FROM         tajemstvi_walk_register LEFT JOIN";
        commStr += "               tajemstvi_rooms ON tajemstvi_walk_register.room = tajemstvi_rooms.id LEFT JOIN";
        commStr += "               tajemstvi_persons ON tajemstvi_rooms.person = tajemstvi_persons.id LEFT JOIN";
        commStr += "               tajemstvi_computer_talk ON tajemstvi_persons.ctalk_id = tajemstvi_computer_talk.id LEFT JOIN";
        commStr += "              tajemstvi_person_talk ON tajemstvi_computer_talk.p1 = tajemstvi_person_talk.id LEFT JOIN";
        commStr += "              tajemstvi_person_talk AS tajemstvi_person_talk_1 ON tajemstvi_computer_talk.p2 = tajemstvi_person_talk_1.id LEFT JOIN";
        commStr += "              tajemstvi_person_talk AS tajemstvi_person_talk_2 ON tajemstvi_computer_talk.p3 = tajemstvi_person_talk_2.id";
        commStr += " WHERE     (tajemstvi_walk_register.user_id = @id)";
        SqlConnection conn = new SqlConnection(Constans.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand(commStr, conn);
            comm.Parameters.Add("@id", Session[Constans.LoginIdStr]);
            SqlDataReader r = comm.ExecuteReader();
            r.Read();
            TextBoxLog.Text = TextBoxLog.Text + "Mluvíš s " + r["pstr"].ToString() + ":\r\nON: " + r["c_talk"].ToString() + "\r\n \r\n";
            Session[Constans.MluvciStr] = r["pstr"].ToString();
            if (r["room"] == DBNull.Value)
            {
                if (r["p_talk1"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk1"].ToString(), r["cid1"].ToString()));
                if (r["p_talk2"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk2"].ToString(), r["cid2"].ToString()));
                if (r["p_talk3"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk3"].ToString(), r["cid3"].ToString()));
            }
            else
            {
                ZmenRoom(Convert.ToInt32(r["room"]));
                TextBoxLog.Text = "Mluvil si s " + r["pstr"].ToString() + ":\r\nON: " + r["c_talk"].ToString() + "\r\n \r\n";
            }
            r.Close();
        }
        finally { conn.Close(); }
    }

    protected void RadioButtonListTalk_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        string commStr = "SELECT     tajemstvi_computer_talk.str AS c_talk, tajemstvi_computer_talk.room, tajemstvi_person_talk.str AS p_talk1, tajemstvi_person_talk_1.str AS p_talk2,";
        commStr += " tajemstvi_person_talk_2.str AS p_talk3, tajemstvi_person_talk.c1 AS cid1, tajemstvi_person_talk_1.c1 AS cid2, tajemstvi_person_talk_2.c1 AS cid3";
        commStr += " FROM         tajemstvi_computer_talk LEFT JOIN";
        commStr += "                       tajemstvi_person_talk ON tajemstvi_computer_talk.p1 = tajemstvi_person_talk.id LEFT JOIN";
        commStr += "                       tajemstvi_person_talk AS tajemstvi_person_talk_1 ON tajemstvi_computer_talk.p2 = tajemstvi_person_talk_1.id LEFT JOIN";
        commStr += "                       tajemstvi_person_talk AS tajemstvi_person_talk_2 ON tajemstvi_computer_talk.p3 = tajemstvi_person_talk_2.id";
        commStr += " WHERE     (tajemstvi_computer_talk.id = @id)";
        SqlConnection conn = new SqlConnection(Constans.ConnStr);
        try
        {
            conn.Open();
            SqlCommand comm = new SqlCommand(commStr, conn);
            comm.Parameters.Add("@id", Convert.ToInt32(RadioButtonListTalk.SelectedValue));
            SqlDataReader r = comm.ExecuteReader();
            r.Read();
            TextBoxLog.Text = TextBoxLog.Text.Substring(0, TextBoxLog.Text.Length - 4);
            string str = RadioButtonListTalk.SelectedItem.Text;
            RadioButtonListTalk.Items.Clear();
            if (r["room"] == DBNull.Value)
            {
                if (r["p_talk1"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk1"].ToString(), r["cid1"].ToString()));
                if (r["p_talk2"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk2"].ToString(), r["cid2"].ToString()));
                if (r["p_talk3"] != DBNull.Value)
                    RadioButtonListTalk.Items.Add(new ListItem(r["p_talk3"].ToString(), r["cid3"].ToString()));
            }
            else
            {
                ZmenRoom(Convert.ToInt32(r["room"]));
                TextBoxLog.Text = "Mluvil si s " + Session[Constans.MluvciStr].ToString() + ":\r\n";
            }
            TextBoxLog.Text = TextBoxLog.Text + "TY: " + str + "\r\nON: " + r["c_talk"].ToString() + "\r\n \r\n";
            r.Close();
        }
        finally { conn.Close(); }
    }

    protected void ButtonS_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constans.SStr]));
    }

    protected void ButtonV_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constans.VStr]));
    }

    protected void ButtonZ_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constans.ZStr]));
    }

    protected void ButtonJ_OnClick(object sender, EventArgs e)
    {
        RadioButtonListTalk.Items.Clear();
        ZmenRoom(Convert.ToInt32(Session[Constans.JStr]));
    }

    protected void ButtonLogin_OnClick(object sender, EventArgs e)
    {
        Response.Redirect(Constans.UrlStarts + "/Default.aspx");
    }
}
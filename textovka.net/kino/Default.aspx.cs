﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class kino_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
	Response.Redirect("http://kino.the-or.cz/");
    }

    protected void Unnamed_Click(object sender, EventArgs e)
    {
        Response.Redirect(Constants_kino.UrlStarts + "/Registrace.aspx");
    }
}
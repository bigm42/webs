﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SlideShow.aspx.cs" Inherits="SlideShow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>-Slide S H O W-</title>
<link rel="icon" href="~/hrnek_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/hrnek_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/hrnek_icon.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
<meta http-equiv="refresh" content="4;url=SlideShow.aspx"/>
</head>
<body style="background-color:#e9e9e9;color:#40339d;font-family:Calibri
	">
    <form id="form1" runat="server">
    <div>
        <asp:Panel runat="server" HorizontalAlign="Center">
<br />
<br />
<h1>- Slide S H O W -</h1>
<br />
<asp:Image runat="server" ImageUrl="~/hrnek_icon.png"/>
<br />
Výška obrázku. (High of the picture.) (px):
         <asp:TextBox ID="TextBoxHigh" runat="server" AutoPostBack="True" OnPreRender="TextBoxHigh_PreRender" OnTextChanged="TextBoxHigh_TextChanged"></asp:TextBox>
<br />
<br />
<asp:Image runat="server" ImageUrl="~/Kafe.ashx" OnPreRender="Kafe_PreRender"/>
<br />
        <br />
        <br />
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Default.aspx" ID="HLBAck">Zpátky. (Back.)</asp:HyperLink>
        <br />

    </asp:Panel>
    </div>
    </form>
</body>
</html>

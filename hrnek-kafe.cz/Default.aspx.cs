﻿using System;
using System.Web.UI.WebControls;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if ((Request["lang"] != null) && !Request["lang"].ToString().Equals("cz"))
            if (!(Request.UserLanguages[0].Contains("cs") || Request.UserLanguages[0].Contains("sk") || Request.UserLanguages[0].Contains("pl")) || ((Request["lang"] != null) && Request["lang"].ToString().Equals("eng")))
            { toto.Visible = false; toto1.Visible = true;
                gadit.Visible = false; gadit1.Visible = true;
                manu.Visible = false; manu1.Visible = true;
                rekl.Visible = false; rekl1.Visible = true;
                rekl_1.Visible = false; rekl1_1.Visible = true;
                rekl_2.Visible = false; rekl1_2.Visible = true;
                eng.Visible = false; cz.Visible = true;
                kafe.ToolTip = engTool.Text;
                LB.Visible = false; LB1.Visible = true; }
        if (Session["fiLength"] == null)
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/hrnky/"));
            FileInfo[] fi = di.GetFiles();
            Session["fiLength"] = fi.Length;
            for (int i = 0; i < fi.Length; i++)
                Session["fi" + i.ToString()] = fi[i].Name;
        }
        if (Session["kafe"] == null)
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            Session["kafe"] = rnd.Next(Convert.ToInt32(Session["fiLength"].ToString()));
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session["kafe"] = null;
    }

    protected void kafe_PreRender(object sender, EventArgs e)
    {
        (sender as Image).ImageUrl = "~/Kafe.ashx?num=" + Session["kafe"].ToString() + "&file=" + Session["fi" + Session["kafe"].ToString()].ToString();
    }

    protected void Download_PreRender(object sender, EventArgs e)
    {
        (sender as HyperLink).NavigateUrl = "~/Download.aspx?kafe=" + Session["kafe"].ToString();
    }

    protected void SlideShow_Click(object sender, EventArgs e)
    {
        Session["actual"]=Session["kafe"].ToString();
	Response.Redirect("~/SlideShow.aspx");
    }
}
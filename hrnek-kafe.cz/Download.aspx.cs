﻿using System;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

public partial class download : System.Web.UI.Page
{
    protected const int max = 30;

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Session["fiLength"]==null)
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/hrnky/"));
            FileInfo[] fi = di.GetFiles();
            Session["fiLength"] = fi.Length;
            for (int i = 0; i < fi.Length; i++)
                Session["fi" + i.ToString()] = fi[i].Name;
        }

        if (Request["kafe"] != null)
        {
            ImageKafe.ImageUrl = "~/Kafe.ashx?num=" + Request["kafe"] + "&file=" + Session["fi" + Request["kafe"]].ToString();
            Session["page"] = Math.Truncate(Convert.ToDecimal(Convert.ToInt32(Request["kafe"].ToString()) / max));
        }
        else Session["page"] = 0;
    }

    protected void TableSeznam_PreRender(object sender, EventArgs e)
    {
        int fimax = (Convert.ToInt32(Session["page"].ToString()) + 1) * max;
        if (fimax > Convert.ToInt32(Session["fiLength"].ToString())) fimax = Convert.ToInt32(Session["fiLength"].ToString());
        int kafe = 0;
	if (Request["kafe"] != null) kafe = Convert.ToInt32(Request["kafe"].ToString());
        for (int i = Convert.ToInt32(Session["page"].ToString()) * max; i < fimax; i++)
        {
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            Label lb = new Label();
            lb.Text = "#" + i.ToString();
            if (i == kafe) lb.ForeColor = Color.Red;
            tc.Controls.Add(lb);
            tr.Cells.Add(tc);

            tc = new TableCell();
            HyperLink hl = new HyperLink();
            hl.Text = "View coffee #" + i.ToString() + ".";
            hl.NavigateUrl = "~/Download.aspx?kafe=" + i.ToString();
            if (i == kafe)
                hl.ForeColor = Color.Red;
            else
                hl.ForeColor = ColorTranslator.FromHtml("#40339d");
            tc.Controls.Add(hl);
            tr.Cells.Add(tc);

            tc = new TableCell();
            hl = new HyperLink();
            hl.Text = "...Download " + Session["fi" + i.ToString()].ToString() + "...";
            hl.NavigateUrl = "~/Original.aspx?dir=hrnky&file=" + Session["fi" + i.ToString()].ToString();
            if (i == kafe)
                hl.ForeColor = Color.Red;
            else
                hl.ForeColor = ColorTranslator.FromHtml("#40339d");
            tc.Controls.Add(hl);
            tr.Cells.Add(tc);

            (sender as Table).Rows.Add(tr);
        }
    }

    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["fiLength"].ToString()) > Convert.ToInt32((sender as TextBox).Text))
        {
            Response.Redirect("~/Download.aspx?kafe="+ (sender as TextBox).Text);
        }
    }

    protected void ButtonPageDown_Click(object sender, EventArgs e)
    {
        if ((Convert.ToInt32(Session["page"].ToString()) - 1) >= 0)
            Response.Redirect("~/Download.aspx?kafe=" + ((Convert.ToInt32(Session["page"].ToString()) - 1) * max).ToString());
    }

    protected void ButtonPageUp_Click(object sender, EventArgs e)
    {
        if ((Convert.ToInt32(Session["page"].ToString()) + 1)*max <= Convert.ToInt32(Session["fiLength"].ToString())-1)
            Response.Redirect("~/Download.aspx?kafe=" + ((Convert.ToInt32(Session["page"].ToString()) + 1) * max).ToString());
    }

    protected void SearchMax_PreRender(object sender, EventArgs e)
    {
	(sender as Label).Text="max #"+(Convert.ToInt32(Session["fiLength"].ToString())-1).ToString();
    }
}
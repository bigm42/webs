﻿<%@ WebHandler Language="C#" Class="Kafe" %>

using System;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

public class Kafe : IHttpHandler {

    public void ProcessRequest(HttpContext context) {
        Bitmap bm = null; Bitmap bm1 = null;
        if (context.Request["file"] != null)
        {
            bm = new Bitmap(context.Server.MapPath("~/hrnky/" + context.Request["file"]));
            bm1 = new Bitmap(Convert.ToInt32(500 * bm.Width / bm.Height), 500);
            Graphics gr = Graphics.FromImage(bm1);

            gr.DrawImage(bm, new Rectangle(0, 0, Convert.ToInt32(500 * bm.Width / bm.Height), 500));

            gr.DrawString("#" + context.Request["num"], new Font("Calibri", 20f), Brushes.LightGray, 8f, 8f);
        }
        else
        {
            DirectoryInfo di = new DirectoryInfo(context.Server.MapPath("~/hrnky/"));
            FileInfo[] fi = di.GetFiles();
            int i = 0;
            if (context.Request["kafe"] != null)
                i = Convert.ToInt32(context.Request["kafe"].ToString());
            else
            {
                Random rnd = new Random(DateTime.Now.Millisecond);
                i = rnd.Next(fi.Length);
            }
            bm = new Bitmap(context.Server.MapPath("~/hrnky/" + fi[i].Name));
            bm1 = new Bitmap(Convert.ToInt32(300 * bm.Width / bm.Height), 300);
            Graphics gr = Graphics.FromImage(bm1);
            //gr.FillRectangle(new SolidBrush(Color.White), 0, 0, 200, 70);
            //gr.DrawRectangle(Pens.Black, 0, 0, 200, 60);

            gr.DrawImage(bm, new Rectangle(0, 0, Convert.ToInt32(300 * bm.Width / bm.Height), 300));

            gr.DrawString("#" + i.ToString(), new Font("Calibri", 20f), Brushes.LightGray, 8f, 8f);
        }

        MemoryStream st = new MemoryStream();
        bm1.Save(st, ImageFormat.Png);
        context.Response.Clear();
        context.Response.ContentType = "image/png";
        //context.Response.Buffer = true;
        //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BinaryWrite(st.GetBuffer());
        context.Response.Flush();
        //context.Response.End();
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}
﻿using System;
using System.Web.UI.WebControls;
using System.IO;

public partial class SlideShow : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["high"] != null) Session["high"] = Request["high"];
        if (Session["high"] == null) Session["high"] = 400;
        if (Session["kafe"] == null)
        {
            if (Session["fiLength"] == null)
            {
                DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/hrnky/"));
                FileInfo[] fi = di.GetFiles();
                Session["fiLength"] = fi.Length;
                for (int i = 0; i < fi.Length; i++)
                    Session["fi" + i.ToString()] = fi[i].Name;
            }
            if (Session["actual"] == null)
            {
                Random rnd = new Random(DateTime.Now.Millisecond);
                Session["actual"] = rnd.Next(Convert.ToInt32(Session["fiLength"].ToString()));
            }
            Session["kafe"] = Session["actual"].ToString();
        }
        else
        {
            if (Session["actual"] == null)
                Session["actual"] = 0;
            if (Session["actual"].ToString() == Session["kafe"].ToString())
                Session["fiLength"] = null;
            if (Session["fiLength"] == null)
            {
                DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/hrnky/"));
                FileInfo[] fi = di.GetFiles();
                Session["fiLength"] = fi.Length;
                for (int i = 0; i < fi.Length; i++)
                    Session["fi" + i.ToString()] = fi[i].Name;
            }
        }
    }

    protected void Kafe_PreRender(object sender, EventArgs e)
    {
        int a = Convert.ToInt32(Session["actual"].ToString());
        (sender as Image).ImageUrl = "~/Kafe.ashx?num=" + a.ToString() + "&file=" + Session["fi" + a.ToString()].ToString() + "&high=" + Session["high"].ToString();
        a++;
        if (a >= Convert.ToInt32(Session["fiLength"].ToString())) a = 0;
        Session["actual"] = a;
    }

    protected void TextBoxHigh_PreRender(object sender, EventArgs e)
    {
        (sender as TextBox).Text = Session["high"].ToString();
    }

    protected void TextBoxHigh_TextChanged(object sender, EventArgs e)
    {
        Session["high"] = (sender as TextBox).Text;
    }
}
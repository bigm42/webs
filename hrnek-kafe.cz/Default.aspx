﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>-H-R-N-E-K--K-A-F-E-.-C-Z-
    </title>
<link rel="icon" href="~/hrnek_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/hrnek_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/hrnek_icon.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
</head>
<body style="background-color:#e9e9e9;color:#40339d;font-family:Calibri
	">
    <form id="form1" runat="server">
    <div>
        <asp:Panel runat="server" HorizontalAlign="Center">
    <br /><br /><br />
        <asp:Label ID="toto" runat="server" Font-Size="60">Toto je tvůj<br />-H-R-N-E-K--K-A-F-E-</asp:Label>
        <asp:Label ID="toto1" runat="server" Font-Size="60" Visible="False">This is yours<br />-C-A-P--O-F--C-O-F-F-E-E-</asp:Label><br /><br /><br />
        <asp:Image runat="server" ID="kafe" ImageUrl="~/Kafe.ashx" OnPreRender="kafe_PreRender" ToolTip="Pro uložení obrázku stikněte nad tímto pravé tlačítko myši a nazvěte NÁZEVem.PNG . Nebo klikněte na Download níže."/>
	<asp:Label runat="server" ID="engTool" Text="For save of this picture, click, please, on it by the right button of mouse and named it NAME.PNG . Or click on Download lower." Visible="False"/>
	<br /><br /><br />
    <asp:Label runat="server" Font-Size="60" ID="gadit">Svorně to zvládnem!</asp:Label>
    <asp:Label runat="server" Font-Size="60" ID="gadit1" Visible="False" ToolTip="Tepr we da it.">We make it together!</asp:Label><br /><br />
        <asp:Label runat="server" ID="manu">Vyrobeno pro Gandalfa LP2015</asp:Label> 
        <asp:Label runat="server" ID="manu1" Visible="False">Manufactured for Gandalf AD2015</asp:Label> <br /><br /><br />
--
    <asp:LinkButton ID="LB" ForeColor="#40339d" runat="server" OnClick="LinkButton1_Click">Nové kafe. (Reload.)</asp:LinkButton>
    <asp:LinkButton ID="LB1" ForeColor="#40339d" runat="server" OnClick="LinkButton1_Click" Visible="false">New coffee. (Reload.)</asp:LinkButton>
--
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Original.aspx?file=Kouzelná_víla.mp4&content=video/mpeg" ID="rekl">Reklama.</asp:HyperLink>
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Original.aspx?file=Magic_fairy.mp4&content=video/mpeg" ID="rekl1" Visible="False">Advertisment.</asp:HyperLink>
--
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Default.aspx?lang=eng" ID="eng">English version.</asp:HyperLink>
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Default.aspx?lang=cz" ID="cz" Visible="False">Česká verze.</asp:HyperLink>
--
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Download.aspx" OnPreRender="Download_PreRender" ID="Download">Download.</asp:HyperLink>
--
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Credits.aspx" ID="Credits">Credits.</asp:HyperLink>
--
	<asp:LinkButton runat="server" ForeColor="#40339d" ID="SlideShow" OnClick="SlideShow_Click">SlideShow.</asp:LinkButton>
--
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Original.aspx?file=Na_cesty__(For_trip).JPG&content=image/jpeg" ID="rekl_1">Na cesty.</asp:HyperLink>
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Original.aspx?file=Na_cesty__(For_trip).JPG&content=image/jpeg" ID="rekl1_1" Visible="False">For trips.</asp:HyperLink>
--
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Original.aspx?file=Na_ráno_(To_morning).JPG" ID="rekl_2">Na ráno.</asp:HyperLink>
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Original.aspx?file=Na_ráno_(To_morning).JPG" ID="rekl1_2" Visible="False">To morning.</asp:HyperLink>
--
	<br />
    </asp:Panel></div>
    </form>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Credits.aspx.cs" Inherits="Credits" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>-Credits-</title>
<link rel="icon" href="~/hrnek_icon.ico"  type="image/x-icon"/>
<link rel="shortcut icon" href="~/hrnek_icon.ico" type="image/x-icon" />
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta name="msapplication-TileImage" content="~/hrnek_icon.png"/> 
<meta name="msapplication-TileColor" content="#e9e9e9"/>
</head>
<body style="background-color:#e9e9e9;color:#40339d;font-family:Calibri
	">
    <form id="form1" runat="server">
    <div>
        <asp:Panel runat="server" HorizontalAlign="Center">
<br />
<br />
<h1>- Credits -</h1>
<br />
<asp:Image runat="server" ImageUrl="~/hrnek_icon.png"/>
<br />
<br />
<br />
<asp:Label ID="LabelWeby" runat="server" ForeColor="DarkGray"><b>Dceřiné weby (coo-webs):</b></asp:Label>
<br />
<asp:HyperLink ID="HyperLink1tarum2cards1" runat="server" ForeColor="DarkGray" NavigateUrl="http://1tarum2cards1.com">1tarum2cards1.com,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkbeltbox" runat="server" ForeColor="DarkGray" NavigateUrl="http://belt-box.net">belt-box.net,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkblueenvelope" runat="server" ForeColor="DarkGray" NavigateUrl="http://blue-envelope.net">blue-envelope.net,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkcobras" runat="server" ForeColor="DarkGray" NavigateUrl="http://c-o-b-r-a-s.org">c-o-b-r-a-s.org,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkcupofcoffee" runat="server" ForeColor="DarkGray" NavigateUrl="http://cup-of-coffee.org">cup-of-coffee.org,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkgame4upper" runat="server" ForeColor="DarkGray" NavigateUrl="http://game4upper.com">game4upper.com,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkimperiumMOBI" runat="server" ForeColor="DarkGray" NavigateUrl="http://imperium.mobi">imperium.mobi,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkLiVEpAss" runat="server" ForeColor="DarkGray" NavigateUrl="https://live-pass.info">live-pass.info,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkmodraschranka" runat="server" ForeColor="DarkGray" NavigateUrl="http://modra-schranka.cz">modra-schranka.cz,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkpodivna" runat="server" ForeColor="DarkGray" NavigateUrl="http://podivna-zahrada.cz">podivna-zahrada.cz,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkrealprice" runat="server" ForeColor="DarkGray" NavigateUrl="http://real-price.info">real-price.info,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinktheor" runat="server" ForeColor="DarkGray" NavigateUrl="http://the-or.cz">the-or.cz,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkvalasINFO" runat="server" ForeColor="DarkGray" NavigateUrl="http://valas.info">valas.info,</asp:HyperLink>
<br />
<asp:HyperLink ID="HyperLinkwhitemagic" runat="server" ForeColor="DarkGray" NavigateUrl="http://white-magic.net">white-magic.net .</asp:HyperLink>
<br /><br />
<br />
<asp:Label ID="LabelFoto" runat="server" ForeColor="DarkGray">Foceno na (Fotographed on) Canon EOS 550D.</asp:Label>
<br />
<asp:Label ID="LabelC" runat="server" ForeColor="DarkGray">(c) 2015-2018 Mickey Software pro (for) Gandalf(a).</asp:Label>
<br />
        <br />
        <br />
	<asp:HyperLink runat="server" ForeColor="#40339d" NavigateUrl="~/Default.aspx" ID="HLBAck">Zpátky. (Back.)</asp:HyperLink>
        <br />
 
    </asp:Panel>
    </div>
    </form>
</body>
</html>

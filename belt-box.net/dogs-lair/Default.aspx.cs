﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DL_Default : System.Web.UI.Page
{
	protected void OnLoad(object sender, EventArgs e)
	{
		(sender as Button).Text="_"+(sender as Button).Text.Substring(1,(sender as Button).Text.Length-2)+"_";
    }

	protected void OnClick(object sender, EventArgs e)
	{
		Random rnd=new Random(DateTime.Now.Millisecond);
		(sender as Button).Text=(rnd.Next(6)+1).ToString()+(sender as Button).Text.Substring(1,(sender as Button).Text.Length-2)+(rnd.Next(6)+1).ToString();
    }
}
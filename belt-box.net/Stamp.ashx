﻿<%@ WebHandler Language="C#" Class="Stamp" %>

using System;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

public class Stamp : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        Bitmap bm;
        if (context.Request[Constants.CodeStr] == null)
        {
            bm = new Bitmap(200, 200);
            Graphics gr = Graphics.FromImage(bm);
            gr.FillRectangle(new SolidBrush(Color.LightGray), 0, 0, 200, 200);
            gr.DrawRectangle(Pens.DarkGray, 10, 10, 180, 180);

            gr.DrawString("no", new Font(FontFamily.GenericMonospace, 33f), Brushes.Black, 20f, 30f);
            gr.DrawString("stamp", new Font(FontFamily.GenericMonospace, 33f), Brushes.Black, 20f, 80f);
            gr.DrawString("from " + context.Request.UserLanguages[0], new Font(FontFamily.GenericMonospace, 18f), Brushes.Black, 20f, 140f);
        }
        else
        {
            if (context.Request[Constants.CodeStr]==Constants.DeleteStamp)
		{
            bm = new Bitmap(200, 200);
            Graphics gr = Graphics.FromImage(bm);
            gr.FillRectangle(new SolidBrush(Color.LightGray), 0, 0, 200, 200);
            gr.DrawRectangle(Pens.DarkGray, 10, 10, 180, 180);

            gr.DrawString("this", new Font(FontFamily.GenericMonospace, 33f), Brushes.Black, 20f, 30f);
            gr.DrawString("stamp", new Font(FontFamily.GenericMonospace, 33f), Brushes.Black, 20f, 80f);
            gr.DrawString("will be deleted", new Font(FontFamily.GenericMonospace, 12.5f), Brushes.Black, 20f, 140f);
		}
		else
		    bm = new Bitmap(Constants.StampsLoc + context.Request[Constants.CodeStr] + ".png");
        }
        
        MemoryStream st = new MemoryStream();
        bm.Save(st, ImageFormat.Bmp);

        context.Response.Clear();
        context.Response.ContentType = "image/bmp";
        //context.Response.Buffer = true;
        //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BinaryWrite(st.GetBuffer());
        //context.Response.Flush();
        context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}